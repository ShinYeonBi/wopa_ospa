#include "DistancePointEllipseEllipsoid.h"


//----------------------------------------------------------------------------
// The ellipse is (x0/e0)^2 + (x1/e1)^2 = 1 with e0 >= e1.  The query point is
// (y0,y1) with y0 >= 0 and y1 >= 0.  The function returns the distance from
// the query point to the ellipse.  It also computes the ellipse point (x0,x1)
// in the first quadrant that is closest to (y0,y1).
//----------------------------------------------------------------------------
double DistancePointEllipseEllipsoid::DistancePointEllipseSpecial (const double e[2], const double y[2], double x[2])
{
    double distance = (double)0;
    if (y[1] > (double)0)
    {
        if (y[0] > (double)0)
        {
            // Bisect to compute the root of F(t) for t >= -e1*e1.
            double esqr[2] = { e[0]*e[0], e[1]*e[1] };
            double ey[2] = { e[0]*y[0], e[1]*y[1] };
            double t0 = -esqr[1] + ey[1];
            double t1 = -esqr[1] + sqrt(ey[0]*ey[0] + ey[1]*ey[1]);
            double t = t0;
            const int imax = 2*std::numeric_limits<double>::max_exponent;
            for (int i = 0; i < imax; ++i)
            {
                t = ((double)0.5)*(t0 + t1);
                if (t == t0 || t == t1)
                {
                    break;
                }

                double r[2] = { ey[0]/(t + esqr[0]), ey[1]/(t + esqr[1]) };
                double f = r[0]*r[0] + r[1]*r[1] - (double)1;
                if (f > (double)0)//(y0, y1) is inside the ellipse
                {
                    t0 = t;
                }
                else if (f < (double)0)//(y0, y1) is inside the ellipse
                {
                    t1 = t;
                }
                else
                {
                    break;
                }
            }
            x[0] = esqr[0]*y[0]/(t + esqr[0]);
            x[1] = esqr[1]*y[1]/(t + esqr[1]);
            double d[2] = { x[0] - y[0], x[1] - y[1] };
            distance = sqrt(d[0]*d[0] + d[1]*d[1]);
        }
        else  // y0 == 0
        {
            x[0] = (double)0;
            x[1] = e[1];
            distance = fabs(y[1] - e[1]);
        }
    }
    else  // y1 == 0
    {
        double denom0 = e[0]*e[0] - e[1]*e[1];
        double e0y0 = e[0]*y[0];
        if (e0y0 < denom0)
        {
            // y0 is inside the subinterval.
            double x0de0 = e0y0/denom0;
            double x0de0sqr = x0de0*x0de0;
            x[0] = e[0]*x0de0;
            x[1] = e[1]*sqrt(fabs((double)1 - x0de0sqr));
            double d0 = x[0] - y[0];
            distance = sqrt(d0*d0 + x[1]*x[1]);
        }
        else
        {
            // y0 is outside the subinterval.  The closest ellipse point has
            // x1 == 0 and is on the domain-boundary interval (x0/e0)^2 = 1.
            x[0] = e[0];
            x[1] = (double)0;
            distance = fabs(y[0] - e[0]);
        }
    }
    return distance;
}

//----------------------------------------------------------------------------
// The ellipse is (x0/e0)^2 + (x1/e1)^2 = 1.  The query point is (y0,y1).
// The function returns the distance from the query point to the ellipse.
// It also computes the ellipse point (x0,x1) that is closest to (y0,y1).
//----------------------------------------------------------------------------
double DistancePointEllipseEllipsoid::DistancePointEllipse (const double e[2], const double y[2], double x[2])
{
    // Determine reflections for y to the first quadrant.
    bool reflect[2];
    int i, j;
    for (i = 0; i < 2; ++i)
    {
        reflect[i] = (y[i] < (double)0);
    }

    // Determine the axis order for decreasing extents.
    int permute[2];
    if (e[0] < e[1])
    {
        permute[0] = 1;  permute[1] = 0;
    }
    else
    {
        permute[0] = 0;  permute[1] = 1;
    }

    int invpermute[2];
    for (i = 0; i < 2; ++i)
    {
        invpermute[permute[i]] = i;
    }

    double locE[2], locY[2];
    for (i = 0; i < 2; ++i)
    {
        j = permute[i];
        locE[i] = e[j];
        locY[i] = y[j];
        if (reflect[j])
        {
            locY[i] = -locY[i];
        }
    }

    double locX[2];
    double distance = DistancePointEllipseSpecial(locE, locY, locX);

    // Restore the axis order and reflections.
    for (i = 0; i < 2; ++i)
    {
        j = invpermute[i];
        if (reflect[i])
        {
            locX[j] = -locX[j];
        }
        x[i] = locX[j];
    }

    return distance;
}

//----------------------------------------------------------------------------
// The ellipsoid is (x0/e0)^2 + (x1/e1)^2 + (x2/e2)^2 = 1 with e0 >= e1 >= e2.
// The query point is (y0,y1,y2) with y0 >= 0, y1 >= 0, and y2 >= 0.  The
// function returns the distance from the query point to the ellipsoid.  It
// also computes the ellipsoid point (x0,x1,x2) in the first octant that is
// closest to (y0,y1,y2).
//----------------------------------------------------------------------------
double DistancePointEllipseEllipsoid::DistancePointEllipsoidSpecial (const double e[3], const double y[3], double x[3])
{
    double distance;
    if (y[2] > (double)0)
    {
        if (y[1] > (double)0)
        {
            if (y[0] > (double)0)
            {
                // Bisect to compute the root of F(t) for t >= -e2*e2.
                double esqr[3] = { e[0]*e[0], e[1]*e[1], e[2]*e[2] };
                double ey[3] = { e[0]*y[0], e[1]*y[1], e[2]*y[2] };
                double t0 = -esqr[2] + ey[2];
                double t1 = -esqr[2] + sqrt(ey[0]*ey[0] + ey[1]*ey[1] +
                    ey[2]*ey[2]);
                double t = t0;
                const int imax = 2*std::numeric_limits<double>::max_exponent;
                for (int i = 0; i < imax; ++i)
                {
                    t = ((double)0.5)*(t0 + t1);
                    if (t == t0 || t == t1)
                    {
                        break;
                    }

                    double r[3] = { ey[0]/(t + esqr[0]), ey[1]/(t + esqr[1]), ey[2]/(t + esqr[2]) };
                    double f = r[0]*r[0] + r[1]*r[1] + r[2]*r[2] - (double)1;
                    if (f > (double)0)//(y0, y1, y2) is outside the ellipse
                    {
                        t0 = t;
                    }
                    else if (f < (double)0)//(y0, y1, y2) is inside the ellipse
                    {
                        t1 = t;
                    }
                    else
                    {
                        break;
                    }
                }

                x[0] = esqr[0]*y[0]/(t + esqr[0]);
                x[1] = esqr[1]*y[1]/(t + esqr[1]);
                x[2] = esqr[2]*y[2]/(t + esqr[2]);
                double d[3] = { x[0] - y[0], x[1] - y[1], x[2] - y[2] };
                distance = sqrt(d[0]*d[0] + d[1]*d[1] + d[2]*d[2]);
            }
            else  // y0 == 0
            {
                x[0] = (double)0;
                double etmp[2] = { e[1], e[2] };
                double ytmp[2] = { y[1], y[2] };
                double xtmp[2];
                distance = DistancePointEllipseSpecial(etmp, ytmp, xtmp);
                x[1] = xtmp[0];
                x[2] = xtmp[1];
            }
        }
        else  // y1 == 0
        {
            x[1] = (double)0;
            if (y[0] > (double)0)
            {
                double etmp[2] = { e[0], e[2] };
                double ytmp[2] = { y[0], y[2] };
                double xtmp[2];
                distance = DistancePointEllipseSpecial(etmp, ytmp, xtmp);
                x[0] = xtmp[0];
                x[2] = xtmp[1];
            }
            else  // y0 == 0
            {
                x[0] = (double)0;
                x[2] = e[2];
                distance = fabs(y[2] - e[2]);
            }
        }
    }
    else  // y2 == 0
    {
        double denom[2] = { e[0]*e[0] - e[2]*e[2], e[1]*e[1] - e[2]*e[2] };
        double ey[2] = { e[0]*y[0], e[1]*y[1] };
        if (ey[0] < denom[0] && ey[1] < denom[1])
        {
            // (y0,y1) is inside the axis-aligned bounding rectangle of the
            // subellipse.  This intermediate test is designed to guard
            // against the division by zero when e0 == e2 or e1 == e2.
            double xde[2] = { ey[0]/denom[0], ey[1]/denom[1] };
            double xdesqr[2] = { xde[0]*xde[0], xde[1]*xde[1] };
            double discr = (double)1 - xdesqr[0] - xdesqr[1];
            if (discr > (double)0)
            {
                // (y0,y1) is inside the subellipse.  The closest ellipsoid
                // point has x2 > 0.
                x[0] = e[0]*xde[0];
                x[1] = e[1]*xde[1];
                x[2] = e[2]*sqrt(discr);
                double d[2] = { x[0] - y[0], x[1] - y[1] };
                distance = sqrt(d[0]*d[0] + d[1]*d[1] + x[2]*x[2]);
            }
            else
            {
                // (y0,y1) is outside the subellipse.  The closest ellipsoid
                // point has x2 == 0 and is on the domain-boundary ellipse
                // (x0/e0)^2 + (x1/e1)^2 = 1.
                x[2] = (double)0;
                distance = DistancePointEllipseSpecial(e, y, x);
            }
        }
        else
        {
            // (y0,y1) is outside the subellipse.  The closest ellipsoid
            // point has x2 == 0 and is on the domain-boundary ellipse
            // (x0/e0)^2 + (x1/e1)^2 = 1.
            x[2] = (double)0;
            distance = DistancePointEllipseSpecial(e, y, x);
        }
    }
    return distance;
}

//----------------------------------------------------------------------------
// The ellipsoid is (x0/e0)^2 + (x1/e1)^2 + (x2/e2)^2 = 1.  The query point is
// (y0,y1,y2).  The function returns the distance from the query point to the
// ellipsoid.   It also computes the ellipsoid point (x0,x1,x2) that is
// closest to (y0,y1,y2).
//----------------------------------------------------------------------------
double DistancePointEllipseEllipsoid::DistancePointEllipsoid (const double e[3], const double y[3], double x[3])
{
    // Determine reflections for y to the first octant.
    bool reflect[3];
    int i, j;
    for (i = 0; i < 3; ++i)
    {
        reflect[i] = (y[i] < (double)0);
    }

    // Determine the axis order for decreasing extents.
    int permute[3];
    if (e[0] < e[1])
    {
        if (e[2] < e[0])
        {
            permute[0] = 1;  permute[1] = 0;  permute[2] = 2;
        }
        else if (e[2] < e[1])
        {
            permute[0] = 1;  permute[1] = 2;  permute[2] = 0;
        }
        else
        {
            permute[0] = 2;  permute[1] = 1;  permute[2] = 0;
        }
    }
    else
    {
        if (e[2] < e[1])
        {
            permute[0] = 0;  permute[1] = 1;  permute[2] = 2;
        }
        else if (e[2] < e[0])
        {
            permute[0] = 0;  permute[1] = 2;  permute[2] = 1;
        }
        else
        {
            permute[0] = 2;  permute[1] = 0;  permute[2] = 1;
        }
    }

    int invpermute[3];
    for (i = 0; i < 3; ++i)
    {
        invpermute[permute[i]] = i;
    }

    double locE[3], locY[3];
    for (i = 0; i < 3; ++i)
    {
        j = permute[i];
        locE[i] = e[j];
        locY[i] = y[j];
        if (reflect[j])
        {
            locY[i] = -locY[i];
        }
    }

    double locX[3];
    double distance = DistancePointEllipsoidSpecial(locE, locY, locX);

    // Restore the axis order and reflections.
    for (i = 0; i < 3; ++i)
    {
        j = invpermute[i];
        if (reflect[i])
        {
            locX[j] = -locX[j];
        }
        x[i] = locX[j];
    }

    return distance;
}

//----------------------------------------------------------------------------
// Bisect for the root of
//   F(t) = sum_{j=0}{m-1} (e[i[j]]*y[i[j]]/(t + e[i[j]]*e[i[j]])
// for t >= -e[i[m-1]]*e[i[m-1]].  The incoming e[] and y[] values are those
// for which the query point components are positive.
//----------------------------------------------------------------------------
double DistancePointEllipseEllipsoid::DPHSBisector (int numComponents, const double e[3], const double y[3], double x[3])
{
    double esqr[3], ey[3], argument = (double)0;
    int i;
    for (i = 0; i < numComponents; ++i)
    {
        esqr[i] = e[i]*e[i];
        ey[i] = e[i]*y[i];
        argument += ey[i]*ey[i];
    }

    double t0 = -esqr[numComponents-1] + ey[numComponents-1];
    double t1 = -esqr[numComponents-1] + sqrt(argument);
    double t = t0;
    const int jmax = 2*std::numeric_limits<double>::max_exponent;
    for (int j = 0; j < jmax; ++j)
    {
        t = ((double)0.5)*(t0 + t1);
        if (t == t0 || t == t1)
        {
            break;
        }

        double f = (double)-1;
        for (i = 0; i < numComponents; ++i)
        {
            double r = ey[i]/(t + esqr[i]);
            f += r*r;
        }
        if (f > (double)0)
        {
            t0 = t;
        }
        else if (f < (double)0)
        {
            t1 = t;
        }
        else
        {
            break;
        }
    }

    double distance = (double)0;
    for (i = 0; i < numComponents; ++i)
    {
        x[i] = esqr[i]*y[i]/(t + esqr[i]);
        double diff = x[i] - y[i];
        distance += diff*diff;
    }
    distance = sqrt(distance);
    return distance;
}

double test(double a)
{
	a++;
	return a;
}