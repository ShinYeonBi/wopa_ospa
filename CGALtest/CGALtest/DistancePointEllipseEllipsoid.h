#pragma once
#include <cmath>
#include <iostream>
class DistancePointEllipseEllipsoid 
{

public:
	double DistancePointEllipseSpecial (const double e[2], const double y[2], double x[2]);
	double DistancePointEllipse (const double e[2], const double y[2], double x[2]);
	double DistancePointEllipsoidSpecial (const double e[3], const double y[3], double x[3]);
	double DistancePointEllipsoid (const double e[3], const double y[3], double x[3]);
	double DPHSBisector (int numComponents, const double e[3], const double y[3], double x[3]);
	double test(double a);
};
