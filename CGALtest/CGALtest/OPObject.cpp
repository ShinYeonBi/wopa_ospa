#include "OPObject.h"


OPObject::OPObject(void)
{

}


OPObject::~OPObject(void)
{
}

OPObject::OPObject(int facet_num, R3Mesh *p_mesh)
{
	mesh = p_mesh;
	m_facet_num = facet_num;
	seg_arr = new int[facet_num];
	rawsdf = new double[facet_num];
	everageEdgeLenght = 0;
	m_final_error_sum = 0;
	m_final_error_evg = 0;
	m_pre_error_sum = 0;
	m_pre_error_evg = 0;
}

void OPObject::splitSegment(int idx)
{
	if(segmentList[idx]->m_maxdistance < segmentList[idx]->m_radius.Length()/5.0f)
		return;

	std::cout<< "[split start! -"<< idx <<"-]" << std::endl;
	int count = 0;
	int id1 = 0;
	int id2 = 0;
	int id3 = 0;
	segment * p_seg = segmentList[idx];//나눠야할 segment
	int isRorL = 0;

	segment * new_segL = new segment(p_seg->m_numpoint);
	new_segL->setmesh(p_seg->m_mesh);

	//segment vertex정리
	for(int i = 0; i<p_seg->m_numpoint; i++)
	{
		if(p_seg->m_segpointList[i] == 1)//메쉬 버텍스 개수 크기의 배열. 이 세그먼트가 메쉬의 어떤 버텍스를 포함하고있는지 저장(포함=1, 안포함=0)
		{
			R3MeshVertex * v = p_seg->m_mesh->Vertex(i);

			isRorL = RorL(p_seg,  p_seg->m_mesh->VertexPosition(v));

			if(isRorL == 1)//왼. 왼쪽에 있는 버텍스들이 새로운 세그먼트에 들어간다.
			{
				count++;
				new_segL->m_segpointList[i] = 1;//새로운 세그먼트에 버텍스 추가
				p_seg->m_segpointList[i] = 0;//현재 세그먼트에서 버텍스 뺌
				new_segL->m_numsegvertex++;//새로운 세그먼트 버텍스 총개수 늘림
				p_seg->m_numsegvertex--;//현재 세그먼트 버텍스 총개수 감소
			}
		}
	}

	new_segL->computeEigenv();
	if(new_segL->m_radius.X()/4.0f > new_segL->m_radius.Z())//너무 납작한 파티클인경우 없앰
		return;

	std::cout<< "[count :"<< count <<"-]" << std::endl;
	if(count <= 6)
	{
		printf("count < 6");
		return;
	}

	segmentList.push_back(new_segL);
	std::cout<< "[new seg :"<< segmentList.size() -1 <<"-]" << std::endl;
	//facearr 정리
	for(int i = 0; i<new_segL->m_mesh->NFaces(); i++)
	{
		R3MeshFace *face = new_segL->m_mesh->Face(i);

		R3MeshVertex * v = new_segL->m_mesh->VertexOnFace(face, 0);
		id1 = new_segL->m_mesh->VertexID(v);
		v = new_segL->m_mesh->VertexOnFace(face, 1);
		id2 = new_segL->m_mesh->VertexID(v);
		v = new_segL->m_mesh->VertexOnFace(face, 2);
		id3 = new_segL->m_mesh->VertexID(v);

		//만약 face에 속한 vertex 세개가 모두 segpointlist에 1로 마킹되어있으면, 이 face는 새로운 segment에 속해있음
		if( (new_segL->m_segpointList[id1] == 1) && (new_segL->m_segpointList[id2] == 1) &&(new_segL->m_segpointList[id2] == 1))
		{
			seg_arr[i] = segmentList.size();//번호= +1이니까...
		}
	}

	p_seg->computeEigenv();

	new_segL->rayEllipsoidIntersect();
	p_seg->rayEllipsoidIntersect();
	splitSegment(idx);

}

int OPObject::RorL(segment* seg, R3Point q_point) 
{
	R3Point st = seg->m_MaxDistrotMeshpoint; 
	R3Point end = seg->m_MaxDistrotEllipsoidpoint;

	R3Vector up = seg->m_eigen_vector3;
	R3Vector fwd = end - st;
	R3Vector targetDir = q_point - st;

	R3Vector perp = fwd%targetDir;
	float dir = perp.Dot(up);
		
	if (dir > 0) //왼쪽에 있음
	{
		return 1;
	} 
	else if (dir < 0) //오른쪽에 있음
	{
		return -1;
	} 
	else //동일선상에 있음
	{
		return 0;
	}
}

void OPObject::searchSplitSegment()
{
	R3Vector cdv;//파티클 센터와 maxerror위치와의 거리
	double len = 0;//바운딩 박스 대각선 길이. 이 길이에 비례하는 값으로 파티클을 나눌지 말지 정함
	double cdlen = 0;
	int i = 3;
	for(int i = 0; i< segmentList.size(); i++)
	{
		segmentList[i]->rayEllipsoidIntersect();


		std::cout<<"[seg]"<< i << ": Length()/7.9f = " << len << " , m_maxdistance = "<< segmentList[i]->m_maxdistance << std::endl;
		

		splitSegment(i);
		std::cout<<"split "<< i << std::endl;

	}
	//setCOMnPointList();
}

OPObject* OPObject::copyOPObject()
{
	OPObject * result_obj = new OPObject(m_facet_num, mesh);

	for(int i = 0; i<m_facet_num; i++)
	{
		result_obj->seg_arr[i] = seg_arr[i];
	}

	for(int i = 0; i<segmentList.size(); i++)
	{
		segment * tmp = new segment(mesh->NVertices());
		tmp->setmesh(mesh);
		result_obj->segmentList.push_back(tmp);
	}

	result_obj->setCOMnPointList();

	for(int i = 0; i<segmentList.size(); i++)
	{
		result_obj->segmentList[i]->computeEigenv();
	}

	return result_obj;

}

void OPObject::setCOMnPointList()
{
	for (int i = 0; i < mesh->NFaces(); i++) 
	{
		R3MeshFace *face = mesh->Face(i);//메쉬의 n번째 face 가져옴
		int color_index = 0;
		color_index = seg_arr[i];
		segmentList[color_index]->m_segFaceList.push_back(face);//각 세그먼트에 속한 face들 저장...

		////
		R3MeshVertex * v = mesh->VertexOnFace(face, 0);
		int id1 = mesh->VertexID(v);
		v = mesh->VertexOnFace(face, 1);
		int id2 = mesh->VertexID(v);
		v = mesh->VertexOnFace(face, 2);
		int id3 = mesh->VertexID(v);
		
		//세그먼트별로 메쉬의 어떤 버텍스를 저장하고있는지 표시
		if(segmentList[color_index]->m_segpointList[id1] == 0)
		{
		segmentList[color_index]->m_segpointList[id1] = 1;
		segmentList[color_index]->m_numsegvertex ++;
		segmentList[color_index]->m_InnerMeshVertex.push_back(mesh->VertexOnFace(face, 0));///
		}

		if(segmentList[color_index]->m_segpointList[id2] == 0)
		{
		segmentList[color_index]->m_segpointList[id2] = 1;
		segmentList[color_index]->m_numsegvertex ++;
		segmentList[color_index]->m_InnerMeshVertex.push_back(mesh->VertexOnFace(face, 1));/////
		}

		if(segmentList[color_index]->m_segpointList[id3] == 0)
		{
		segmentList[color_index]->m_segpointList[id3] = 1;
		segmentList[color_index]->m_numsegvertex ++;
		segmentList[color_index]->m_InnerMeshVertex.push_back(mesh->VertexOnFace(face, 2));//////
		}

	}

}

void OPObject::ParticleArrangement()
{
	int i = 0;
	int j = 0;
	m_allocatedParticles.clear();
	m_notallocatedParticles.clear();
	bool intersect = false;
	double length = 0;
	R3Point com(0,0,0);
	int cnt = 0;
	double gap = mesh->BBox().DiagonalLength()/18.0f;//논문 실험시 17로 함
	double rad = mesh->BBox().DiagonalLength()/30.0f;
	std::cout<<"gap"<<gap<<std::endl;

	R3MeshVertex * v = mesh->Vertex(0);
	m_allocatedParticles.push_back(mesh->VertexPosition(v));

	for(i = 1; i<mesh->NVertices(); i++)
	{
	    R3MeshVertex * v2 = mesh->Vertex(i);
	    m_notallocatedParticles.push_back(mesh->VertexPosition(v2));
	}

	for(i = 0; i<m_notallocatedParticles.size(); i++)
	{
	    intersect = false;
		for(j = 0; j<m_allocatedParticles.size(); j++)
		{
			length = (m_allocatedParticles[j] - m_notallocatedParticles[i]).Length();

			if(length < gap)//너무 많이 겹치면 pass
	    	{
	    		intersect = true;
	    		break;
	    	}
		}

		if(!intersect)//미리 배치된 모든 파티클과 겹치치 않으면
	    {
			m_allocatedParticles.push_back(m_notallocatedParticles[i]);
			m_notallocatedParticles.erase(m_notallocatedParticles.begin() + i);
			i--;//원소 하나 빠졌으니까 인덱스 하나 빼줌
		}
	}

	//세그먼트...는 아니고 파티클 정보 설정해서 배치
	for(j = 0; j<m_allocatedParticles.size(); j++)
	{
		segment * new_seg = new segment(mesh->NVertices());
		new_seg->m_radius.Reset(rad, rad, rad);
		new_seg->m_com.Reset(m_allocatedParticles[j].X(), m_allocatedParticles[j].Y(), m_allocatedParticles[j].Z());
		new_seg->setmesh(mesh);

		for(int k = 0; k<5; k++)//일정 횟수 반복
		{
			new_seg->m_InnerMeshVertex.clear();
			com.Reset(0,0,0);
			cnt = 0;
			new_seg->m_numsegvertex = 0;
			//각 파티클의 내부에 속하는 점들 설정
			for(i = 0; i<mesh->NVertices(); i++)
			{
				//new_seg->m_segpointList[i] = 0;
				R3MeshVertex * v3 = mesh->Vertex(i);

				if((mesh->VertexPosition(v3) - new_seg->m_com).Length() < new_seg->m_radius.X())//구 파티클 내부에 있으면
				{
					//new_seg->m_segpointList[i] = 1;
					new_seg->m_InnerMeshVertex.push_back(v3);
					new_seg->m_numsegvertex ++;
					com += mesh->VertexPosition(v3);
					cnt++;
				}
			}
		
			com/=cnt;
			//중심을 안쪽으로 이동
			new_seg->m_com = com;
		}
		new_seg->computeEigenv();

		//파티클 라디어스 constraint[r/a ...r]
		if(new_seg->m_radius.X()/2.5 > new_seg->m_radius.Z())
		{
			new_seg->m_radius.SetCoord(2, new_seg->m_radius.X()/2.5);
		}

		new_seg->m_volume = 4*PI*new_seg->m_radius.X()*new_seg->m_radius.Y()*new_seg->m_radius.Z()/3.0f;
		

		segmentList.push_back(new_seg);
	}

}

void OPObject::drawArrangedPoints()
{
	std::cout<<p_Points.size()<<std::endl;
	for(int i = 0; i<p_Points.size(); i++)
	{

		glEnable(GL_LIGHTING);
		static GLfloat material[] = { 0.8, 0.4, 0.2, 1.0 };
		glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, material); 
		RNScalar radius = 0.005 * mesh->BBox().DiagonalLength();

		R3Point position = m_allocatedParticles[i];
		position += 0.5 * radius * R3Vector(0.01, 0.01, 0.01);
		material[0] = (31 + 48 * ((3*i) % 5)) / 256.0;
		material[1] = (31 + 32 * ((5*i) % 7)) / 256.0;
		material[2] = (31 + 19 * ((7*i) % 11)) / 256.0;
		glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, material); 
		R3Sphere(mesh->VertexPosition(p_Points[i].v), radius).Draw();		
	}
}

typedef struct
{
  double Length;
  int idx;
} strt;

bool compare(strt &a, strt &b)
{
	return a.Length < b.Length;
}

bool compare2(OPObject::v_Len &a, OPObject::v_Len &b)
{
	return a.Length > b.Length;
}

void OPObject::Connect1ring()
{
	m_connectionTable = new int[segmentList.size()*segmentList.size()];
	for(int i = 0; i<segmentList.size()*segmentList.size(); i++)
	{
		m_connectionTable[i] = 0;
	}
	int cnt = segmentList.size() * segmentList[0]->m_numNeighbor;
	std::cout<<"connection start"<<std::endl;
	std::vector<strt> vector_length;

	for(int i = 0; i<segmentList.size(); i++)
	{
		vector_length.clear();
		for(int k = 0; k<segmentList[i]->m_numNeighbor; k++)
		{
			segmentList[i]->m_NeiborIdx[k] = 0;
		}

		segmentList[i]->m_NeiborsegPointer.clear();

		for(int j = 0; j<segmentList.size(); j++)
		{
			if(i == j)
				continue;
			strt tmp;
			tmp.Length = (segmentList[i]->m_com - segmentList[j]->m_com).Length();
			//if(tmp.Length < 0.001)
			//	continue;
			tmp.idx = j;
			vector_length.push_back(tmp);
		}
		std::sort(vector_length.begin(), vector_length.end(), compare);

		for(int k = 0; k<segmentList[i]->m_numNeighbor; k++)
		{
			segmentList[i]->m_NeiborIdx[k] = vector_length[k].idx;
			segmentList[i]->m_NeiborsegPointer.push_back( segmentList[vector_length[k].idx]);
			m_connectionTable[i*segmentList.size() + vector_length[k].idx] = 1;
			m_connectionTable[vector_length[k].idx*segmentList.size() + i] = 1;
		}
		
	}

	std::cout<<"connection end"<<std::endl;
}

void OPObject::draw1ring()
{
	for(int i = 0; i<segmentList.size(); i++)
	{
		for(int j = 0; j<segmentList[i]->m_NeiborsegPointer.size(); j++)
		{
			glDisable(GL_LIGHTING);
			glBegin(GL_LINES);
			glColor3f(0, 0, 0);
			glVertex3d(segmentList[i]->m_com.X(),segmentList[i]->m_com.Y(), segmentList[i]->m_com.Z()); 
			glVertex3d(segmentList[segmentList[i]->m_NeiborIdx[j]]->m_com.X(),
				segmentList[segmentList[i]->m_NeiborIdx[j]]->m_com.Y(),
				segmentList[segmentList[i]->m_NeiborIdx[j]]->m_com.Z()); 
			glEnd( );
			glEnable(GL_LIGHTING);
		}
	}
	
}

void OPObject::searcherrorPoints()
{
	double hit = 0;
	double in = 0;
	R3Point p;
	R3Point rx;
	R3Vector ray(0,0,0);
	double mindist = FLT_MAX;
	double maxdist = FLT_MIN;
	double tmpdist = 0;
	double n_mindist = FLT_MAX;
	int minparticleIDX = 0;
	int maxvertexIDX = 0;
	bool isin = false;

	p_Points.clear();
	
	for(int i = 0; i<mesh->NVertices(); i++)
	{
		isin = false;
		minparticleIDX = 0;
		//모든 파티클과의 radial projection 거리 구해서, 최소거리를 선택
		R3MeshVertex * v = mesh->Vertex(i);
		for(int j = 0; j< segmentList.size(); j++)
		{
			p = (segmentList[j]->m_OriMat*(mesh->VertexPosition(v)-segmentList[j]->m_com)).Point();
			in = (p.X()*p.X())/(segmentList[j]->m_radius.X()*segmentList[j]->m_radius.X())
				+ (p.Y()*p.Y())/(segmentList[j]->m_radius.Y()*segmentList[j]->m_radius.Y())
				+ (p.Z()*p.Z())/(segmentList[j]->m_radius.Z()*segmentList[j]->m_radius.Z()) - 1;

			if(in < 0)//파티클 내부에 있는 점임
			{
				isin = true;
				break;
			}
		}

		//if(!isin)//파티클 밖에 있는 버텍스에서만 파티클과의 거리 계산
		{
			mindist = FLT_MAX;
			n_mindist = FLT_MAX;
			for(int j = 0; j< segmentList.size(); j++)
			{
				p = (segmentList[j]->m_OriMat*(mesh->VertexPosition(v)-segmentList[j]->m_com)).Point();
				ray.Reset(p.X(), p.Y(), p.Z());
				ray.Normalize();//파티클 중심에서 메쉬버텍스를 향하는 벡터

				double a = (ray.X()*ray.X())/(segmentList[j]->m_radius.X()*segmentList[j]->m_radius.X())
						+ (ray.Y()*ray.Y())/(segmentList[j]->m_radius.Y()*segmentList[j]->m_radius.Y())
						+ (ray.Z()*ray.Z())/(segmentList[j]->m_radius.Z()*segmentList[j]->m_radius.Z());

				hit = sqrt(a)/a;

				if(mindist > ((ray*hit).Point()-p).Length())
				{
					mindist = ((ray*hit).Point()-p).Length();
					minparticleIDX = j;

				}
			}

			if(mindist > mesh->BBox().DiagonalLength()/70.0f)//에러가 일정 값 이상이면 후보버텍스에 추가.
			{
				v_Len tmp;
				tmp.v = v;
				tmp.Length = mindist;
				tmp.nearseg = segmentList[minparticleIDX];
				p_Points.push_back(tmp);
			}

		}

		std::sort(p_Points.begin(), p_Points.end(), compare2);//거리순으로 내림차순 정렬되어있음.
		
	}
	
	/*
	for(int i = 0; i<mesh->NVertices(); i++)
	{
		isin = false;
		minparticleIDX = 0;
		mindist = FLT_MAX;
		n_mindist = FLT_MAX;
		//가장 가까운 파티클과의 radial projection 거리 구함
		R3MeshVertex * v = mesh->Vertex(i);
		for(int j = 0; j< segmentList.size(); j++)
		{
			tmpdist = (mesh->VertexPosition(v)-segmentList[j]->m_com).Length();

			if(mindist > tmpdist)
			{
				mindist = tmpdist;
				minparticleIDX = j;//가장 가까운 파티클의 인덱스
			}
		}

		
		p = (segmentList[minparticleIDX]->m_OriMat*(mesh->VertexPosition(v)-segmentList[minparticleIDX]->m_com)).Point();
		ray.Reset(p.X(), p.Y(), p.Z());
		ray.Normalize();//파티클 중심에서 메쉬버텍스를 향하는 벡터

		double a = (ray.X()*ray.X())/(segmentList[minparticleIDX]->m_radius.X()*segmentList[minparticleIDX]->m_radius.X())
				+ (ray.Y()*ray.Y())/(segmentList[minparticleIDX]->m_radius.Y()*segmentList[minparticleIDX]->m_radius.Y())
				+ (ray.Z()*ray.Z())/(segmentList[minparticleIDX]->m_radius.Z()*segmentList[minparticleIDX]->m_radius.Z());

		hit = sqrt(a)/a;


		if(mesh->BBox().DiagonalLength()/70.0f < ((ray*hit).Point()-p).Length())
		{
			v_Len tmp;
			tmp.v = v;
			tmp.Length = ((ray*hit).Point()-p).Length();
			tmp.nearseg = segmentList[minparticleIDX];
			p_Points.push_back(tmp);

		}
		std::sort(p_Points.begin(), p_Points.end(), compare2);//거리순으로 내림차순 정렬되어있음.
	}*/
}

void OPObject::findCandidatePoints()
{
	searcherrorPoints();

	while(p_Points.size()>0)
	{
		std::cout<<"while..."<<p_Points.size()<<std::endl;
		splitSegment2(p_Points);
		std::cout<<"while..."<<p_Points.size()<<std::endl;
	}
}


void OPObject::splitSegment2(std::vector <v_Len>& p_Points)
{
	int k = 0;
	int i = 0;
	double len_com1 = 0;
	double len_com2 = 0;
	R3Vector eigv;
	double rad = mesh->BBox().DiagonalLength()/30.0f;
	std::vector <R3MeshVertex*> plist;
	segment* p_seg = p_Points[0].nearseg; //잘려야할 세그먼트 = 가장 가까운 세그먼트
	segment * new_seg = new segment(p_seg->m_numpoint);
	new_seg->setmesh(p_seg->m_mesh);

	p_seg->m_isOptimized = false;

	plist.push_back(p_Points[0].v);
	
	for(i = 0; i<mesh->NVertices(); i++)
	{
		R3MeshVertex * v = mesh->Vertex(i);
		if((mesh->VertexPosition(p_Points[0].v) - mesh->VertexPosition(v)).Length() < p_Points[0].Length)
		{
			plist.push_back(v);//후보리스트 중에서 일정거리 이내에 있는 버텍스를 PCA할 버텍스리스트에 넣음
			for(k = 1; k<p_Points.size(); k++)
			{
				if(v == p_Points[k].v)
				{
					p_Points.erase(p_Points.begin() + k);//PCA 후보가 된 버텍스는 후보리스트에서 뺌
					break;
				}
			}
			
		}
	}
	
	std::cout<<"erase..."<<p_Points.size()<<std::endl;

	for(i = 0; i<p_seg->m_InnerMeshVertex.size(); i++)
	{
		plist.push_back(p_seg->m_InnerMeshVertex[i]);//잘려야할 세그먼트에 속한 버텍스들도 pca후보리스트에 추가
	}

	//p_Points[0]과 가장가까운 세그먼트의 중심을 잇는 벡터와 가장가까운 세그먼트의 아이겐벡터 3개를 비교. 그 벡터와 방향이 가장 평행한 아이겐벡터 선택
	double v1 = p_seg->m_eigen_vector1.Dot(mesh->VertexPosition(p_Points[0].v) - p_Points[0].nearseg->m_com);
	v1 *= v1;
	double v2 = p_seg->m_eigen_vector2.Dot(mesh->VertexPosition(p_Points[0].v) - p_Points[0].nearseg->m_com);
	v2 *= v2;
	double v3 = p_seg->m_eigen_vector3.Dot(mesh->VertexPosition(p_Points[0].v) - p_Points[0].nearseg->m_com);
	v3 *= v3;

	if(v1 > v2 && v1>v3)
		eigv = p_seg->m_eigen_vector1;

	else if(v2 > v1 && v2 > v3)
		eigv = p_seg->m_eigen_vector2;

	else 
		eigv = p_seg->m_eigen_vector3;

	R3Point com1 = p_seg->m_com + eigv*(p_seg->m_radius.X());//기존 세그먼트의 중심
	R3Point com2 = p_seg->m_com - eigv*(p_seg->m_radius.X());//새 세그먼트의 중심

	p_seg->m_InnerMeshVertex.clear();//잘릴 세그먼트의 내부 버텍스들을 새로 설정하기 위해서 내부 점 클리어.

	for(i = 0; i<plist.size(); i++)//pca 후보점들
	{
		len_com1 = (mesh->VertexPosition(plist[i]) - com1).Length(); //두 개의 중심 중에 어느 중심에 가까운지 결정.
		len_com2 = (mesh->VertexPosition(plist[i]) - com2).Length();

		if(len_com1 < len_com2)
		{
			p_seg->m_InnerMeshVertex.push_back(plist[i]);
		}
		else
			new_seg->m_InnerMeshVertex.push_back(plist[i]);
	}
	p_Points.erase(p_Points.begin());

	if(p_seg->m_InnerMeshVertex.size() <=1 || new_seg->m_InnerMeshVertex.size()<=1)
		return;


	//각각의 파티클 정보 설정
	p_seg->m_com = com1;
	new_seg->m_com = com2;
	p_seg->m_radius.Reset(rad, rad, rad);
	new_seg->m_radius.Reset(rad, rad, rad);
	p_seg->computeEigenv();
	new_seg->computeEigenv();
	segmentList.push_back(new_seg);

	//파티클 라디어스 constraint[r/a ...r]
	if(new_seg->m_radius.X()/2.5 > new_seg->m_radius.Z())
	{
		new_seg->m_radius.SetCoord(2, new_seg->m_radius.X()/2.5);
	}

	if(p_seg->m_radius.X()/2.5 > p_seg->m_radius.Z())
	{
		p_seg->m_radius.SetCoord(2, p_seg->m_radius.X()/2.5);
	}
	
}

void OPObject::computefinalError(int flg)
{
	double hit = 0;
	double in = 0;
	double tmpdist = 0;
	R3Point p;
	R3Point rx;
	R3Vector ray(0,0,0);
	double mindist = FLT_MAX;
	double maxdist = FLT_MIN;
	double n_mindist = FLT_MAX;
	int minparticleIDX = 0;
	int maxvertexIDX = 0;
	bool isin = false;
	R3Point hitv;
	R3Point aaa;
	m_final_error_sum = 0;
	m_final_error_evg = 0;
	m_pre_error_sum = 0;
	m_pre_error_evg = 0;
	double m_pre_error_Pevg = 0;
	double m_final_error_Pevg = 0;
	m_FinalErrorPoints.clear();

	for(int i = 0; i<mesh->NVertices(); i++)
	{
		isin = false;
		minparticleIDX = 0;
		mindist = FLT_MAX;
		n_mindist = FLT_MAX;
		//가장 가까운 파티클과의 radial projection 거리 구함
		R3MeshVertex * v = mesh->Vertex(i);
		for(int j = 0; j< segmentList.size(); j++)
		{
			tmpdist = (mesh->VertexPosition(v)-segmentList[j]->m_com).Length();

			if(mindist > tmpdist)
			{
				mindist = tmpdist;
				minparticleIDX = j;
			}
		}

		
		p = (segmentList[minparticleIDX]->m_OriMat*(mesh->VertexPosition(v)-segmentList[minparticleIDX]->m_com)).Point();
		ray.Reset(p.X(), p.Y(), p.Z());
		ray.Normalize();//파티클 중심에서 메쉬버텍스를 향하는 벡터

		double a = (ray.X()*ray.X())/(segmentList[minparticleIDX]->m_radius.X()*segmentList[minparticleIDX]->m_radius.X())
				+ (ray.Y()*ray.Y())/(segmentList[minparticleIDX]->m_radius.Y()*segmentList[minparticleIDX]->m_radius.Y())
				+ (ray.Z()*ray.Z())/(segmentList[minparticleIDX]->m_radius.Z()*segmentList[minparticleIDX]->m_radius.Z());

		hit = sqrt(a)/a;

		hitv.Reset((ray*hit).Point().X(), (ray*hit).Point().Y(), (ray*hit).Point().Z());//파티클 표면 위의 점
		aaa = segmentList[minparticleIDX]->inv*hitv + segmentList[minparticleIDX]->m_com;

		mindist = ((ray*hit).Point()-p).Length();

		m_FinalErrorPoints.push_back(aaa);

		if(maxdist < mindist)
		{
			maxdist = mindist;
		}

		if(flg == 2)
			m_final_error_sum += mindist;
		else if(flg == 1)
			m_pre_error_sum += mindist;

		
	}
	if(flg == 2)
	{
		m_final_error_evg = m_final_error_sum/mesh->NVertices();
		m_final_error_Pevg = m_final_error_sum/segmentList.size();
		std::cout<< "2_error_sum : "<< m_final_error_sum << ",  2_error_evg : "<< m_final_error_evg<<
			", threshold : "<< mesh->BBox().DiagonalLength()/70.0f << ", Nvertic: "<<mesh->NVertices()<< 
			", error_Pevg : "<< m_final_error_Pevg<< ", maxerror : "<< maxdist<< std::endl;
	}
	else if(flg == 1)
	{
		m_pre_error_evg = m_pre_error_sum/mesh->NVertices();
		m_pre_error_Pevg = m_pre_error_sum/segmentList.size();
		std::cout<< "1_error_sum : "<< m_pre_error_sum << ",  1_error_evg : "<< m_pre_error_evg<< 
			", threshold : "<< mesh->BBox().DiagonalLength()/70.0f << ", Nvertic: "<<mesh->NVertices()<< 
			", error_Pevg : "<< m_pre_error_Pevg<< ", maxerror : "<< maxdist<< std::endl;
		
	}
}

void OPObject::drawFinalError()
{
	R3Point p;
	for(int i = 0; i<mesh->NVertices(); i++)
	{
		R3MeshVertex * v = mesh->Vertex(i);
		glDisable(GL_LIGHTING);
		glBegin(GL_LINES);
		glColor3f(0.0, 0.0, 0.0);
		p = mesh->VertexPosition(v);
		glVertex3d(p.X(), p.Y(), p.Z()); 
		glVertex3d(m_FinalErrorPoints[i].X(),m_FinalErrorPoints[i].Y(), m_FinalErrorPoints[i].Z()); 
		glEnd( );

		glEnable(GL_LIGHTING);
	}
}

void OPObject::computefinalError2(int flg)
{
	double hit = 0;
	double hitsecond = 0;
	double in = 0;
	double tmp = 0;
	double tmpdist = 0;
	R3Point p;
	R3Point ray_origin(0,0,0);
	R3Vector ray_normal1(0,0,0);
	double mindist = FLT_MAX;
	double maxdist = FLT_MIN;
	double n_mindist = FLT_MAX;
	int minparticleIDX = 0;
	int maxvertexIDX = 0;
	bool isin = false;
	R3Point hitv;
	double m_sumrayDist = 0;
	int m_vertN = 0;
	R3Point ry;
	R3Point rx;
	m_final_error_sum = 0;
	m_final_error_evg = 0;
	m_pre_error_sum = 0;
	m_pre_error_evg = 0;
	double m_pre_error_Pevg = 0;
	double m_final_error_Pevg = 0;
	m_FinalErrorPoints.clear();
	m_FinalErrorPoints_s.clear();

	for(int i = 0; i<mesh->NVertices(); i++)
	{
		isin = false;
		minparticleIDX = 0;
		mindist = FLT_MAX;
		n_mindist = FLT_MAX;
		//가장 가까운 파티클과의 radial projection 거리 구함
		R3MeshVertex * v = mesh->Vertex(i);
		for(int j = 0; j< segmentList.size(); j++)
		{
			tmpdist = (mesh->VertexPosition(v)-segmentList[j]->m_com).Length();

			if(mindist > tmpdist)
			{
				mindist = tmpdist;
				minparticleIDX = j;
			}
		}

		
		p = (segmentList[minparticleIDX]->m_OriMat*(mesh->VertexPosition(v)-segmentList[minparticleIDX]->m_com)).Point();
		ray_origin.Reset(p.X(),	p.Y(), p.Z());
		ray_normal1= segmentList[minparticleIDX]->m_OriMat*(mesh->VertexNormal(v));


		double a = ((ray_normal1.X()*ray_normal1.X())/(segmentList[minparticleIDX]->m_radius.X()*segmentList[minparticleIDX]->m_radius.X()))
					+ ((ray_normal1.Y()*ray_normal1.Y())/(segmentList[minparticleIDX]->m_radius.Y()*segmentList[minparticleIDX]->m_radius.Y()))
					+ ((ray_normal1.Z()*ray_normal1.Z())/(segmentList[minparticleIDX]->m_radius.Z()*segmentList[minparticleIDX]->m_radius.Z()));

		double b = ((2*ray_origin.X()*ray_normal1.X())/(segmentList[minparticleIDX]->m_radius.X()*segmentList[minparticleIDX]->m_radius.X()))
					+ ((2*ray_origin.Y()*ray_normal1.Y())/(segmentList[minparticleIDX]->m_radius.Y()*segmentList[minparticleIDX]->m_radius.Y()))
					+ ((2*ray_origin.Z()*ray_normal1.Z())/(segmentList[minparticleIDX]->m_radius.Z()*segmentList[minparticleIDX]->m_radius.Z()));

		double c = ((ray_origin.X()*ray_origin.X())/(segmentList[minparticleIDX]->m_radius.X()*segmentList[minparticleIDX]->m_radius.X()))
					+ ((ray_origin.Y()*ray_origin.Y())/(segmentList[minparticleIDX]->m_radius.Y()*segmentList[minparticleIDX]->m_radius.Y()))
					+ ((ray_origin.Z()*ray_origin.Z())/(segmentList[minparticleIDX]->m_radius.Z()*segmentList[minparticleIDX]->m_radius.Z()))
					- 1;

		double d = ((b*b)-(4*a*c));			
		if ( d < 0 ) { continue; }
		else { d = sqrt(d); }
		hit = (-b + d)/(2*a);
		hitsecond = (-b - d)/(2*a);
			
		if((hit<0 && hitsecond<0) || (hit>0 && hitsecond>0))//만약 hit, hitsecond의 부호가 같으면 절대값이 작은걸 고른다
		{
			if( fabs(hit) < fabs(hitsecond))
			{	
				rx = ray_origin + ray_normal1*hit;
				tmp = fabs(hit);
			}
			else
			{
				rx = ray_origin + ray_normal1*hitsecond;
				tmp = fabs(hitsecond);
			}	
		}

		else//만약 hit, hitsecond중 하나는 양수, 하나는 음수이면 (노말 또는 노말 반대방향)과 같은방향이라는 것을 나타내는 해(양수인 해)를 고른다
		{
			if(hit>=0)
			{
				rx = ray_origin + ray_normal1*hit;
				tmp = fabs(hit);
			}

			else
			{
				rx = ray_origin + ray_normal1*hitsecond;
				tmp = fabs(hitsecond);
			}
			
		}

		ry = segmentList[minparticleIDX]->inv*R3Point(ray_origin.X(), ray_origin.Y(), ray_origin.Z());//메쉬 위의 점
		rx = segmentList[minparticleIDX]->inv*R3Point(rx.X(), rx.Y(), rx.Z());//타구체 위의 점

		ry+=segmentList[minparticleIDX]->m_com;
		rx+=segmentList[minparticleIDX]->m_com;

		m_sumrayDist += (rx-ry).Length();
		m_vertN++;

		tmp = (rx-ry).Length();

		m_FinalErrorPoints_s.push_back(ry);
		m_FinalErrorPoints.push_back(rx);

		if(maxdist < tmp)
		{
			maxdist = tmp;
		}

		if(flg == 2)
			m_final_error_sum += tmp;
		else if(flg == 1)
			m_pre_error_sum += tmp;

		
	}
	if(flg == 2)
	{
		m_final_error_evg = m_final_error_sum/m_FinalErrorPoints_s.size();
		m_final_error_Pevg = m_final_error_sum/segmentList.size();
		std::cout<< "2_error_sum : "<< m_final_error_sum << ",  2_error_evg : "<< m_final_error_evg<<
			", threshold : "<< mesh->BBox().DiagonalLength()/70.0f << ", Nvertic: "<<mesh->NVertices()<< 
			", error_Pevg : "<< m_final_error_Pevg<< ", maxerror : "<< maxdist<< std::endl;
	}
	else if(flg == 1)
	{
		m_pre_error_evg = m_pre_error_sum/m_FinalErrorPoints_s.size();
		m_pre_error_Pevg = m_pre_error_sum/segmentList.size();
		std::cout<< "1_error_sum : "<< m_pre_error_sum << ",  1_error_evg : "<< m_pre_error_evg<< 
			", threshold : "<< mesh->BBox().DiagonalLength()/70.0f << ", Nvertic: "<<mesh->NVertices()<< 
			", error_Pevg : "<< m_pre_error_Pevg<< ", maxerror : "<< maxdist<< std::endl;
		
	}
}

void OPObject::drawFinalError2()
{
	for(int i = 0; i<m_FinalErrorPoints_s.size(); i++)
	{
		glDisable(GL_LIGHTING);
		glBegin(GL_LINES);
		glColor3f(0.0, 0.0, 0.0);
		glVertex3d(m_FinalErrorPoints_s[i].X(),m_FinalErrorPoints_s[i].Y(), m_FinalErrorPoints_s[i].Z()); 
		glVertex3d(m_FinalErrorPoints[i].X(),m_FinalErrorPoints[i].Y(), m_FinalErrorPoints[i].Z()); 
		glEnd( );

		glEnable(GL_LIGHTING);
	}
}

double OPObject::computeRadialError(segment *seg)
{
	R3Point ray_origin(0,0,0);
	R3Vector ray_normal1(0,0,0);
	R3Point rx;
	double hit = 0;
	//normal error+
	R3Vector Mnormal;
	double E[3];//Ellipsoid normal;

	double RadialError = 0;

	for(int i = 0; i<seg->m_InnerMeshVertex.size(); i++)
	{

		R3Point p = seg->m_OriMat*R3Point(seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).X() - seg->m_com.X(),
			seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).Y() - seg->m_com.Y(), 
			seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).Z() - seg->m_com.Z());
			
		ray_normal1.Reset(p.X(), p.Y(), p.Z());
		ray_normal1.Normalize();

		double a = (ray_normal1.X()*ray_normal1.X())/(seg->m_radius.X()*seg->m_radius.X())
				+ (ray_normal1.Y()*ray_normal1.Y())/(seg->m_radius.Y()*seg->m_radius.Y())
				+ (ray_normal1.Z()*ray_normal1.Z())/(seg->m_radius.Z()*seg->m_radius.Z());

		double b = (2*ray_origin.X()*ray_normal1.X())/(seg->m_radius.X()*seg->m_radius.X())
				+ (2*ray_origin.Y()*ray_normal1.Y())/(seg->m_radius.Y()*seg->m_radius.Y())
				+ (2*ray_origin.Z()*ray_normal1.Z())/(seg->m_radius.Z()*seg->m_radius.Z());

		double c = (ray_origin.X()*ray_origin.X())/(seg->m_radius.X()*seg->m_radius.X())
				+ (ray_origin.Y()*ray_origin.Y())/(seg->m_radius.Y()*seg->m_radius.Y())
				+ (ray_origin.Z()*ray_origin.Z())/(seg->m_radius.Z()*seg->m_radius.Z());
				- 1;

		double d = ((b*b)-(4*a*c));			
		if ( d < 0 ) { continue; }

		hit = sqrt(a)/a;
		rx = ray_origin + ray_normal1*hit;
		RadialError += (( (rx-p).Length()*(rx-p).Length() ) / (seg->m_maxradialerr*seg->m_maxradialerr));
		
	}
	return RadialError;
	//printf("RadialError : %f \n", RadialError);

}

double OPObject::computeNormalError(segment *seg)
{
	R3Point ray_origin(0,0,0);
	R3Vector ray_normal1(0,0,0);
	R3Point rx;
	double hit = 0;
	//normal error+
	R3Vector Mnormal;
	double E[3];//Ellipsoid normal;

	double NormalError = 0;

	for(int i = 0; i<seg->m_InnerMeshVertex.size(); i++)
	{

		R3Point p = seg->m_OriMat*R3Point(seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).X() - seg->m_com.X(),
			seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).Y() - seg->m_com.Y(), 
			seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).Z() - seg->m_com.Z());
			
		ray_normal1.Reset(p.X(), p.Y(), p.Z());
		ray_normal1.Normalize();

		double a = (ray_normal1.X()*ray_normal1.X())/(seg->m_radius.X()*seg->m_radius.X())
				+ (ray_normal1.Y()*ray_normal1.Y())/(seg->m_radius.Y()*seg->m_radius.Y())
				+ (ray_normal1.Z()*ray_normal1.Z())/(seg->m_radius.Z()*seg->m_radius.Z());

		double b = (2*ray_origin.X()*ray_normal1.X())/(seg->m_radius.X()*seg->m_radius.X())
				+ (2*ray_origin.Y()*ray_normal1.Y())/(seg->m_radius.Y()*seg->m_radius.Y())
				+ (2*ray_origin.Z()*ray_normal1.Z())/(seg->m_radius.Z()*seg->m_radius.Z());

		double c = (ray_origin.X()*ray_origin.X())/(seg->m_radius.X()*seg->m_radius.X())
				+ (ray_origin.Y()*ray_origin.Y())/(seg->m_radius.Y()*seg->m_radius.Y())
				+ (ray_origin.Z()*ray_origin.Z())/(seg->m_radius.Z()*seg->m_radius.Z());
				- 1;

		double d = ((b*b)-(4*a*c));			
		if ( d < 0 ) { continue; }

		hit = sqrt(a)/a;

		//normal error +
		E[0] = (2*hit*ray_normal1.X())/(seg->m_radius.X()*seg->m_radius.X());
		E[1] = (2*hit*ray_normal1.Y())/(seg->m_radius.Y()*seg->m_radius.Y());
		E[2] = (2*hit*ray_normal1.Z())/(seg->m_radius.Z()*seg->m_radius.Z());
		
		Mnormal = seg->m_OriMat*seg->m_mesh->VertexNormal(seg->m_InnerMeshVertex[i]);

		double w[3] = {Mnormal.X(), Mnormal.Y(), Mnormal.Z()};
		NormalError += computeVectorAngle(E, w);		
	}
	return NormalError;

}

double OPObject::computeDistError(segment *seg)
{
	double length = 0;
	double real_length = 0;
	double ray_normald[3] = {0,0,0};
	double ray_normald2[3] = {0,0,0};
	R3Vector ray_normal1(0,0,0);
	double hit = 0;
	double hit2 = 0;
	double a2;
	double error;
	double sumdist = 0;
	double weight = 0;
	double DistError = 0;
	for(int i = 0; i<seg->m_NeiborsegPointer.size(); i++)
	{
		sumdist += 1/(seg->m_NeiborsegPointer[i]->m_com - seg->m_com).Length();
	}

	for(int i = 0; i<seg->m_NeiborsegPointer.size(); i++)
	{
		//나를 기준으로 이웃의 중심 좌표 구함
		R3Point p = seg->m_OriMat*R3Point(seg->m_NeiborsegPointer[i]->m_com.X() - seg->m_com.X(),
										seg->m_NeiborsegPointer[i]->m_com.Y() - seg->m_com.Y(), 
										seg->m_NeiborsegPointer[i]->m_com.Z() - seg->m_com.Z());
			
		ray_normal1.Reset(p.X(), p.Y(), p.Z());
		weight = 1/ray_normal1.Length()/sumdist;
		length += ray_normal1.Length()*weight;//*1.2f;//나와 이웃중심까지의 거리

		ray_normal1.Normalize();
		ray_normald[0] = ray_normal1.X();	ray_normald[1] = ray_normal1.Y();	ray_normald[2] = ray_normal1.Z();//이웃의 중심을 향하는 ray
		ray_normald2[0] = -ray_normal1.X();	ray_normald2[1] = -ray_normal1.Y();	ray_normald2[2] = -ray_normal1.Z();//이웃에서 나를 향하는 ray


		double a = (ray_normal1.X()*ray_normal1.X())/(seg->m_radius.X()*seg->m_radius.X())
				+ (ray_normal1.Y()*ray_normal1.Y())/(seg->m_radius.Y()*seg->m_radius.Y())
				+ (ray_normal1.Z()*ray_normal1.Z())/(seg->m_radius.Z()*seg->m_radius.Z());

		a2 = (ray_normald2[0]*ray_normald2[0])/(seg->m_NeiborsegPointer[i]->m_radius.X()*seg->m_NeiborsegPointer[i]->m_radius.X())
				+ (ray_normald2[1]*ray_normald2[1])/(seg->m_NeiborsegPointer[i]->m_radius.Y()*seg->m_NeiborsegPointer[i]->m_radius.Y())
				+ (ray_normald2[2]*ray_normald2[2])/(seg->m_NeiborsegPointer[i]->m_radius.Z()*seg->m_NeiborsegPointer[i]->m_radius.Z());

		hit = sqrt(a)/a;
		hit2 = sqrt(a2)/a2;

		real_length += (hit + hit2)*weight;
	}
	if(seg->m_maxradialerr <= 0.000000001 || seg->m_numNeighbor <= 0 || (length - real_length) == 0)
		return 0;
	else
	{
		//printf("length: %f, maxraderr: %f, m_Inner: %d, m_numNei: %f\n",(length - real_length), seg->m_maxradialerr, seg->m_InnerMeshVertex.size(), seg->m_numNeighbor);
		error = (length - real_length)*(length - real_length)/(seg->m_maxradialerr*seg->m_maxradialerr);//(seg->m_numNeighbor);
		//DistError = error*seg->m_InnerMeshVertex.size();
		//printf("disterr: %f\n", DistError);
		//return DistError;
		return error;
	}
}

double OPObject::computeVectorAngle(double * v, double *w)
{
	double vlength = sqrt((v[0]*v[0]) + (v[1]*v[1]) + (v[2]*v[2]));
	double wlength = sqrt((w[0]*w[0]) + (w[1]*w[1]) + (w[2]*w[2]));

    v[0] /= vlength;
    v[1] /= vlength;
    v[2] /= vlength;

	w[0] /= wlength;
    w[1] /= wlength;
    w[2] /= wlength;

	double Dot = (v[0]*w[0]) + (v[1]*w[1]) + (v[2]*w[2]);
	
	return (-(Dot-1)/2.0f);//*(-(Dot-1)/2.0f);
}

double OPObject::computeVolumeError(segment *seg)
{
	double volumeError = 0;

	volumeError = (seg->m_volume - 4*3.141592*seg->m_radius.X()*seg->m_radius.Y()*seg->m_radius.Z()/3.0f)*
		(seg->m_volume - 4*3.141592*seg->m_radius.X()*seg->m_radius.Y()*seg->m_radius.Z()/3.0f)/(seg->m_volume*seg->m_volume);

	return volumeError;
}

void OPObject::WOPAsplitSegment(segment* seg, double thresh)
{
	if(seg->m_maxdistance < thresh)
		return;

	//std::cout<< "[split start! -"<< idx <<"-]" << std::endl;
	//int count = 0;
	int id1 = 0;
	int id2 = 0;
	int id3 = 0;
	int isRorL = 0;
	segment * p_seg = seg;//segmentList[idx];//나눠야할 segment

	segment * new_segL = new segment(p_seg->m_numpoint , p_seg->m_mesh);//새로운 segment
	segment * new_segR = new segment(p_seg->m_numpoint , p_seg->m_mesh);//새로운 segment

	//segment vertex정리
	for(int i = 0; i<p_seg->m_InnerMeshVertex.size(); i++)//나눠야할 세그먼트에 속한 버텍스 살펴봄
	{
		R3MeshVertex * v = p_seg->m_InnerMeshVertex[i];

		isRorL = RorL(p_seg,  p_seg->m_mesh->VertexPosition(v));

		if(isRorL == 1)//왼. 왼쪽에 있는 버텍스들이 새로운 세그먼트에 들어간다.
		{
			new_segL->m_numsegvertex++;//새로운 세그먼트 버텍스 총개수 늘림
			new_segL->m_InnerMeshVertex.push_back(v);
			new_segL->m_segpointList[new_segL->m_mesh->VertexID(v)] = 1;
		}
		else
		{
			new_segR->m_numsegvertex++;//새로운 세그먼트 버텍스 총개수 늘림
			new_segR->m_InnerMeshVertex.push_back(v);
			new_segR->m_segpointList[new_segR->m_mesh->VertexID(v)] = 1;
		}
		
	}

	std::cout<< "[new_segL vertex num:"<< new_segL->m_numsegvertex <<"-]" << std::endl;
	std::cout<< "[p_seg vertex num:"<< new_segR->m_numsegvertex <<"-]" << std::endl;

	if(new_segL->m_numsegvertex <=6 || new_segR->m_numsegvertex <=6)
		return;

	new_segL->computeEigenv();
	new_segR->computeEigenv();

	std::cout<< "[new segL idx:"<< segmentList.size() <<"-]" << std::endl;
	std::cout<< "[new segR idx:"<< segmentList.size()+1 <<"-]" << std::endl;
	//facearr 정리
	for(int i = 0; i<p_seg->m_segFaceList.size(); i++)
	{
		R3MeshFace *face = p_seg->m_segFaceList[i];

		R3MeshVertex * v = new_segL->m_mesh->VertexOnFace(face, 0);
		id1 = new_segL->m_mesh->VertexID(v);
		v = new_segL->m_mesh->VertexOnFace(face, 1);
		id2 = new_segL->m_mesh->VertexID(v);
		v = new_segL->m_mesh->VertexOnFace(face, 2);
		id3 = new_segL->m_mesh->VertexID(v);

		//만약 face에 속한 vertex 세개가 모두 segpointlist에 1로 마킹되어있으면, 이 face는 새로운 segment에 속해있음
		if( (new_segL->m_segpointList[id1] == 1) && (new_segL->m_segpointList[id2] == 1) &&(new_segL->m_segpointList[id3] == 1))
		{
			seg_arr[mesh->FaceID(face)] = segmentList.size();//번호= +1이니까...
		}

		else
		{
			seg_arr[mesh->FaceID(face)] = segmentList.size()+1;//번호= +2이니까...
		}
	}

	p_seg->del = true;

	segmentList.push_back(new_segL);
	segmentList.push_back(new_segR);

	new_segL->rayEllipsoidIntersect();
	new_segR->rayEllipsoidIntersect();
	//splitSegment(new_segR, thresh);
	//splitSegment(new_segL, thresh);

}

void OPObject::WOPAsearchSplitSegment(double thresh)
{
	for(int i = 0; i< segmentList.size(); i++)
	{
		segmentList[i]->rayEllipsoidIntersect();

		std::cout<<"[seg]"<< i << ":threshold  = " << thresh << " , maxdistance = "<< segmentList[i]->m_maxdistance << std::endl;	
		WOPAsplitSegment(segmentList[i], thresh);
	}

	for(int i = 0; i< segmentList.size(); i++)
	{
		if(segmentList[i]->del)
		{
			segmentList.erase(segmentList.begin() + i);
			i--;
		}
	}
}