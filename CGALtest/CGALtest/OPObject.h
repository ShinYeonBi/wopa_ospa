#pragma once
#include "segment.h"
//#include "Opt.h"
#include <stdio.h>
#include <vector>
#include <algorithm>

class OPObject
{
public:
	OPObject(void);
	~OPObject(void);
	OPObject(int facet_num, R3Mesh *p_mesh);


	struct v_len
	{
	  double Length;//가장 가까운 파티클까지와의 거리
	  R3MeshVertex * v;
	  segment * nearseg;//가장 가까운 파티클
	};

	typedef struct v_len v_Len;

	R3Mesh *mesh;
	//R3Mesh *remesh;
	int m_facet_num;
	int *seg_arr;//face 순서대로 무슨 세그먼트에 속해있는지 본다.
	int *m_connectionTable;//연결구조
	std::vector <segment*> segmentList;
	double *rawsdf;
	double everageEdgeLenght;
	//KK a;
	//표면에 파티클 배치용
	std::vector <R3Point> m_allocatedParticles;
	std::vector <R3Point> m_notallocatedParticles;
	//표면배치할때 추가로 배치해야할 버텍스 후보 저장
	//std::vector <R3MeshVertex*> m_candidatePoints;
	//
	std::vector <v_Len> p_Points;
	std::vector <R3Point> m_FinalErrorPoints_s;
	std::vector <R3Point> m_FinalErrorPoints;

	//final error
	double m_final_error_sum;
	double m_final_error_evg;
	double m_pre_error_sum;
	double m_pre_error_evg;

	void searchSplitSegment();
	void splitSegment(int idx);
	OPObject* copyOPObject();
	void setCOMnPointList();
	int RorL(segment* seg, R3Point q_point);
	void ParticleArrangement();
	void Connect1ring();
	void drawArrangedPoints();
	void draw1ring();
	void findCandidatePoints();
	void findCandidatePoints2(std::vector <v_Len> p_Points, segment* newseg);
	void splitSegment2(std::vector <v_Len>& p_Points);
	void searcherrorPoints();
	void computefinalError(int flg);
	void drawFinalError();
	void computefinalError2(int flg);
	void drawFinalError2();

	//줄어든 에러 계산
	double computeVectorAngle(double * v, double *w);
	double computeRadialError(segment *seg);
	double computeNormalError(segment *seg);
	double computeDistError(segment *seg);
	double computeVolumeError(segment *seg);

	//wopa
	void WOPAsplitSegment(segment* seg, double thresh);
	void WOPAsearchSplitSegment(double thresh);
};

