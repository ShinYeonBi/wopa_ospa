#include "Opt.h"


Opt::Opt(void)
{
	lb[0] = 0;
	lb[1] = 0;
	lb[2] = 0;
	ub[0] = HUGE_VAL;
	ub[1] = HUGE_VAL;
	ub[2] = HUGE_VAL;
	m_total_rad_err = 0;
	m_total_cen_err = 0;
	m_total_ori_err = 0;
	oriopt = nlopt_create(NLOPT_LN_COBYLA, 3);

	radiiopt = nlopt_create(NLOPT_LD_MMA, 3); /* algorithm and dimensionality */
	centeropt = nlopt_create(NLOPT_LD_MMA, 3);

}

Opt::~Opt(void)
{
}

Opt::Opt(int p_totalpoint, OPObject *p_op)
{
	lb[0] = 0;
	lb[1] = 0;
	lb[2] = 0;
	ub[0] = HUGE_VAL;
	ub[1] = HUGE_VAL;
	ub[2] = HUGE_VAL;
	totalpoint = p_totalpoint;
	op = p_op;
	m_total_rad_err = 0;
	m_total_cen_err = 0;
	m_total_ori_err = 0;
	oriopt = nlopt_create(NLOPT_LN_COBYLA, 3);

	radiiopt = nlopt_create(NLOPT_LD_MMA, 3); /* algorithm and dimensionality */
	centeropt = nlopt_create(NLOPT_LD_MMA, 3);

}
double computeVectorAngle(double * v, double *w)
{
	double vlength = sqrt((v[0]*v[0]) + (v[1]*v[1]) + (v[2]*v[2]));
	double wlength = sqrt((w[0]*w[0]) + (w[1]*w[1]) + (w[2]*w[2]));

    v[0] /= vlength;
    v[1] /= vlength;
    v[2] /= vlength;

	w[0] /= wlength;
    w[1] /= wlength;
    w[2] /= wlength;

	double Dot = (v[0]*w[0]) + (v[1]*w[1]) + (v[2]*w[2]);
	
	return (-(Dot-1)/2.0f);//*(-(Dot-1)/2.0f);
}

double oneringDistError_radii(const double* x, segment *seg)//많이 겹치면 안됨
{
	double length = 0;
	double real_length = 0;
	double ray_normald[3] = {0,0,0};
	double ray_normald2[3] = {0,0,0};
	R3Vector ray_normal1(0,0,0);
	double hit = 0;
	double hit2 = 0;
	double a2;
	double error;
	double sumdist = 0;
	double weight = 0;

	for(int i = 0; i<seg->m_NeiborsegPointer.size(); i++)
	{
		sumdist += 1/(seg->m_NeiborsegPointer[i]->m_com - seg->m_com).Length();
	}

	for(int i = 0; i<seg->m_NeiborsegPointer.size(); i++)
	{
		//나를 기준으로 이웃의 중심 좌표 구함
		R3Point p = seg->m_OriMat*R3Point(seg->m_NeiborsegPointer[i]->m_com.X() - seg->m_com.X(),
										seg->m_NeiborsegPointer[i]->m_com.Y() - seg->m_com.Y(), 
										seg->m_NeiborsegPointer[i]->m_com.Z() - seg->m_com.Z());
			
		ray_normal1.Reset(p.X(), p.Y(), p.Z());
		weight = 1/ray_normal1.Length()/sumdist;
		length += ray_normal1.Length()*weight;//*1.2f;//나와 이웃중심까지의 거리

		ray_normal1.Normalize();
		ray_normald[0] = ray_normal1.X();	ray_normald[1] = ray_normal1.Y();	ray_normald[2] = ray_normal1.Z();//이웃의 중심을 향하는 ray
		ray_normald2[0] = -ray_normal1.X();	ray_normald2[1] = -ray_normal1.Y();	ray_normald2[2] = -ray_normal1.Z();//이웃에서 나를 향하는 ray


		double a = (ray_normal1.X()*ray_normal1.X())/(x[0]*x[0])
				+ (ray_normal1.Y()*ray_normal1.Y())/(x[1]*x[1])
				+ (ray_normal1.Z()*ray_normal1.Z())/(x[2]*x[2]);

		a2 = (ray_normald2[0]*ray_normald2[0])/(seg->m_NeiborsegPointer[i]->m_radius.X()*seg->m_NeiborsegPointer[i]->m_radius.X())
				+ (ray_normald2[1]*ray_normald2[1])/(seg->m_NeiborsegPointer[i]->m_radius.Y()*seg->m_NeiborsegPointer[i]->m_radius.Y())
				+ (ray_normald2[2]*ray_normald2[2])/(seg->m_NeiborsegPointer[i]->m_radius.Z()*seg->m_NeiborsegPointer[i]->m_radius.Z());

		hit = sqrt(a)/a;
		hit2 = sqrt(a2)/a2;

		real_length += (hit + hit2)*weight;
	}
	error = (length - real_length)*(length - real_length)/(seg->m_maxradialerr*seg->m_maxradialerr);//(seg->m_numNeighbor);
	return error;
}

double oneringDistError_center(const double* x, segment *seg)//많이 겹치면 안됨
{
	double length = 0;
	double real_length = 0;
	double ray_normald[3] = {0,0,0};
	double ray_normald2[3] = {0,0,0};
	R3Vector ray_normal1(0,0,0);
	R3Vector Origin(0,0,0);
	double hit = 0;
	double hit2 = 0;
	double a2;
	double error;
	double sumdist = 0;
	double weight = 0;
	double colinear = 1;
	
	for(int i = 0; i<seg->m_NeiborsegPointer.size(); i++)
	{
		//나를 기준으로 이웃의 중심 좌표 구함
		R3Point p = seg->m_OriMat*R3Point(seg->m_NeiborsegPointer[i]->m_com.X() - seg->m_com.X(),
										seg->m_NeiborsegPointer[i]->m_com.Y() - seg->m_com.Y(), 
										seg->m_NeiborsegPointer[i]->m_com.Z() - seg->m_com.Z());
			
		ray_normal1.Reset(p.X()-x[0], p.Y()-x[1], p.Z()-x[2]);//x[0,1,2] = 내가 이동한 중심
		sumdist += 1/ray_normal1.Length();
	}

	for(int i = 0; i<seg->m_NeiborsegPointer.size(); i++)
	{
		//나를 기준으로 이웃의 중심 좌표 구함
		R3Point p = seg->m_OriMat*R3Point(seg->m_NeiborsegPointer[i]->m_com.X() - seg->m_com.X(),
										seg->m_NeiborsegPointer[i]->m_com.Y() - seg->m_com.Y(), 
										seg->m_NeiborsegPointer[i]->m_com.Z() - seg->m_com.Z());
			
		Origin.Reset(p.X(), p.Y(), p.Z());	
		Origin.Normalize();//이동한 위치가 연결직선상인지 확인하기 위함

		ray_normal1.Reset(p.X()-x[0], p.Y()-x[1], p.Z()-x[2]);//x[0,1,2] = 내가 이동한 중심
		weight = 1/ray_normal1.Length()/sumdist;
		length += ray_normal1.Length()*weight*1.3f;//나와 이웃중심까지의 거리

		ray_normal1.Normalize();
		ray_normald[0] = ray_normal1.X();	ray_normald[1] = ray_normal1.Y();	ray_normald[2] = ray_normal1.Z();//이웃의 중심을 향하는 ray
		ray_normald2[0] = -ray_normal1.X();	ray_normald2[1] = -ray_normal1.Y();	ray_normald2[2] = -ray_normal1.Z();//이웃에서 나를 향하는 ray

		//이동한 위치가 연결직선상인지 확인하기 위함
		colinear += (ray_normal1-Origin).Length()/2;
		
		double a = (ray_normal1.X()*ray_normal1.X())/(seg->m_radius.X()*seg->m_radius.X())
				+ (ray_normal1.Y()*ray_normal1.Y())/(seg->m_radius.Y()*seg->m_radius.Y())
				+ (ray_normal1.Z()*ray_normal1.Z())/(seg->m_radius.Z()*seg->m_radius.Z());

		a2 = (ray_normald2[0]*ray_normald2[0])/(seg->m_NeiborsegPointer[i]->m_radius.X()*seg->m_NeiborsegPointer[i]->m_radius.X())
				+ (ray_normald2[1]*ray_normald2[1])/(seg->m_NeiborsegPointer[i]->m_radius.Y()*seg->m_NeiborsegPointer[i]->m_radius.Y())
				+ (ray_normald2[2]*ray_normald2[2])/(seg->m_NeiborsegPointer[i]->m_radius.Z()*seg->m_NeiborsegPointer[i]->m_radius.Z());

		hit = sqrt(a)/a;
		hit2 = sqrt(a2)/a2;

		real_length += (hit + hit2)*weight;
	}
	error = (length - real_length)*(length - real_length)*colinear/(seg->m_maxradialerr*seg->m_maxradialerr);//(seg->m_numNeighbor);
	return error;
}



int radiicount = 0;
int centercount = 0;
int radiicountSum = 0;
int centercountSum = 0;

double radiiOPT_radialPrj(const double* x, segment *seg)
{
	double radierr = 0;
	double norerr = 0;
	double errV =0;
	R3Point ray_origin(0,0,0);
	R3Vector ray_normal1(0,0,0);
	R3Point rx;
	double hit = 0;
	double result = 0;
	int numpoint = seg->m_numpoint;
	double diag = seg->m_mesh->BBox().DiagonalLength();
	//normal error+
	R3Vector Mnormal;
	double E[3];//Ellipsoid normal;
	//printf("radiierr :");
	for(int i = 0; i<seg->m_InnerMeshVertex.size(); i++)
	{

		R3Point p = seg->m_OriMat*R3Point(seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).X() - seg->m_com.X(),
			seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).Y() - seg->m_com.Y(), 
			seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).Z() - seg->m_com.Z());
			
		ray_normal1.Reset(p.X(), p.Y(), p.Z());
		ray_normal1.Normalize();

		double a = (ray_normal1.X()*ray_normal1.X())/(x[0]*x[0])
				+ (ray_normal1.Y()*ray_normal1.Y())/(x[1]*x[1])
				+ (ray_normal1.Z()*ray_normal1.Z())/(x[2]*x[2]);

		double b = (2*ray_origin.X()*ray_normal1.X())/(x[0]*x[0])
				+ (2*ray_origin.Y()*ray_normal1.Y())/(x[1]*x[1])
				+ (2*ray_origin.Z()*ray_normal1.Z())/(x[2]*x[2]);

		double c = (ray_origin.X()*ray_origin.X())/(x[0]*x[0])
				+ (ray_origin.Y()*ray_origin.Y())/(x[1]*x[1])
				+ (ray_origin.Z()*ray_origin.Z())/(x[2]*x[2])
				- 1;

		double d = ((b*b)-(4*a*c));			
		if ( d < 0 ) { continue; }

		hit = sqrt(a)/a;

		rx = ray_origin + ray_normal1*hit;

		radierr += (( (rx-p).Length()*(rx-p).Length() ) / (seg->m_maxradialerr*seg->m_maxradialerr))*seg->weight[0];
		result += (( (rx-p).Length()*(rx-p).Length() ) / (seg->m_maxradialerr*seg->m_maxradialerr))*seg->weight[0];

		//normal error +
		E[0] = (2*hit*ray_normal1.X())/(x[0]*x[0]);
		E[1] = (2*hit*ray_normal1.Y())/(x[1]*x[1]);
		E[2] = (2*hit*ray_normal1.Z())/(x[2]*x[2]);
		
		Mnormal = seg->m_OriMat*seg->m_mesh->VertexNormal(seg->m_InnerMeshVertex[i]);

		double w[3] = {Mnormal.X(), Mnormal.Y(), Mnormal.Z()};
		norerr += computeVectorAngle(E, w)*seg->weight[1];
		result += computeVectorAngle(E, w)*seg->weight[1];

		//result += oneringDistError_radii(x, seg)*seg->weight[2];

		
	}
	result += oneringDistError_radii(x, seg)*seg->weight[2];

	errV += seg->weight[3]*(seg->m_volume - 4*3.141592*x[0]*x[1]*x[2]/3.0f)*(seg->m_volume - 4*3.141592*x[0]*x[1]*x[2]/3.0f)/(seg->m_volume*seg->m_volume);
		result += seg->weight[3]*(seg->m_volume - 4*3.141592*x[0]*x[1]*x[2]/3.0f)*(seg->m_volume - 4*3.141592*x[0]*x[1]*x[2]/3.0f)/(seg->m_volume*seg->m_volume);
	//printf("radial : %f, norerr : %f, errV : %f \n", radierr, norerr, errV);

	return result;
}

double centerOPT_radialPrj(const double* x, segment *seg)
{
	R3Point ray_origin(0,0,0);
	R3Vector ray_normal1(0,0,0);
	R3Point rx;
	R3Point tmp;
	double hit = 0;
	double hitsecond = 0;
	double result = 0;
	double E[3];//Ellipsoid normal;
	int numpoint = seg->m_numpoint;
	double diag = seg->m_mesh->BBox().DiagonalLength();
	//normal error+
	R3Vector Mnormal;

	for(int i = 0; i<seg->m_InnerMeshVertex.size(); i++)
	{

		R3Point p = seg->m_OriMat*R3Point(seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).X() - seg->m_com.X(),
			seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).Y() - seg->m_com.Y(), 
			seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).Z() - seg->m_com.Z());
		
		tmp.Reset(p.X()-x[0], p.Y()-x[1], p.Z()-x[2]);
		ray_normal1.Reset(p.X()-x[0], p.Y()-x[1], p.Z()-x[2]);
		ray_normal1.Normalize();

		double a = (ray_normal1.X()*ray_normal1.X())/(seg->m_radius.X()*seg->m_radius.X())
				+ (ray_normal1.Y()*ray_normal1.Y())/(seg->m_radius.Y()*seg->m_radius.Y())
				+ (ray_normal1.Z()*ray_normal1.Z())/(seg->m_radius.Z()*seg->m_radius.Z());

		double b = (2*ray_origin.X()*ray_normal1.X())/(seg->m_radius.X()*seg->m_radius.X())
				+ (2*ray_origin.Y()*ray_normal1.Y())/(seg->m_radius.Y()*seg->m_radius.Y())
				+ (2*ray_origin.Z()*ray_normal1.Z())/(seg->m_radius.Z()*seg->m_radius.Z());

		double c = (ray_origin.X()*ray_origin.X())/(seg->m_radius.X()*seg->m_radius.X())
				+ (ray_origin.Y()*ray_origin.Y())/(seg->m_radius.Y()*seg->m_radius.Y())
				+ (ray_origin.Z()*ray_origin.Z())/(seg->m_radius.Z()*seg->m_radius.Z())
				- 1;

		double d = ((b*b)-(4*a*c));			
		if ( d < 0 ) { continue; }

		hit = sqrt(a)/a;

		rx = ray_origin + ray_normal1*hit;
		result += (((rx-tmp).Length()*(rx-tmp).Length())/(seg->m_maxradialerr*seg->m_maxradialerr))*seg->weight[0];

		//normal error +
		E[0] = (2*hit*ray_normal1.X())/(seg->m_radius.X()*seg->m_radius.X());
		E[1] = (2*hit*ray_normal1.Y())/(seg->m_radius.Y()*seg->m_radius.Y());
		E[2] = (2*hit*ray_normal1.Z())/(seg->m_radius.Z()*seg->m_radius.Z());
		
		Mnormal = seg->m_OriMat*seg->m_mesh->VertexNormal(seg->m_InnerMeshVertex[i]);

		double w[3] = {Mnormal.X(), Mnormal.Y(), Mnormal.Z()};
		result += computeVectorAngle(E, w)*seg->weight[1];

		//result += oneringDistError_center(x, seg)*seg->weight[2];

		
	}
	result += oneringDistError_center(x, seg)*seg->weight[2];
	//double vol = 4*3.141592*(seg->m_mesh->BBox().XLength()/2)*(seg->m_mesh->BBox().YLength()/2)*(seg->m_mesh->BBox().ZLength()/2)/3.0f;
	//	result += (seg->m_volume - 4*3.141592*seg->m_radius.X()*seg->m_radius.Y()*seg->m_radius.Z()/3.0f)*
	//				(seg->m_volume - 4*3.141592*seg->m_radius.X()*seg->m_radius.Y()*seg->m_radius.Z()/3.0f)*seg->weight[3]/vol/vol;
	result += seg->weight[3]*(seg->m_volume - 4*3.141592*seg->m_radius.X()*seg->m_radius.Y()*seg->m_radius.Z()/3.0f)*
						(seg->m_volume - 4*3.141592*seg->m_radius.X()*seg->m_radius.Y()*seg->m_radius.Z()/3.0f)/(seg->m_volume*seg->m_volume);

	return result;
}

double radiif(unsigned n, const double* x, double* grad, void* f_data)
{
	radiicount++;
	segment *seg = (segment *) f_data;

	double result = 0.0;
	double r_g[3];
	adolcOPT aopt;
	aopt.test_Radii(seg, x, r_g);

	if (grad) {
        grad[0] = r_g[0];
        grad[1] = r_g[1];
		grad[2] = r_g[2];
    }
	
    return radiiOPT_radialPrj(x, seg);
}

double centerf(unsigned n, const double* x, double* grad, void* f_data)
{
	centercount++;
	segment *seg = (segment *) f_data;

	double result = 0.0;
	double r_g[3];
	adolcOPT aopt;
	aopt.test_Center(seg, x, r_g);

	if (grad) {
        grad[0] = r_g[0];
        grad[1] = r_g[1];
		grad[2] = r_g[2];
    }

    return centerOPT_radialPrj(x, seg);
}

void Opt::radiioptimization(segment *seg)
{
	radiicount = 0;

	double x[3] = {seg->m_radius.X(), seg->m_radius.Y() , seg->m_radius.Z()}; /* some initial guess */
	double minf; /* the minimum objective value, upon return */
	double m_lb[3] = {x[0]/5, x[1]/5 , x[2]/5};//{seg->m_radius.X()/2.0f, seg->m_radius.Y()/2.0f , seg->m_radius.Z()/2.0f};
	double m_ub[3] = {seg->m_radius.X()*1.5, seg->m_radius.Y()*1.5 , seg->m_radius.Z()*1.5};
	double tol[3] = {1e-4, 1e-4, 1e-4};
	m_total_rad_err += radiiOPT_radialPrj(x, seg);

	//lb[i] ≤ x[i] ≤ ub[i] for 0 ≤ i < n. 
	nlopt_set_lower_bounds(radiiopt, m_lb);
	//nlopt_set_upper_bounds(radiiopt, m_ub);
	nlopt_set_min_objective(radiiopt, radiif, seg);

	//nlopt_set_xtol_rel (radiiopt, 1e-4);
	//nlopt_set_maxeval(radiiopt, 30);
	nlopt_set_xtol_abs(radiiopt, tol);

	if (nlopt_optimize(radiiopt, x, &minf) < 0) 
	{
		printf("nlopt failed!\n");
	}
	else 
	{
		m_total_rad_err -= minf;
	}
	//큰반지름 작은반지름 비율 고정
	/*
	double max = FLT_MIN;
	int maxidx = 0;
	double min = FLT_MAX;
	int minidx = 2;
	for(int i = 0; i<3; i++)
	{
		if(x[i]>max)
		{
			max = x[i];
			maxidx = i;
		}

		if(x[i]<min)
		{
			min = x[i];
			minidx = i;
		}
	}*/

	//if(x[maxidx] > x[minidx]*2)
	//	x[minidx] = x[maxidx]/2.0f;

	printf("found minimum after %d evaluations\n", radiicount);
	radiicountSum += radiicount;

	seg->m_radius.Reset(x[0], x[1], x[2]);
	nlopt_destroy(radiiopt);
}

void Opt::centeroptimization(segment *seg)
{
	//std::cout<< "OPT" << " ";

	centercount = 0;
	double x[3] = {0, 0 ,0}; /* some initial guess */
	double minf; /* the minimum objective value, upon return */
	double m_lb[3] = {-seg->m_radius.X() , -seg->m_radius.Y(), -seg->m_radius.Z()};
	double m_ub[3] = {seg->m_radius.X(), seg->m_radius.Y() , seg->m_radius.Z()};
	double tol[3] = {1e-4, 1e-4, 1e-4};
	m_total_cen_err += centerOPT_radialPrj(x, seg);

	nlopt_set_lower_bounds(centeropt, m_lb);
	nlopt_set_upper_bounds(centeropt, m_ub);
	nlopt_set_min_objective(centeropt, centerf, seg);
	//nlopt_set_xtol_rel (centeropt, 1.0e-4);
	//nlopt_set_maxeval(centeropt, 10);
	nlopt_set_xtol_abs(centeropt, tol);

	if (nlopt_optimize(centeropt, x, &minf) < 0) 
	{
		printf("nlopt failed!\n");
	}
	else 
	{
		m_total_cen_err -= minf;
	}

	printf("found minimum after %d evaluations\n", centercount);
	centercountSum += centercount;

	R3Point tls;
	tls = seg->inv*R3Point(x[0], x[1], x[2]);

	double cx = seg->m_com.X() + tls.X();
	double cy = seg->m_com.Y() + tls.Y();
	double cz = seg->m_com.Z() + tls.Z();
	seg->m_com.Reset(cx, cy, cz);
	nlopt_destroy(centeropt);
}

void Opt::printErr()
{
	std::cout<<"total radii Err"<<std::endl;
	std::cout<<"optimization"<< m_total_rad_err << std::endl;
}

void Opt::all_optimization(OPObject *obj, double threshold)
{
	double Rerr_reduced = 0;
	double Cerr_reduced = 0;
	double Oerr_reduced = 0;
	double diff = INT_MAX;
	double total_diff = 0;
	int iter = 0;

	//optimization
	/*
	while(diff > threshold)
	{
		if(iter > 2)
			break;

		Rerr_reduced = 0;
		Cerr_reduced = 0;
		Oerr_reduced = 0;
		std::cout<<"Optimization : "<<std::endl;*/
		for(int i = 0; i<obj->segmentList.size(); i++)
		{
			Opt opttest;
			printf("*%d",i);
			if(!obj->segmentList[i]->m_isOptimized)
			{

				opttest.centeroptimization(obj->segmentList[i]);
				Cerr_reduced += opttest.m_total_cen_err;

				opttest.radiioptimization(obj->segmentList[i]);
				Rerr_reduced += opttest.m_total_rad_err;

			}

		}
/*
		diff = Rerr_reduced + Cerr_reduced;
		total_diff += diff;
		iter ++;
	}*/

		std::cout<<"everage iter:" << (radiicountSum + centercountSum)/ obj->segmentList.size()/2<< std::endl;
}

