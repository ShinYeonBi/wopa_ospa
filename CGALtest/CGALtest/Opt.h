#pragma once
#include "adolcOPT.h"
#include <math.h>
#include <nlopt.h>
#include <stdio.h>
#include <vector>
#include "OPObject.h"
#include "segment.h"
#include "commonType.h"
#include "R3Graphics/R3Graphics.h"

class Opt
{
public:
	Opt(void);
	~Opt(void);
	Opt(int p_totalpoint, OPObject *p_op);

	double lb[3];
	double ub[3];
	nlopt_opt radiiopt;
	nlopt_opt centeropt;
	nlopt_opt oriopt;
	int totalpoint;
	OPObject *op;
	std::vector <R3Point> m_rotMeshpoints;
	std::vector <R3Point> m_rotnormal;

	double m_total_rad_err;
	double m_total_cen_err;
	double m_total_ori_err;

	adolcOPT m_adolc;

	void radiioptimization(segment *seg);
	void centeroptimization(segment *seg);
	void printErr();
	void all_optimization(OPObject *obj, double threshold);

};

