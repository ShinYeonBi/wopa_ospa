#include "WOPA.h"


WOPA::WOPA(void)
{
}


WOPA::~WOPA(void)
{
}

void WOPA::setWOPAdata(std::vector<OPObject*> *p_Obj, Polyhedron *p_me, RNArray<R3Mesh *> *p_mes)
{
	p_ObjectList = p_Obj; 
	p_mesh = p_me;
	p_meshes = p_mes;
	th = (*p_ObjectList)[0]->mesh->BBox().DiagonalLength()*0.015;
}

void WOPA::WOPAmergeobj()
{
	printf("WOPAmergeobj\n");
	(*p_ObjectList)[1]->setCOMnPointList();
	(*p_ObjectList)[2]->setCOMnPointList();
	(*p_ObjectList)[3]->setCOMnPointList();
	(*p_ObjectList)[4]->setCOMnPointList();
	(*p_ObjectList)[5]->setCOMnPointList();
	
	for(int j = 0;j<(*p_ObjectList).size(); j++)
	{
		for(int i = 0; i<(*p_ObjectList)[j]->segmentList.size(); i++)
		{
			(*p_ObjectList)[j]->segmentList[i]->computeEigenv();
			(*p_ObjectList)[j]->segmentList[i]->intersect_radialPrj();
		}
	}
	
	merge_obj(1, 2);
	
	
	(*p_ObjectList)[0]->setCOMnPointList();

	for(int i = 0; i<(*p_ObjectList)[0]->segmentList.size(); i++)
	{
		(*p_ObjectList)[0]->segmentList[i]->computeEigenv();
		(*p_ObjectList)[0]->segmentList[i]->intersect_radialPrj();
	}
	
	merge_obj(0, 3);

	(*p_ObjectList)[0]->setCOMnPointList();

	for(int i = 0; i<(*p_ObjectList)[0]->segmentList.size(); i++)
	{
		(*p_ObjectList)[0]->segmentList[i]->computeEigenv();
		(*p_ObjectList)[0]->segmentList[i]->intersect_radialPrj();
	}
	
	merge_obj(0, 4);

	(*p_ObjectList)[0]->setCOMnPointList();

	for(int i = 0; i<(*p_ObjectList)[0]->segmentList.size(); i++)
	{
		(*p_ObjectList)[0]->segmentList[i]->computeEigenv();
		(*p_ObjectList)[0]->segmentList[i]->intersect_radialPrj();
	}
	
	merge_obj(0, 5);

	(*p_ObjectList)[0]->setCOMnPointList();

	for(int i = 0; i<(*p_ObjectList)[0]->segmentList.size(); i++)
	{
		(*p_ObjectList)[0]->segmentList[i]->computeEigenv();
		(*p_ObjectList)[0]->segmentList[i]->intersect_radialPrj();
		//OPObjectList[0]->segmentList[i]->rayEllipsoidIntersect();
	}

	WOPAoptimization();
	WOPASplit();
}

void WOPA::merge_obj(int mergeidx1, int mergeidx2)
{
	std::vector <int> errsegList;
	std::vector <int> mergesegList;
	bool isin = false;
	bool isin2 = false;
	double diag;
	OPObject * obresult = new OPObject(p_mesh->size_of_facets(), (*p_meshes)[0]);

	//idx1의 세그먼트 에러 검사. 에러가 일정값 이상인 세그먼트의 번호는 에러세그먼트 리스트에, 나머지번호는 머지 세그먼트 리스트에
	for(int i = 0; i<(*p_ObjectList)[mergeidx1]->segmentList.size(); i++)
	{
		diag = (*p_ObjectList)[mergeidx1]->mesh->BBox().DiagonalLength()/120.0f;
		std::cout<<"diag: "<<diag<<std::endl;
		if((*p_ObjectList)[mergeidx1]->segmentList[i]->m_everageEllipsoidDist > diag)
		{
			errsegList.push_back(i);
		}
		else
		{
			mergesegList.push_back(i);
		}
	}

	//?
	for(int i = 0; i< errsegList.size(); i++)
		std::cout<<"errseg : " << errsegList[i]<<std::endl;

	
	for(int i = 0; i<p_mesh->size_of_facets(); i++)
	{
		for(int j = 0; j<errsegList.size(); j++)//메쉬의 페이스별로 검사. 그 페이스가 idx1에서 에러가 높은 세그먼트에 속하면, isin = true.
		{
			if(errsegList[j] == (*p_ObjectList)[mergeidx1]->seg_arr[i])
			{
				isin = true;
				break;
			}
		}

		if(isin)//그 페이스가 idx1에서 에러가 높은 세그먼트에 속해있는(추가로 세그멘테이션 해야할) 페이스인 경우
		{
			//머지된 세그먼트 = idx2의 세그먼트 번호 + idx1의 세그먼트 번호(번호 중복 막기위함)
			obresult->seg_arr[i] = (*p_ObjectList)[mergeidx2]->seg_arr[i]+ (*p_ObjectList)[mergeidx1]->segmentList.size();

			for(int k = 0; k<mergesegList.size(); k++)
			{
				if(mergesegList[k] == (*p_ObjectList)[mergeidx2]->seg_arr[i]+ (*p_ObjectList)[mergeidx1]->segmentList.size()) //그 번호가 머지세그먼트 리스트에 이미 들어간경우는 pushback안함
				{
					isin2 = true;
					break;
				}
			}

			if(!isin2)
				mergesegList.push_back((*p_ObjectList)[mergeidx2]->seg_arr[i]+ (*p_ObjectList)[mergeidx1]->segmentList.size());//그 번호가 머지세그먼트 리스트에 안들어간 경우는 pushback
		}

		else//그 페이스가 idx1에서 에러가 낮은(세그멘테이션 할 필요 없는)페이스인 경우
		{
			obresult->seg_arr[i] = (*p_ObjectList)[mergeidx1]->seg_arr[i] ;//결과 오브젝트에 그대로 세그먼트 번호 복사
		}

		isin = false;
		isin2 = false;
	}

	std::cout<<"mergesegList.size : " << mergesegList.size()<<std::endl;

	
	obresult->segmentList.clear();//결과 오브젝트에 세그먼트 재배치
	for(int i = 0; i< mergesegList.size(); i++)
	{
		segment * tmp = new segment(p_mesh->size_of_vertices(), (*p_meshes)[0]);
		obresult->segmentList.push_back(tmp);
	}

	///re_arr
	int j = 0;
	for(int i = 0; i< p_mesh->size_of_facets(); i++)//메쉬의 모든 페이스에 대해 검사
	{
		for(j = 0; j<mergesegList.size(); j++)
		{
			if(mergesegList[j] == obresult->seg_arr[i])//머지 세그먼트 리스트의 세그먼트인덱스 값과 결과오브젝트의 페이스의 세그먼트인덱스값이 같으면 빠져나옴
				break;
		}
		obresult->seg_arr[i] = j;//0~머지세그먼트 리스트 개수 사이의 정수값으로 세그먼트 인덱스번호 재배치함.
	}

	(*p_ObjectList)[0]->segmentList.clear();
	//merge결과 복사
	for(int i = 0; i<p_mesh->size_of_facets(); i++)
	{
		(*p_ObjectList)[0]->seg_arr[i] = obresult->seg_arr[i];
	}

	for(int i = 0; i<obresult->segmentList.size(); i++)
	{
		(*p_ObjectList)[0]->segmentList.push_back(obresult->segmentList[i]);
	}

	std::cout<<"test: "<<obresult->segmentList.size()<<std::endl;
}

void WOPA::WOPAoptimization()
{

	////////optimization, split////////
	
	//Opt opttest;
	opttest.all_optimization((*p_ObjectList)[0], 1e-3);
	std::cout<<"opti1 end"<<std::endl;
}

void WOPA::WOPASplit()
{
	///split
	(*p_ObjectList)[0]->WOPAsearchSplitSegment(th);
	std::cout<<"split1 end"<<std::endl;
	for(int i = 0; i<(*p_ObjectList)[0]->segmentList.size(); i++)
	{
		(*p_ObjectList)[0]->segmentList[i]->intersect_radialPrj();
	}
	opttest.all_optimization((*p_ObjectList)[0], 1e-3);
	std::cout<<"opti2 end"<<std::endl;
}

void WOPA::setthreshold(double p_th)
{
	th = p_th;
}
