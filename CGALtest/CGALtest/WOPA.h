#pragma once
#include "Opt.h"
#include "OPObject.h"
#include "R3Graphics/R3Graphics.h"
#include <vector>
#include <iostream>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polyhedron_items_with_id_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/mesh_segmentation.h>
#include <CGAL/property_map.h>
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Polyhedron_3<K, CGAL::Polyhedron_items_with_id_3>  Polyhedron;
class WOPA
{
public:
	WOPA(void);
	~WOPA(void);
	void setWOPAdata(std::vector<OPObject*> *p_Obj, Polyhedron *p_me, RNArray<R3Mesh *> *p_mes);

	std::vector <OPObject*> *p_ObjectList;
	Polyhedron *p_mesh;
	RNArray<R3Mesh *> *p_meshes;
	Opt opttest;
	double th;
	//void WOPAsegmentation(Polyhedron mesh, RNArray meshes);
	void WOPAmergeobj();
	void merge_obj(int mergeidx1, int mergeidx2);
	void WOPAoptimization();
	void WOPASplit();
	void setthreshold(double p_th);
};

