#include "adolcOPT.h"


adolcOPT::adolcOPT(void)
{
}


adolcOPT::~adolcOPT(void)
{
}


adouble adolcOPT::radii_objectiveF(adouble* x, segment *seg)
{
	R3Point ray_origin(0,0,0);
	R3Vector ray_normal1(0,0,0);
	double ray_normald[3] = {0,0,0};
	adouble rxd[3] = {0,0,0};
	adouble hit = 0;
	adouble result = 0;
	int numpoint = seg->m_numpoint;
	double diag = seg->m_mesh->BBox().DiagonalLength();
	//normal error+
	R3Vector Mnormal;
	adouble E[3];//Ellipsoid normal;

	for(int i = 0; i<seg->m_InnerMeshVertex.size(); i++)
	{

		R3Point p = seg->m_OriMat*R3Point(seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).X() - seg->m_com.X(),
			seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).Y() - seg->m_com.Y(), 
			seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).Z() - seg->m_com.Z());
			
		ray_normal1.Reset(p.X(), p.Y(), p.Z());
		ray_normal1.Normalize();
		ray_normald[0] = ray_normal1.X();	ray_normald[1] = ray_normal1.Y();	ray_normald[2] = ray_normal1.Z();

		adouble a = (ray_normal1.X()*ray_normal1.X())/(x[0]*x[0])
				+ (ray_normal1.Y()*ray_normal1.Y())/(x[1]*x[1])
				+ (ray_normal1.Z()*ray_normal1.Z())/(x[2]*x[2]);

		adouble b = (2*ray_origin.X()*ray_normal1.X())/(x[0]*x[0])
				+ (2*ray_origin.Y()*ray_normal1.Y())/(x[1]*x[1])
				+ (2*ray_origin.Z()*ray_normal1.Z())/(x[2]*x[2]);

		adouble c = (ray_origin.X()*ray_origin.X())/(x[0]*x[0])
				+ (ray_origin.Y()*ray_origin.Y())/(x[1]*x[1])
				+ (ray_origin.Z()*ray_origin.Z())/(x[2]*x[2])
				- 1;

		adouble d = ((b*b)-(4*a*c));			
		if ( d < 0 ) { continue; }

		hit = sqrt(a)/a;

		rxd[0] = hit*ray_normald[0]-p.X();	rxd[1] = hit*ray_normald[1]-p.Y();	rxd[2] = hit*ray_normald[2]-p.Z();
		result += ((rxd[0]*rxd[0] + rxd[1]*rxd[1] + rxd[2]*rxd[2])/(seg->m_maxradialerr*seg->m_maxradialerr))*seg->weight[0];

		//normal error +
		
		E[0] = (2*hit*ray_normald[0])/(x[0]*x[0]);
		E[1] = (2*hit*ray_normald[1])/(x[1]*x[1]);
		E[2] = (2*hit*ray_normald[2])/(x[2]*x[2]);
		
		Mnormal = seg->m_OriMat*seg->m_mesh->VertexNormal(seg->m_InnerMeshVertex[i]);

		double w[3] = {Mnormal.X(), Mnormal.Y(), Mnormal.Z()};
		result += computeVectorAngle(E, w)*seg->weight[1];//diag;

		//result += oneringDistError_radii(x, seg)*seg->weight[2];
		
		
	}
	result += oneringDistError_radii(x, seg)*seg->weight[2];

	//double vol = 4*3.141592*(seg->m_mesh->BBox().XLength()/2)*(seg->m_mesh->BBox().YLength()/2)*(seg->m_mesh->BBox().ZLength()/2)/3.0f;
	//		result += (seg->m_volume - 4*3.141592*x[0]*x[1]*x[2]/3.0f)*(seg->m_volume - 4*3.141592*x[0]*x[1]*x[2]/3.0f)*seg->weight[3]/vol/vol;
	result += seg->weight[3]*(seg->m_volume - 4*3.141592*x[0]*x[1]*x[2]/3.0f)*(seg->m_volume - 4*3.141592*x[0]*x[1]*x[2]/3.0f)/(seg->m_volume*seg->m_volume);
	

	return result;
}

adouble adolcOPT::center_objectiveF(adouble* x, segment *seg)
{
	R3Point ray_origin(0,0,0);
	R3Vector ray_normal1(0,0,0);
	adouble ray_normald[3] = {0,0,0};
	adouble rxd[3] = {0,0,0};
	adouble E[3];//Ellipsoid normal;
	adouble tmpd[3] = {0,0,0};//원래 메쉬 버텍스에서 x만큼 이동한 위치 저장
	adouble hit = 0;
	adouble result = 0;
	adouble norm;
	int numpoint = seg->m_numpoint;
	double diag = seg->m_mesh->BBox().DiagonalLength();
	//normal error+
	R3Vector Mnormal;

	for(int i = 0; i<seg->m_InnerMeshVertex.size(); i++)
	{
		R3Point p = seg->m_OriMat*R3Point(seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).X() - seg->m_com.X(),
			seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).Y() - seg->m_com.Y(), 
			seg->m_mesh->VertexPosition(seg->m_InnerMeshVertex[i]).Z() - seg->m_com.Z());
		tmpd[0] = p.X()-x[0];	tmpd[1] = p.Y()-x[1];	tmpd[2] = p.Z()-x[2];  

		ray_normald[0] = p.X()-x[0];	ray_normald[1] = p.Y()-x[1];	ray_normald[2] = p.Z()-x[2];
		norm = sqrt(ray_normald[0]*ray_normald[0] + ray_normald[1]*ray_normald[1] + ray_normald[2]*ray_normald[2]);
		ray_normald[0] /= norm;	ray_normald[1] /= norm;	ray_normald[2] /= norm;//normalize

		adouble a = (ray_normald[0]*ray_normald[0])/(seg->m_radius.X()*seg->m_radius.X())
				+ (ray_normald[1]*ray_normald[1])/(seg->m_radius.Y()*seg->m_radius.Y())
				+ (ray_normald[2]*ray_normald[2])/(seg->m_radius.Z()*seg->m_radius.Z());

		adouble b = (2*ray_origin.X()*ray_normald[0])/(seg->m_radius.X()*seg->m_radius.X())
				+ (2*ray_origin.Y()*ray_normald[1])/(seg->m_radius.Y()*seg->m_radius.Y())
				+ (2*ray_origin.Z()*ray_normald[2])/(seg->m_radius.Z()*seg->m_radius.Z());

		adouble c = (ray_origin.X()*ray_origin.X())/(seg->m_radius.X()*seg->m_radius.X())
				+ (ray_origin.Y()*ray_origin.Y())/(seg->m_radius.Y()*seg->m_radius.Y())
				+ (ray_origin.Z()*ray_origin.Z())/(seg->m_radius.Z()*seg->m_radius.Z())
				- 1;

		adouble d = ((b*b)-(4*a*c));			
		if ( d < 0 ) { continue; }

		hit = sqrt(a)/a;

		rxd[0] = hit*ray_normald[0]-tmpd[0];	rxd[1] = hit*ray_normald[1]-tmpd[1];	rxd[2] = hit*ray_normald[2]-tmpd[2];
		result += ((rxd[0]*rxd[0] + rxd[1]*rxd[1] + rxd[2]*rxd[2])/(seg->m_maxradialerr*seg->m_maxradialerr))*seg->weight[0];

		//normal error +
		
		E[0] = (2*hit*ray_normald[0])/(seg->m_radius.X()*seg->m_radius.X());
		E[1] = (2*hit*ray_normald[1])/(seg->m_radius.Y()*seg->m_radius.Y());
		E[2] = (2*hit*ray_normald[2])/(seg->m_radius.Z()*seg->m_radius.Z());
		
		Mnormal = seg->m_OriMat*seg->m_mesh->VertexNormal(seg->m_InnerMeshVertex[i]);

		double w[3] = {Mnormal.X(), Mnormal.Y(), Mnormal.Z()};
		result += computeVectorAngle(E, w)*seg->weight[1];

		//result += oneringDistError_center(x, seg)*seg->weight[2];

		
	}
	result += oneringDistError_center(x, seg)*seg->weight[2];
	//double vol = 4*3.141592*(seg->m_mesh->BBox().XLength()/2)*(seg->m_mesh->BBox().YLength()/2)*(seg->m_mesh->BBox().ZLength()/2)/3.0f;
	//		result += (seg->m_volume - 4*3.141592*seg->m_radius.X()*seg->m_radius.Y()*seg->m_radius.Z()/3.0f)*
	//					(seg->m_volume - 4*3.141592*seg->m_radius.X()*seg->m_radius.Y()*seg->m_radius.Z()/3.0f)*seg->weight[3]/vol/vol;
	result += seg->weight[3]*(seg->m_volume - 4*3.141592*seg->m_radius.X()*seg->m_radius.Y()*seg->m_radius.Z()/3.0f)*
						(seg->m_volume - 4*3.141592*seg->m_radius.X()*seg->m_radius.Y()*seg->m_radius.Z()/3.0f)/(seg->m_volume*seg->m_volume);
	

	return result;
}

void adolcOPT::testmain(segment * seg)
{
	double x[3], y;
	adouble ax[3], ay; // declaration of active variables
	x[0]=0.5; x[1]=1;	x[2]=2;
}

void adolcOPT::test_Radii(segment * seg, const double *p_x, double *p_g)
{
	double x[3], y;
	adouble ax[3], ay; // declaration of active variables
	x[0]=p_x[0]; x[1]=p_x[1];	x[2]=p_x[2];

	trace_on(1); // starting active section
	ax[0]<<=x[0]; ax[1]<<=x[1]; ax[2]<<=x[2];// marking independent variables
	ay=radii_objectiveF(ax, seg); // function evaluation
	ay>>=y; // marking dependend variables
	trace_off(); // ending active section


	gradient(1,3,x,p_g);
}

void adolcOPT::test_Center(segment * seg, const double *p_x, double *p_g)
{
	double x[3], y;
	adouble ax[3], ay; 
	x[0]=p_x[0]; x[1]=p_x[1];	x[2]=p_x[2];

	trace_on(2); 
	ax[0]<<=x[0]; ax[1]<<=x[1]; ax[2]<<=x[2];
	ay=center_objectiveF(ax, seg); 
	ay>>=y; 
	trace_off(); 

	gradient(2,3,x,p_g);
}


adouble adolcOPT::computeVectorAngle(adouble * v, double *w)
{
	adouble vlength = sqrt((v[0]*v[0]) + (v[1]*v[1]) + (v[2]*v[2]));
	double wlength = sqrt((w[0]*w[0]) + (w[1]*w[1]) + (w[2]*w[2]));

    v[0] /= vlength;
    v[1] /= vlength;
    v[2] /= vlength;

	w[0] /= wlength;
    w[1] /= wlength;
    w[2] /= wlength;

	adouble Dot = (v[0]*w[0]) + (v[1]*w[1]) + (v[2]*w[2]);
	
	return (-(Dot-1)/2.0f);//*(-(Dot-1)/2.0f);
}

adouble adolcOPT::oneringDistError_radii(adouble* x, segment *seg)
{
	double length = 0;
	adouble real_length = 0;
	double ray_normald[3] = {0,0,0};
	double ray_normald2[3] = {0,0,0};
	R3Vector ray_normal1(0,0,0);
	R3Point ray_origin(0,0,0);
	adouble hit = 0;
	double hit2 = 0;
	double a2;
	adouble error;
	double sumdist = 0;
	double weight = 0;

	for(int i = 0; i<seg->m_NeiborsegPointer.size(); i++)
	{
		sumdist += 1/(seg->m_NeiborsegPointer[i]->m_com - seg->m_com).Length();
	}

	for(int i = 0; i<seg->m_NeiborsegPointer.size(); i++)
	{
		//나를 기준으로 이웃의 중심 좌표 구함
		R3Point p = seg->m_OriMat*R3Point(seg->m_NeiborsegPointer[i]->m_com.X() - seg->m_com.X(),
										seg->m_NeiborsegPointer[i]->m_com.Y() - seg->m_com.Y(), 
										seg->m_NeiborsegPointer[i]->m_com.Z() - seg->m_com.Z());
			
		ray_normal1.Reset(p.X(), p.Y(), p.Z());
		weight = 1/ray_normal1.Length()/sumdist;
		length += ray_normal1.Length()*weight;//*1.3f;//나와 이웃중심까지의 거리

		ray_normal1.Normalize();
		ray_normald[0] = ray_normal1.X();	ray_normald[1] = ray_normal1.Y();	ray_normald[2] = ray_normal1.Z();//이웃의 중심을 향하는 ray
		ray_normald2[0] = -ray_normal1.X();	ray_normald2[1] = -ray_normal1.Y();	ray_normald2[2] = -ray_normal1.Z();//이웃에서 나를 향하는 ray


		adouble a = (ray_normal1.X()*ray_normal1.X())/(x[0]*x[0])
				+ (ray_normal1.Y()*ray_normal1.Y())/(x[1]*x[1])
				+ (ray_normal1.Z()*ray_normal1.Z())/(x[2]*x[2]);

		a2 = (ray_normald2[0]*ray_normald2[0])/(seg->m_NeiborsegPointer[i]->m_radius.X()*seg->m_NeiborsegPointer[i]->m_radius.X())
				+ (ray_normald2[1]*ray_normald2[1])/(seg->m_NeiborsegPointer[i]->m_radius.Y()*seg->m_NeiborsegPointer[i]->m_radius.Y())
				+ (ray_normald2[2]*ray_normald2[2])/(seg->m_NeiborsegPointer[i]->m_radius.Z()*seg->m_NeiborsegPointer[i]->m_radius.Z());

		hit = sqrt(a)/a;
		hit2 = sqrt(a2)/a2;

		real_length += (hit + hit2)*weight;
	}
	error = (length - real_length)*(length - real_length)/(seg->m_maxradialerr*seg->m_maxradialerr);//(seg->m_numNeighbor);
	return error;
}

adouble adolcOPT::oneringDistError_center(adouble* x, segment *seg)
{
	adouble length = 0;
	adouble real_length = 0;
	adouble ray_normald[3];
	adouble ray_normald2[3];
	adouble hit = 0;
	adouble hit2 = 0;
	adouble a2;
	adouble error;
	adouble v[3];
	adouble vlength ;
	adouble sumdist = 0;
	adouble weight = 0;
	R3Vector Origin(0,0,0);
	adouble colinear = 1;
	
	for(int i = 0; i<seg->m_NeiborsegPointer.size(); i++)
	{
		
		//나를 기준으로 이웃의 중심 좌표 구함
		R3Point p = seg->m_OriMat*R3Point(seg->m_NeiborsegPointer[i]->m_com.X() - seg->m_com.X(),
										seg->m_NeiborsegPointer[i]->m_com.Y() - seg->m_com.Y(), 
										seg->m_NeiborsegPointer[i]->m_com.Z() - seg->m_com.Z());
			
		v[0] = p.X()-x[0]; v[1] =  p.Y()-x[1] ; v[2] = p.Z()-x[2];
		vlength = sqrt((v[0]*v[0]) + (v[1]*v[1]) + (v[2]*v[2]));
		sumdist += 1/vlength;
	}

	for(int i = 0; i<seg->m_NeiborsegPointer.size(); i++)
	{
		//나를 기준으로 이웃의 중심 좌표 구함
		R3Point p = seg->m_OriMat*R3Point(seg->m_NeiborsegPointer[i]->m_com.X() - seg->m_com.X(),
										seg->m_NeiborsegPointer[i]->m_com.Y() - seg->m_com.Y(), 
										seg->m_NeiborsegPointer[i]->m_com.Z() - seg->m_com.Z());

		Origin.Reset(p.X(), p.Y(), p.Z());	
		Origin.Normalize();//이동한 위치가 연결직선상인지 확인하기 위함
			
		v[0] = p.X()-x[0]; v[1] =  p.Y()-x[1] ; v[2] = p.Z()-x[2];
		vlength = sqrt((v[0]*v[0]) + (v[1]*v[1]) + (v[2]*v[2]));
		weight = 1/vlength/sumdist;
		length += vlength*weight*1.1f;//나와 이웃중심까지의 거리

		//ray_normal1.Normalize();
		v[0] /= vlength;
		v[1] /= vlength;
		v[2] /= vlength;

		ray_normald[0] = v[0];	ray_normald[1] = v[1];	ray_normald[2] = v[2];//이웃의 중심을 향하는 ray
		ray_normald2[0] = -v[0];	ray_normald2[1] = -v[1];	ray_normald2[2] = -v[2];//이웃에서 나를 향하는 ray

		//이동한 위치가 연결직선상인지 확인하기 위함
		adouble co[3] = {ray_normald[0]-Origin.X(), ray_normald[1]-Origin.Y(), ray_normald[2]-Origin.Z()};
		colinear += sqrt((co[0]*co[0]) + (co[1]*co[1]) + (co[2]*co[2]))/2;

		adouble a = (ray_normald[0]*ray_normald[0])/(seg->m_radius.X()*seg->m_radius.X())
				+ (ray_normald[1]*ray_normald[1])/(seg->m_radius.Y()*seg->m_radius.Y())
				+ (ray_normald[2]*ray_normald[2])/(seg->m_radius.Z()*seg->m_radius.Z());

		a2 = (ray_normald2[0]*ray_normald2[0])/(seg->m_NeiborsegPointer[i]->m_radius.X()*seg->m_NeiborsegPointer[i]->m_radius.X())
				+ (ray_normald2[1]*ray_normald2[1])/(seg->m_NeiborsegPointer[i]->m_radius.Y()*seg->m_NeiborsegPointer[i]->m_radius.Y())
				+ (ray_normald2[2]*ray_normald2[2])/(seg->m_NeiborsegPointer[i]->m_radius.Z()*seg->m_NeiborsegPointer[i]->m_radius.Z());

		hit = sqrt(a)/a;
		hit2 = sqrt(a2)/a2;

		real_length += (hit + hit2)*weight;
	}
	error = (length - real_length)*(length - real_length)*colinear/(seg->m_maxradialerr*seg->m_maxradialerr);//(seg->m_numNeighbor);
	return error;
}
/*
adouble adolcOPT::oneringeveDistError(adouble* x, segment *seg)//평균 거리와 비슷
{
	adouble error = 0;
	adouble real_error = 0;
	adouble tmp[3];

	for(int i = 0; i<seg->m_numNeighbor; i++)
	{
		//나를 기준으로 이웃의 중심 좌표 구함
		R3Point p = seg->m_OriMat*R3Point(seg->m_NeiborsegPointer[i]->m_com.X() - seg->m_com.X(),
										seg->m_NeiborsegPointer[i]->m_com.Y() - seg->m_com.Y(), 
										seg->m_NeiborsegPointer[i]->m_com.Z() - seg->m_com.Z());

		tmp[0] = p.X() - x[0];
		tmp[1] = p.Y() - x[1]; 
		tmp[2] = p.Z() - x[2];

		error += sqrt((tmp[0]*tmp[0]) + (tmp[1]*tmp[1]) + (tmp[2]*tmp[2]));
	}
	
	real_error = seg->everage1ringDist*seg->m_numNeighbor - error;

	return 0.1*real_error*real_error;
}*/
