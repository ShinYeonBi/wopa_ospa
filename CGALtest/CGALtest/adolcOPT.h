#pragma once
#include "adolc/adolc.h" 
#include "adolc/adouble.h"
#include "OPObject.h"
#include "segment.h"
#include "commonType.h"
#include "R3Graphics/R3Graphics.h"
class adolcOPT
{
public:
	adolcOPT(void);
	~adolcOPT(void);

	adouble radii_objectiveF(adouble* x, segment *seg);
	adouble center_objectiveF(adouble* x, segment *seg);
	adouble computeVectorAngle(adouble * v, double *w);
	adouble * computeEllipsoidNormal(adouble * v);
	adouble oneringDistError_radii(adouble* x, segment *seg);
	adouble oneringDistError_center(adouble* x, segment *seg);
	adouble compute1ringrayDist();
	adouble oneringeveDistError(adouble* x, segment *seg);

	void testmain(segment * seg);
	void test_Radii(segment * seg, const double *p_x, double *p_g);
	void test_Center(segment * seg, const double *p_x, double *p_g);

};

