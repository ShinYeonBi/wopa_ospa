/*
	Written by Ji-yong Kwon (rinthel@gmail.com)
	This source implements typical data types and methods for elementary linear algebra.
	This source have been used in Sketch-Based Modeling (SBM) Project.
	The license of this project is following MIT license.
*/

#include "commonType.h"
#ifdef __COMMONTYPE_USE_GL__
#include <windows.h>
#include <gl/glew.h>
#endif
#include <stdio.h>
#include <string.h>
#include <math.h>

void GetFilenameExtFromFullpath(const char *source, char *target)
{
	int i;
	int sl = (int)strlen(source);
	int st = 0;
	for(i = sl-1; i >= 0; i--)
	{
		if(source[i] == '\\' || source[i] == '/')
		{
			st = i + 1;
			break;
		}
	}
	for(i = st; i < sl; i++)
	{
		target[i-st] = source[i];
	}
	target[i-st] = NULL;
}

#ifdef __COMMONTYPE_USE_GL__
void SetDrawToScreen(int x, int y)
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0, x, y, 0, -x, x);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
}

void ReleaseDrawToScreen()
{
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

void DrawCubicBezierInterp(TVect2 &pt0, TVect2 &pt1, TVect2 &pt2, TVect2 &pt3)
{
	static const TMat4 bezierMat = {
		-1.0f, 3.0f, -3.0f, 1.0f,
		3.0f, -6.0f, 3.0f, 0.0f,
		-3.0f, 3.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f, 0.0f
	};
	TVect4 controlPtX, controlPtY;
	TVect4 paramVect, catVectX, catVectY;
	int i;

	controlPtX[0] = pt0[0];		controlPtX[1] = pt1[0];
	controlPtX[2] = pt2[0];		controlPtX[3] = pt3[0];
	controlPtY[0] = pt0[1];		controlPtY[1] = pt1[1];
	controlPtY[2] = pt2[1];		controlPtY[3] = pt3[1];
	sbmMat4Vect4Product(catVectX, (float*)bezierMat, controlPtX);
	sbmMat4Vect4Product(catVectY, (float*)bezierMat, controlPtY);

	glBegin(GL_LINE_STRIP);
	for(i = 0; i <= SBM_CUBIC_SUBDIVIDE_LEVEL; i++)
	{
		float t = (float)i / (float)SBM_CUBIC_SUBDIVIDE_LEVEL;
		paramVect[3] = 1.0f;
		paramVect[2] = t;
		paramVect[1] = t*t;
		paramVect[0] = t*t*t;
		glVertex2f(sbmVect4Dot(paramVect, catVectX), sbmVect4Dot(paramVect, catVectY));
	}
	glEnd();
}

inline void DrawCatmullRom(TVect2 &pt0, TVect2 &pt1, TVect2 &pt2, TVect2 &pt3)
{
	static const TMat4 catmullRomMat = {
		-0.5f, 1.5f, -1.5f, 0.5f,
		1.0f, -2.5f, 2.0f, -0.5f,
		-0.5f, 0.0f, 0.5f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f
	};
	TVect4 controlPtX, controlPtY;
	TVect4 paramVect, catVectX, catVectY;
	int i;

	controlPtX[0] = pt0[0];		controlPtX[1] = pt1[0];
	controlPtX[2] = pt2[0];		controlPtX[3] = pt3[0];
	controlPtY[0] = pt0[1];		controlPtY[1] = pt1[1];
	controlPtY[2] = pt2[1];		controlPtY[3] = pt3[1];
	sbmMat4Vect4Product(catVectX, (float*)catmullRomMat, controlPtX);
	sbmMat4Vect4Product(catVectY, (float*)catmullRomMat, controlPtY);

	glBegin(GL_LINE_STRIP);
	for(i = 0; i <= SBM_CUBIC_SUBDIVIDE_LEVEL; i++)
	{
		float t = (float)i / (float)SBM_CUBIC_SUBDIVIDE_LEVEL;
		paramVect[3] = 1.0f;
		paramVect[2] = t;
		paramVect[1] = t*t;
		paramVect[0] = t*t*t;
		glVertex2f(sbmVect4Dot(paramVect, catVectX), sbmVect4Dot(paramVect, catVectY));
	}
	glEnd();
}

inline void DrawFourPtInterp(TVect2 &pt0, TVect2 &pt1, TVect2 &pt2, TVect2 &pt3)
{
	static const TMat4 fourPtMat = {
		-4.5f, 13.5f, -13.5f, 4.5f,
		9.0f, -22.5f, 18.0f, -4.5f,
		-5.5f, 9.0f, -4.5f, 1.0f,
		1.0f, 0.0f, 0.0f, 0.0f
	};
	TVect4 controlPtX, controlPtY;
	TVect4 paramVect, catVectX, catVectY;
	int i;

	controlPtX[0] = pt0[0];		controlPtX[1] = pt1[0];
	controlPtX[2] = pt2[0];		controlPtX[3] = pt3[0];
	controlPtY[0] = pt0[1];		controlPtY[1] = pt1[1];
	controlPtY[2] = pt2[1];		controlPtY[3] = pt3[1];
	sbmMat4Vect4Product(catVectX, (float*)fourPtMat, controlPtX);
	sbmMat4Vect4Product(catVectY, (float*)fourPtMat, controlPtY);

	glBegin(GL_LINE_STRIP);
	for(i = 0; i <= SBM_CUBIC_SUBDIVIDE_LEVEL; i++)
	{
		float t = (float)i / (float)SBM_CUBIC_SUBDIVIDE_LEVEL;
		paramVect[3] = 1.0f;
		paramVect[2] = t;
		paramVect[1] = t*t;
		paramVect[0] = t*t*t;
		glVertex2f(sbmVect4Dot(paramVect, catVectX), sbmVect4Dot(paramVect, catVectY));
	}
	glEnd();
}

void SetDrawShadow(TVect4 &lightPos, TVect4 &plane)
{
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_ZERO, GL_SRC_COLOR);
	glEnable(GL_BLEND);
	glEnable(GL_CULL_FACE);

	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glDepthMask(GL_FALSE);
	glClearStencil(0);
	glEnable(GL_STENCIL_TEST);
	glStencilFunc(GL_NEVER, 0x0, 0xFF);
	glStencilOp(GL_INCR, GL_INCR, GL_INCR);
	glClear(GL_STENCIL_BUFFER_BIT);

	TMat4 projMat, glProjMat;
	sbmMat4Projection(projMat, lightPos, plane);
	sbmMat4Transpose(projMat, glProjMat);

	glPushMatrix();
	glMultMatrixf(glProjMat);
}

void ReleaseDrawShadow()
{
	glPopMatrix();

	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glDepthMask(GL_TRUE);
	glStencilFunc(GL_NOTEQUAL, 0x00, 0xFF);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

	SetDrawToScreen(1, 1);
	glBegin(GL_QUADS);
	glVertex2f(0.0f, 0.0f);
	glVertex2f(0.0f, 1.0f);
	glVertex2f(1.0f, 1.0f);
	glVertex2f(1.0f, 0.0f);
	glEnd();
	ReleaseDrawToScreen();

	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glDepthMask(GL_TRUE);
	glDisable(GL_STENCIL_TEST);

	glDisable(GL_BLEND);
	glDisable(GL_CULL_FACE);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
}
#endif