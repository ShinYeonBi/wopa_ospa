/*
	Written by Ji-yong Kwon (rinthel@gmail.com)
	This source implements typical data types and methods for elementary linear algebra.
	This source have been used in Sketch-Based Modeling (SBM) Project.
	The license of this project is following MIT license.
*/

#ifndef __COMMONTYPE_H__
#define __COMMONTYPE_H__

#include <math.h>

#ifndef M_PI
#define M_PI	3.141592653589793238
#endif
#ifndef M_PIF
#define M_PIF	3.141592653589793238f
#endif
#define FRAME_PER_SEC	30
#define FRAME_RATE		0.03333333333f

#define SBM_RGB(r, g, b)		(((int)(r)<<24) | ((int)(g)<<16) | ((int)(b)<<8))
#define SBM_RGBA(r, g, b, a)	(((int)(r)<<24) | ((int)(g)<<16) | ((int)(b)<<8) | ((int)(a)))
#define SBM_R(rgb)			((0xff000000 & rgb) >> 24)
#define SBM_G(rgb)			((0x00ff0000 & rgb) >> 16)
#define SBM_B(rgb)			((0x0000ff00 & rgb) >> 8)
#define SBM_A(rgb)			((0x000000ff & rgb))

#define SBM_MIN(x, y)		((x) < (y) ? (x) : (y))
#define SBM_MAX(x, y)		((x) > (y) ? (x) : (y))
#define SBM_CLAMP(x, a, b)	(SBM_MIN(SBM_MAX((x), (a)), (b)))

#define SBM_SQUARE(x)		((x)*(x))

#define SBM_CUBIC_SUBDIVIDE_LEVEL	10

#define SBM_FALSE	0
#define SBM_TRUE	1


typedef float TVect2[2];
typedef float TVect3[3];
typedef float TVect4[4];

typedef float TMat2[4];
typedef float TMat3[9];
typedef float TMat4[16];

typedef enum {
	SNS_EVENT_CLOSE,
	SNS_EVENT_FAR
} ESNSEventType;

inline float sbmReciprocalSqrt(float x);

inline void sbmMat3Zero(float *z);
inline void sbmMat3Identity(float *z);
inline void sbmMat3Translation(float *z, float x, float y);
inline void sbmMat3NonUniformScale(float *z, float x, float y);
inline void sbmMat3Rotation(float *z, float rad);
inline void sbmMat3Product(float *z, float *x, float *y);
inline void sbmMat3Vect3Product(float *z, float *x, float *y);
inline void sbmMat3Copy(float *source, float *target);
inline void sbmMat3Add(float *z, float *x, float *y);
inline void sbmMat3Sub(float *z, float *x, float *y);
inline void sbmMat3Scalar(float *x, float scale);
inline void sbmMat3Scalar(float *z, float *x, float scale);
inline void sbmMat3Transpose(float *x);
inline void sbmMat3Transpose(float *source, float *target);
inline void sbmMat3RenormalizeRotation(float *mat);
inline void sbmMat3HomoRigidInverse(float *source, float *target);
inline void sbmMat3HomoAffineInverse(float *source, float *target);
inline void sbmMat3Inverse(float *source, float *target);
inline void sbmMat3ToMat4(float *s, float *t);
inline float sbmMat3OneNorm(float *x);
inline float sbmMat3InfNorm(float *x);
inline float sbmMat3PolarDecomposition(float *sourceMat, float *uMat, float *pMat, float tolerance=1e-6f);

inline void sbmVect3Zero(float *vect);
inline void sbmVect3Set(float *vect, float x, float y, float z);
inline void sbmVect3Add(float *z, float *x, float *y);
inline void sbmVect3Sub(float *z, float *x, float *y);
inline float sbmVect3Dot(float *x, float *y);
inline void sbmVect3Cross(float *z, float *x, float *y);
inline void sbmVect3Outer(TMat3 &z, float *x, float *y);
inline float sbmVect3Norm(float *x);
inline float sbmVect3ApproxNorm(float *x);
inline float sbmVect3SquareNorm(float *x);
inline void sbmVect3Scalar(float *x, float s);
inline void sbmVect3Scalar(float *z, float *x, float s);
inline void sbmVect3ToVect4(float *source, float *target);
inline void sbmVect3LinearInterp(float *z, float *x, float *y, float t);
inline void sbmVect3FromMat4Trans(float *vect, float *mat);
inline float sbmVect3Distance(float *x, float *y);

inline void sbmVect4Zero(float *vect);
inline void sbmVect4ZeroPoint(float *vect);
inline void sbmVect4Set(float *vect, float x, float y, float z, float w);
inline void sbmVect3Copy(float *source, float *target);
inline void sbmVect4Copy(float *source, float *target);
inline float sbmVect4Dot(TVect4 &x, TVect4 &y);
inline void sbmVect4Zero(TVect4 &z);
inline void sbmVect4Add(TVect4 &z, TVect4 &x, TVect4 &y);
inline void sbmVect4ScalarMultAdd(TVect4 &z, TVect4 &x, TVect4 &y, float s);
inline float sbmVect4Norm(TVect4 &x);
inline void sbmVect4Scalar(TVect4 &x, float s);
inline void sbmVect4Scalar(TVect4 &z, TVect4 &x, float s);
inline void sbmVect4LinearInterp(float *z, float *x, float *y, float t);

inline void sbmMat4Zero(float *mat);
inline void sbmMat4Identity(float *mat);
inline void sbmVect3LinearInterp(float *z, float *x, float *y, float t);
inline void sbmMat4Add(float *z, float *x, float *y);
inline void sbmMat4Sub(float *z, float *x, float *y);
inline void sbmMat4Scalar(float *z, float s);
inline void sbmMat4Scalar(float *z, float *x, float s);
inline void sbmMat4Copy(float *source, float *target);
inline void sbmMat4Rotation(float *mat, int axis, float rad);
inline void sbmMat4Translation(float *mat, float x, float y, float z);
inline void sbmMat4SetRotation(float *mat, int axis, float rad);
inline void sbmMat4SetTranslation(float *mat, float x, float y, float z);
inline void sbmMat4NonUniformScale(float *mat, float x, float y, float z);
inline void sbmMat4Vect3Product(float *z, float *x, float *y);
inline void sbmMat4Vect4Product(float *z, float *x, float *y);
inline void sbmMat4Product(float *z, float *x, float *y);
inline void sbmMat4Transpose(float *mat);
inline void sbmMat4Transpose(float *source, float *target);
inline void sbmMat4BasisTranspose(float *mat, float *b1, float *b2, float *b3);
inline void sbmMat4HomoAffineInverse(float *source, float *target);
inline void sbmMat4HomoRigidInverse(float *source, float *target);
inline void sbmMat4Projection(TMat4 &projMat, TVect4 &focus, TVect4 &plane);
inline void sbmMat4ToEulerZXY(TMat4 &mat, TVect3 &euler);
inline void sbmMat4ToTranslation(TMat4 &mat, TVect3 &trans);
inline void sbmMat4AxisAngle(TMat4 &mat, TVect3 &axis, float angle);
inline void sbmMat4ToMat3(float *source, float *target);

#define sbmQuatZero		sbmVect4ZeroPoint
inline void sbmQuatToMat3(TVect4 &q, TMat3 &mat);
inline void sbmQuatToMat4(TVect4 &q, TMat4 &mat);
inline void sbmQuatFromAxisAngle(TVect4 &q, TVect3 &axis, float angle);
inline void sbmQuatExp(TVect3 &fromVect, TVect4 &toQuat);
inline void sbmQuatLog(TVect4 &fromQuat, TVect3 &toVect);
inline void sbmQuatMult(TVect4 &z, TVect4 &x, TVect4 &y);
inline void sbmQuatConj(TVect4 &src, TVect4 &tgt);
inline void sbmQuatConj(TVect4 &src);
inline void sbmQuatInverse(TVect4 &src, TVect4 &tgt);
inline float sbmQuatNorm(TVect4 &x);
inline float sbmQuatDot(TVect4 &x, TVect4 &y);
inline void sbmMat4ToQuat(TMat4 &mat, TVect4 &q);
inline void sbmMat4ToQuatForDQ(TMat4 &mat, TVect4 &q);
inline void sbmMat3ToQuat(TMat3 &mat, TVect4 &q);
inline void sbmQuatSLERP(TVect4 &z, TVect4 &x, TVect4 &y, float t);
inline void sbmDualQuatToMat4(TMat4 &mat, TVect4 &qn, TVect4 &qd);

inline void sbmVect2Set(TVect2 &vect, float x, float y);
inline void sbmVect2Add(float *z, float *x, float *y);
inline void sbmVect2Sub(float *z, float *x, float *y);
inline float sbmVect2Inner(float *x, float *y);
inline float sbmVect2Cross(float *x, float *y);
inline float sbmVect2Norm(float *x);
inline void sbmVect2Scalar(float *x, float s);
inline void sbmVect2Scalar(float *z, float *x, float s);
inline void sbmMat2Add(float *z, float *x, float *y);
inline void sbmMat2Sub(float *z, float *x, float *y);
inline void sbmMat2Vect2Product(float *z, float *x, float *y);
inline void sbmMat2Inverse(float *mat);
inline void sbmMat2Inverse(float *source, float *target);

void GetFilenameExtFromFullpath(const char *source, char *target);


class CPoint2i
{
public:
	CPoint2i()
	{
		m_pt[0] = 0;
		m_pt[1] = 0;
	}
	~CPoint2i()
	{
	}
	inline int GetX() {return m_pt[0];}
	inline int GetY() {return m_pt[1];}
	inline int* GetPt() {return m_pt;}
	inline void GetPt(int &x, int &y)
	{
		x = m_pt[0];
		y = m_pt[1];
	}
	inline void SetX(int x) {m_pt[0] = x;}
	inline void SetY(int y) {m_pt[1] = y;}
	inline void SetPt(int x, int y)
	{
		m_pt[0] = x;
		m_pt[1] = y;
	}
	inline void SetPt(int *pt)
	{
		m_pt[0] = pt[0];
		m_pt[1] = pt[1];
	}
	CPoint2i& operator+ (CPoint2i &pt)
	{
		CPoint2i resultPt;
		resultPt.m_pt[0] = m_pt[0] + pt.m_pt[0];
		resultPt.m_pt[1] = m_pt[1] + pt.m_pt[1];
	}
	inline float SquareDIst(CPoint2i &pt)
	{
		int xx = m_pt[0]- pt.GetX();
		int yy = m_pt[1]- pt.GetY();
		return (float)(xx*xx + yy*yy);
	}
private:
	int m_pt[2];
};

#ifdef __COMMONTYPE_USE_GL__
void SetDrawToScreen(int x, int y);
void ReleaseDrawToScreen();
void DrawCubicBezierInterp(TVect2 &pt0, TVect2 &pt1, TVect2 &pt2, TVect2 &pt3);
void DrawCatmullRom(TVect2 &pt0, TVect2 &pt1, TVect2 &pt2, TVect2 &pt3);
void DrawFourPtInterp(TVect2 &pt0, TVect2 &pt1, TVect2 &pt2, TVect2 &pt3);

void SetDrawShadow(TVect4 &lightPos, TVect4 &plane);
void ReleaseDrawShadow();
#endif

#include "commonType.inl"

#endif
