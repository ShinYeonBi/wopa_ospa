/*
	Written by Ji-yong Kwon (rinthel@gmail.com)
	This source implements typical data types and methods for elementary linear algebra.
	This source have been used in Sketch-Based Modeling (SBM) Project.
	The license of this project is following MIT license.
*/

inline void sbmMat3Zero(float *z)
{
	z[0] = 0.0f;	z[1] = 0.0f;	z[2] = 0.0f;
	z[3] = 0.0f;	z[4] = 0.0f;	z[5] = 0.0f;
	z[6] = 0.0f;	z[7] = 0.0f;	z[8] = 0.0f;
}

inline void sbmMat3Identity(float *z)
{
	z[0] = 1.0f;	z[1] = 0.0f;	z[2] = 0.0f;
	z[3] = 0.0f;	z[4] = 1.0f;	z[5] = 0.0f;
	z[6] = 0.0f;	z[7] = 0.0f;	z[8] = 1.0f;
}

inline void sbmMat3Translation(float *z, float x, float y)
{
	z[0] = 1.0f;	z[1] = 0.0f;	z[2] = x;
	z[3] = 0.0f;	z[4] = 1.0f;	z[5] = y;
	z[6] = 0.0f;	z[7] = 0.0f;	z[8] = 1.0f;
}

inline void sbmMat3NonUniformScale(float *z, float x, float y)
{
	z[0] = x;		z[1] = 0.0f;	z[2] = 0.0f;
	z[3] = 0.0f;	z[4] = y;		z[5] = 0.0f;
	z[6] = 0.0f;	z[7] = 0.0f;	z[8] = 1.0f;
}

inline void sbmMat3Rotation(float *z, float rad)
{
	float c = (float)cos(rad);
	float s = (float)sin(rad);

	z[0] = c;		z[1] = -s;		z[2] = 0.0f;
	z[3] = s;		z[4] = c;		z[5] = 0.0f;
	z[6] = 0.0f;	z[7] = 0.0f;	z[8] = 1.0f;
}

inline void sbmMat3Product(float *z, float *x, float *y)
{
	int i, j, k;
	for(i = 0; i < 3; i++)
	{
		for(j = 0; j < 3; j++)
		{
			z[3*i+j] = 0.0f;
			for(k = 0; k < 3; k++)
				z[3*i+j] += (x[3*i+k]*y[3*k+j]);
		}
	}
}

inline void sbmMat3Vect3Product(float *z, float *x, float *y)
{
	z[0] = x[0]*y[0] + x[1]*y[1] + x[2]*y[2];
	z[1] = x[3]*y[0] + x[4]*y[1] + x[5]*y[2];
	z[2] = x[6]*y[0] + x[7]*y[1] + x[8]*y[2];
}

inline void sbmMat3Copy(float *source, float *target)
{
	int i;
	for(i = 0; i < 9; i++)
		target[i] = source[i];
}

inline void sbmMat3Add(float *z, float *x, float *y)
{
	int i;
	for(i = 0; i < 9; i++)
		z[i] = x[i] + y[i];
}

inline void sbmMat3Sub(float *z, float *x, float *y)
{
	int i;
	for(i = 0; i < 9; i++)
		z[i] = x[i] - y[i];
}

inline void sbmMat3Scalar(float *x, float scale)
{
	int i;
	for(i = 0; i < 9; i++)
		x[i] *= scale;
}

inline void sbmMat3Scalar(float *z, float *x, float scale)
{
	int i;
	for(i = 0; i < 9; i++)
		z[i] = x[i] * scale;
}

inline void sbmMat3Transpose(float *x)
{
	float temp;
	temp = x[1];	x[1] = x[3];	x[3] = temp;
	temp = x[2];	x[2] = x[6];	x[6] = temp;
	temp = x[5];	x[5] = x[7];	x[7] = temp;
}

inline void sbmMat3Transpose(float *source, float *target)
{
	target[0] = source[0];	target[1] = source[3];	target[2] = source[6];
	target[3] = source[1];	target[4] = source[4];	target[5] = source[7];
	target[6] = source[2];	target[7] = source[5];	target[8] = source[8];
}

inline void sbmMat3RenormalizeRotation(float *mat)
{
	float l = (float)(1.0 / sqrt(mat[0]*mat[0] + mat[3]*mat[3]));
	mat[0] *= l;
	mat[3] *= l;

	float direction = mat[0]*mat[4] - mat[1]*mat[3];
	if(direction > 0.0)
	{
		mat[1] = -mat[3];
		mat[4] = mat[0];
	}
	else
	{
		mat[1] = mat[3];
		mat[4] = -mat[0];
	}
}

inline void sbmMat3HomoRigidInverse(float *source, float *target)
{
	target[0] = source[0];
	target[1] = source[3];

	target[3] = source[1];
	target[4] = source[4];

	target[2] = -(source[0]*source[2] + source[3]*source[5]);
	target[5] = -(source[1]*source[2] + source[4]*source[5]);

	target[6] = 0.0f;
	target[7] = 0.0f;
	target[8] = 1.0f;
}

inline void sbmMat3HomoAffineInverse(float *source, float *target)
{
	float inv = 1.0f / (source[0]*source[4] - source[1]*source[3]);
	target[0] = inv*source[4];
	target[1] = -inv*source[1];

	target[3] = -inv*source[3];
	target[4] = inv*source[0];

	target[2] = -(target[0]*source[2] + target[1]*source[5]);
	target[5] = -(target[3]*source[2] + target[4]*source[5]);

	target[6] = 0.0f;
	target[7] = 0.0f;
	target[8] = 1.0f;
}

inline void sbmMat3Inverse(float *s, float *t)
{
	float det;
	t[0] = s[8]*s[4] - s[7]*s[5];
	t[1] = s[7]*s[2] - s[8]*s[1];
	t[2] = s[5]*s[1] - s[4]*s[2];

	t[3] = s[6]*s[5] - s[8]*s[3];
	t[4] = s[8]*s[0] - s[6]*s[2];
	t[5] = s[3]*s[2] - s[5]*s[0];

	t[6] = s[7]*s[3] - s[6]*s[4];
	t[7] = s[6]*s[1] - s[7]*s[0];
	t[8] = s[4]*s[0] - s[3]*s[1];

	det = 1.0f / (s[0]*t[0] + s[3]*t[1] + s[6]*t[2]);
	
	t[0] *= det;	t[1] *= det;	t[2] *= det;
	t[3] *= det;	t[4] *= det;	t[5] *= det;
	t[6] *= det;	t[7] *= det;	t[8] *= det;
}

inline void sbmMat3ToMat4(float *s, float *t)
{
	t[0] = s[0];	t[1] = s[1];	t[2] = s[2];	t[3] = 0.0f;
	t[4] = s[3];	t[5] = s[4];	t[6] = s[5];	t[7] = 0.0f;
	t[8] = s[6];	t[9] = s[7];	t[10] = s[8];	t[11] = 0.0f;
	t[12] = 0.0f;	t[13] = 0.0f;	t[14] = 0.0f;	t[15] = 1.0f;
}

inline float sbmMat3OneNorm(float *x)
{
	float norm = 0.0f;
	for(int i = 0; i < 3; i++)
		norm = SBM_MAX(fabsf(x[i  ]) + fabsf(x[i+3]) + fabsf(x[i+6]), norm);
	return norm;
}

inline float sbmMat3InfNorm(float *x)
{
	float norm = 0.0f;
	for(int i = 0; i < 3; i++)
		norm = SBM_MAX(fabsf(x[3*i  ]) + fabsf(x[3*i+1]) + fabsf(x[3*i+2]), norm);
	return norm;
}

inline float sbmMat3PolarDecomposition(float *sourceMat, float *orthoMat, float *symmetricMat, float tolerance)
{
	TMat3 eMat, uMat, uMatAdjT;
	float uMatOneNorm, uMatInfNorm, det, eMatOneNorm;
	sbmMat3Transpose(sourceMat, uMat);
	uMatOneNorm = sbmMat3OneNorm(uMat);
	uMatInfNorm = sbmMat3InfNorm(uMat);

	do 
	{
		float uMatAdjTOneNorm, uMatAdjTInfNorm, gamma, g1, g2;

		sbmVect3Cross(uMatAdjT  , uMat+3, uMat+6);
		sbmVect3Cross(uMatAdjT+3, uMat+6, uMat  );
		sbmVect3Cross(uMatAdjT+6, uMat  , uMat+3);

		det = sbmVect3Dot(uMat, uMatAdjT);
		if(det == 0.0)
			return det;			// singular case

		uMatAdjTOneNorm = sbmMat3OneNorm(uMatAdjT);
		uMatAdjTInfNorm = sbmMat3InfNorm(uMatAdjT);
		gamma = sqrtf(sqrtf((uMatAdjTOneNorm*uMatAdjTInfNorm) / (uMatOneNorm*uMatInfNorm)) / fabsf(det));
		g1 = gamma * 0.5f;
		g2 = 0.5f / (gamma * det);

		for(int i = 0; i < 9; i++)
		{
			eMat[i] = uMat[i];
			uMat[i] = g1*uMat[i] + g2*uMatAdjT[i];
			eMat[i] -= uMat[i];
		}

		eMatOneNorm = sbmMat3OneNorm(eMat);
		uMatOneNorm = sbmMat3OneNorm(uMat);
		uMatInfNorm = sbmMat3InfNorm(uMat);
	} while (eMatOneNorm > uMatOneNorm*tolerance);

	sbmMat3Transpose(uMat, orthoMat);
	sbmMat3Product(symmetricMat, uMat, sourceMat);

	symmetricMat[1] = symmetricMat[3] = (symmetricMat[1] + symmetricMat[3]) * 0.5f;
	symmetricMat[2] = symmetricMat[6] = (symmetricMat[2] + symmetricMat[6]) * 0.5f;
	symmetricMat[5] = symmetricMat[7] = (symmetricMat[5] + symmetricMat[7]) * 0.5f;

	return det;
}


inline void sbmMat4Zero(float *mat)
{
	mat[ 0]=0.0f;	mat[ 1]=0.0f;	mat[ 2]=0.0f;	mat[ 3]=0.0f;
	mat[ 4]=0.0f;	mat[ 5]=0.0f;	mat[ 6]=0.0f;	mat[ 7]=0.0f;
	mat[ 8]=0.0f;	mat[ 9]=0.0f;	mat[10]=0.0f;	mat[11]=0.0f;
	mat[12]=0.0f;	mat[13]=0.0f;	mat[14]=0.0f;	mat[15]=0.0f;
}

inline void sbmMat4Identity(float *mat)
{
	mat[ 0]=1.0f;	mat[ 1]=0.0f;	mat[ 2]=0.0f;	mat[ 3]=0.0f;
	mat[ 4]=0.0f;	mat[ 5]=1.0f;	mat[ 6]=0.0f;	mat[ 7]=0.0f;
	mat[ 8]=0.0f;	mat[ 9]=0.0f;	mat[10]=1.0f;	mat[11]=0.0f;
	mat[12]=0.0f;	mat[13]=0.0f;	mat[14]=0.0f;	mat[15]=1.0f;
}

inline void sbmMat4Add(float *z, float *x, float *y)
{
	int i;
	for(i = 0; i < 16; i++)
		z[i] = x[i] + y[i];
}

inline void sbmMat4Sub(float *z, float *x, float *y)
{
	int i;
	for(i = 0; i < 16; i++)
		z[i] = x[i] - y[i];
}

inline void sbmMat4Scalar(float *z, float s)
{
	int i;
	for(i = 0; i < 16; i++)
		z[i] *= s;
}

inline void sbmMat4Scalar(float *z, float *x, float s)
{
	int i;
	for(i = 0; i < 16; i++)
		z[i] = x[i] * s;
}

inline void sbmMat4Transpose(float *mat)
{
	float temp;
	int i, j;
	for(i = 0; i < 3; i++)
	{
		for(j = i+1; j < 4; j++)
		{
			temp = mat[4*i+j];
			mat[4*i+j] = mat[4*j+i];
			mat[4*j+i] = temp;
		}
	}
}

inline void sbmMat4Transpose(float *source, float *target)
{
	int i, j;
	for(i = 0; i < 4; i++)
	{
		for(j = 0; j < 4; j++)
		{
			target[4*i+j] = source[4*j+i];
		}
	}
}


inline void sbmMat4Copy(float *source, float *target)
{
	int i;
	for(i = 0; i < 16; i++)
		target[i] = source[i];
}
inline void sbmMat4Rotation(float *mat, int axis, float rad)
{
	float c = (float)cos(rad);
	float s = (float)sin(rad);
	sbmMat4Identity(mat);
	switch(axis)
	{
		case 0:		// x
			mat[ 5]=c;		mat[ 6]=-s;
			mat[ 9]=s;		mat[10]=c;
			break;
		case 1:		// y
			mat[ 0]=c;		mat[ 2]=s;
			mat[ 8]=-s;		mat[10]=c;
			break;
		case 2:		// x
			mat[ 0]=c;		mat[ 1]=-s;
			mat[ 4]=s;		mat[ 5]=c;
			break;
	}
}
inline void sbmMat4Translation(float *mat, float x, float y, float z)
{
	mat[ 0]=1.0f;	mat[ 1]=0.0f;	mat[ 2]=0.0f;	mat[ 3]=x;
	mat[ 4]=0.0f;	mat[ 5]=1.0f;	mat[ 6]=0.0f;	mat[ 7]=y;
	mat[ 8]=0.0f;	mat[ 9]=0.0f;	mat[10]=1.0f;	mat[11]=z;
	mat[12]=0.0f;	mat[13]=0.0f;	mat[14]=0.0f;	mat[15]=1.0f;
}

inline void sbmMat4SetRotation(float *mat, int axis, float rad)
{
	float c = (float)cos(rad);
	float s = (float)sin(rad);
	switch(axis)
	{
	case 0:		// x
		mat[ 0]=1.0f;	mat[ 1]=0.0f;	mat[ 2]=0.0f;
		mat[ 4]=0.0f;	mat[ 5]=c;		mat[ 6]=-s;
		mat[ 8]=0.0f;	mat[ 9]=s;		mat[10]=c;
		break;
	case 1:		// y
		mat[ 0]=c;		mat[ 1]=0.0f;	mat[ 2]=s;
		mat[ 4]=0.0f;	mat[ 5]=1.0f;	mat[ 6]=0.0f;
		mat[ 8]=-s;		mat[ 9]=0.0f;	mat[10]=c;
		break;
	case 2:		// z
		mat[ 0]=c;		mat[ 1]=-s;		mat[ 2]=0.0f;
		mat[ 4]=s;		mat[ 5]=c;		mat[ 6]=0.0f;
		mat[ 8]=0.0f;	mat[ 9]=0.0f;	mat[10]=1.0f;
		break;
	}
}
inline void sbmMat4SetTranslation(float *mat, float x, float y, float z)
{
	mat[ 3]=x;
	mat[ 7]=y;
	mat[11]=z;
}

inline void sbmMat4NonUniformScale(float *mat, float x, float y, float z)
{
	mat[ 0]=x;		mat[ 1]=0.0f;	mat[ 2]=0.0f;	mat[ 3]=0.0f;
	mat[ 4]=0.0f;	mat[ 5]=y;		mat[ 6]=0.0f;	mat[ 7]=0.0f;
	mat[ 8]=0.0f;	mat[ 9]=0.0f;	mat[10]=z;		mat[11]=0.0f;
	mat[12]=0.0f;	mat[13]=0.0f;	mat[14]=0.0f;	mat[15]=1.0f;
}

inline void sbmMat4Vect3Product(float *z, float *x, float *y)
{
	z[0] = x[ 0]*y[0] + x[ 1]*y[1] + x[ 2]*y[2] + x[ 3];
	z[1] = x[ 4]*y[0] + x[ 5]*y[1] + x[ 6]*y[2] + x[ 7];
	z[2] = x[ 8]*y[0] + x[ 9]*y[1] + x[10]*y[2] + x[11];
}

inline void sbmMat4Vect4Product(float *z, float *x, float *y)
{
	z[0] = x[ 0]*y[0] + x[ 1]*y[1] + x[ 2]*y[2] + x[ 3]*y[3];
	z[1] = x[ 4]*y[0] + x[ 5]*y[1] + x[ 6]*y[2] + x[ 7]*y[3];
	z[2] = x[ 8]*y[0] + x[ 9]*y[1] + x[10]*y[2] + x[11]*y[3];
	z[3] = x[12]*y[0] + x[13]*y[1] + x[14]*y[2] + x[15]*y[3];
}
inline void sbmMat4Product(float *z, float *x, float *y)
{
	int i, j, k;
	for(i = 0; i < 4; i++)
	{
		for(j = 0; j < 4; j++)
		{
			z[4*i+j] = 0.0f;
			for(k = 0; k < 4; k++)
				z[4*i+j] += (x[4*i+k]*y[4*k+j]);
		}
	}
}

inline void sbmMat4BasisTranspose(float *mat, float *b1, float *b2, float *b3)
{
	mat[ 0] = b1[0];	mat[ 1] = b1[1];	mat[ 2] = b1[2];	mat[ 3] = 0.0f;
	mat[ 4] = b2[0];	mat[ 5] = b2[1];	mat[ 6] = b2[2];	mat[ 7] = 0.0f;
	mat[ 8] = b3[0];	mat[ 9] = b3[1];	mat[10] = b3[2];	mat[11] = 0.0f;
	mat[12] = 0.0f;		mat[13] = 0.0f;		mat[14] = 0.0f;		mat[15] = 1.0f;
}

inline void sbmMat4HomoAffineInverse(float *s, float *t)
{
	float det;
	t[0] = s[10]*s[5] - s[9]*s[6];
	t[1] = s[9]*s[2] - s[10]*s[1];
	t[2] = s[6]*s[1] - s[5]*s[2];

	t[4] = s[8]*s[6] - s[10]*s[4];
	t[5] = s[10]*s[0] - s[8]*s[2];
	t[6] = s[4]*s[2] - s[6]*s[0];

	t[8] = s[9]*s[4] - s[8]*s[5];
	t[9] = s[8]*s[1] - s[9]*s[0];
	t[10] = s[5]*s[0] - s[4]*s[1];

	det = 1.0f / (s[0]*t[0] + s[4]*t[1] + s[8]*t[2]);
	
	t[0] *= det;	t[1] *= det;	t[2] *= det;
	t[4] *= det;	t[5] *= det;	t[6] *= det;
	t[8] *= det;	t[9] *= det;	t[10] *= det;

	t[3] = -(t[0]*s[3] + t[1]*s[7] + t[2]*s[11]);
	t[7] = -(t[4]*s[3] + t[5]*s[7] + t[6]*s[11]);
	t[11] = -(t[8]*s[3] + t[9]*s[7] + t[10]*s[11]);

	t[12] = t[13] = t[14] = 0.0f;
	t[15] = 1.0f;
}

inline void sbmMat4HomoRigidInverse(float *s, float *t)
{
	t[0] = s[0];	t[1] = s[4];	t[2] = s[8];
	t[4] = s[1];	t[5] = s[5];	t[6] = s[9];
	t[8] = s[2];	t[9] = s[6];	t[10] = s[10];

	t[3] = -(t[0]*s[3] + t[1]*s[7] + t[2]*s[11]);
	t[7] = -(t[4]*s[3] + t[5]*s[7] + t[6]*s[11]);
	t[11] = -(t[8]*s[3] + t[9]*s[7] + t[10]*s[11]);

	t[12] = t[13] = t[14] = 0.0f;
	t[15] = 1.0f;
}


inline void sbmMat4Projection(TMat4 &projMat, TVect4 &focus, TVect4 &plane)
{
	float dot;

	// 평면과 광원의 위치의 내적을 설정한다.
	dot = sbmVect4Dot(focus, plane);

	// 투명을 한다.
	// 첫 번째 열을 설정한다.
	projMat[0] = dot - focus[0] * plane[0];
	projMat[1] = 0.0f - focus[0] * plane[1];
	projMat[2] = 0.0f - focus[0] * plane[2];
	projMat[3] = 0.0f - focus[0] * plane[3];

	// 두 번째 열을 설정한다.
	projMat[4] = 0.0f - focus[1] * plane[0];
	projMat[5] = dot - focus[1] * plane[1];
	projMat[6] = 0.0f - focus[1] * plane[2];
	projMat[7] = 0.0f - focus[1] * plane[3];

	// 세 번째 열을 설정한다.
	projMat[8] = 0.0f - focus[2] * plane[0];
	projMat[9] = 0.0f - focus[2] * plane[1];
	projMat[10] = dot - focus[2] * plane[2];
	projMat[11] = 0.0f - focus[2] * plane[3];

	// 네 번째 열을 설정한다.
	projMat[12] = 0.0f - focus[3] * plane[0];
	projMat[13] = 0.0f - focus[3] * plane[1];
	projMat[14] = 0.0f - focus[3] * plane[2];
	projMat[15] = dot - focus[3] * plane[3];
}

inline void sbmMat4ToEulerZXY(TMat4 &mat, TVect3 &euler)
{
	if(mat[9] > 0.999999f)
	{
		euler[1] = 0.0f;
		euler[0] = (float)(M_PI * 0.5f);
		euler[2] = (float)atan2(mat[4], mat[0]);
	}
	else
	{
		euler[1] = (float)atan2(-mat[8], mat[10]);
		euler[0] = (float)asin(mat[9]);
		euler[2] = (float)atan2(-mat[1], mat[5]);
	}
}

inline void sbmMat4ToTranslation(TMat4 &mat, TVect3 &trans)
{
	trans[0] = mat[3];
	trans[1] = mat[7];
	trans[2] = mat[11];
}

inline void sbmMat4AxisAngle(TMat4 &mat, TVect3 &axis, float angle)
{
	//TVect4 quat;
	//sbmQuatFromAxisAngle(quat, axis, angle);
	//sbmQuatToMat4(quat, mat);
	float c, s, ic, is;
	c = (float)cos(angle);
	s = (float)sin(angle);
	ic = 1.0f - c;
	is = 1.0f - s;
	mat[0] = c + ic*axis[0]*axis[0];
	mat[1] = ic*axis[0]*axis[1] - s*axis[2];
	mat[2] = ic*axis[0]*axis[2] + s*axis[1];
	mat[3] = 0.0f;

	mat[4] = ic*axis[0]*axis[1] + s*axis[2];
	mat[5] = c + ic*axis[1]*axis[1];
	mat[6] = ic*axis[1]*axis[2] - s*axis[0];
	mat[7] = 0.0f;

	mat[8] = ic*axis[0]*axis[2] - s*axis[1];
	mat[9] = ic*axis[1]*axis[2] + s*axis[0];
	mat[10] = c + ic*axis[2]*axis[2];
	mat[11] = 0.0f;

	mat[12] = 0.0f;
	mat[13] = 0.0f;
	mat[14] = 0.0f;
	mat[15] = 1.0f;
}

inline void sbmMat4ToMat3(float *source, float *target)
{
	target[0] = source[0];	target[1] = source[1];	target[2] = source[2];
	target[3] = source[4];	target[4] = source[5];	target[5] = source[6];
	target[6] = source[8];	target[7] = source[9];	target[8] = source[10];
}


inline void sbmQuatToMat3(TVect4 &q, TMat3 &mat)
{
	// M = [1-2y^2-2z^2		2xy+2wz			2xz-2wy		]
	//	   [2xy-2wz			1-2x^2-2z^2		2yz+2wx		]
	//	   [2xz+2wy			2yz-2wx			1-2x^2-2y^2	]
	float x2 = q[0] + q[0];
	float y2 = q[1] + q[1];
	float z2 = q[2] + q[2];

	float xx2 = 2.0f*q[0]*q[0];
	float yy2 = 2.0f*q[1]*q[1];
	float zz2 = 2.0f*q[2]*q[2];

	mat[0] = 1.0f - yy2 - zz2;
	mat[3] = x2*q[1] + z2*q[3];
	mat[6] = x2*q[2] - y2*q[3];

	mat[1] = x2*q[1] - z2*q[3];
	mat[4] = 1.0f - xx2 - zz2;
	mat[7] = y2*q[2] + x2*q[3];

	mat[2] = x2*q[2] + y2*q[3];
	mat[5] = y2*q[2] - x2*q[3];
	mat[8] = 1.0f - yy2 - zz2;
}

inline void sbmQuatToMat4(TVect4 &q, TMat4 &mat)
{
	float x2 = q[0] + q[0];
	float y2 = q[1] + q[1];
	float z2 = q[2] + q[2];

	float xx2 = 2.0f*q[0]*q[0];
	float yy2 = 2.0f*q[1]*q[1];
	float zz2 = 2.0f*q[2]*q[2];

	mat[0] = 1.0f - yy2 - zz2;
	mat[4] = x2*q[1] + z2*q[3];
	mat[8] = x2*q[2] - y2*q[3];
	mat[12] = 0.0f;

	mat[1] = x2*q[1] - z2*q[3];
	mat[5] = 1.0f - xx2 - zz2;
	mat[9] = y2*q[2] + x2*q[3];
	mat[13] = 0.0f;

	mat[2] = x2*q[2] + y2*q[3];
	mat[6] = y2*q[2] - x2*q[3];
	mat[10] = 1.0f - xx2 - yy2;
	mat[14] = 0.0f;

	mat[3] = 0.0f;
	mat[7] = 0.0f;
	mat[11] = 0.0f;
	mat[15] = 1.0f;
}

inline void sbmQuatFromAxisAngle(TVect4 &q, TVect3 &axis, float angle)
{
	float s;
	s = (float)sin(angle*0.5f);
	q[0] = axis[0]*s;
	q[1] = axis[1]*s;
	q[2] = axis[2]*s;
	q[3] = (float)cos(angle*0.5f);
}

inline void sbmQuatExp(TVect3 &fromVect, TVect4 &toQuat)
{
	float norm = sbmVect3Norm(fromVect);
	if(norm == 0.0f)
	{
		toQuat[0] = 0.0f;		toQuat[1] = 0.0f;
		toQuat[2] = 0.0f;		toQuat[3] = 1.0f;
	}
	else
	{
		float sn = 1.0f / norm * (float)sin(norm);
		toQuat[0] = sn*fromVect[0];
		toQuat[1] = sn*fromVect[1];
		toQuat[2] = sn*fromVect[2];
		toQuat[3] = (float)cos(norm);
	}
}

inline void sbmQuatLog(TVect4 &fromQuat, TVect3 &toVect)
{
	float vect3Norm = sbmVect3Norm(fromQuat);
	if(vect3Norm == 0.0f)
	{
		toVect[0] = 0.0f;
		toVect[1] = 0.0f;
		toVect[2] = 0.0f;
	}
	else
	{
		float angle = (float)(atan2(vect3Norm, fromQuat[3]) / vect3Norm);
		toVect[0] = fromQuat[0] * angle;
		toVect[1] = fromQuat[1] * angle;
		toVect[2] = fromQuat[2] * angle;
	}
}

inline void sbmQuatMult(TVect4 &z, TVect4 &x, TVect4 &y)
{	
	z[0] = x[1]*y[2] - x[2]*y[1] + x[3]*y[0] + x[0]*y[3];	//x
	z[1] = x[2]*y[0] - x[0]*y[2] + x[3]*y[1] + x[1]*y[3];	//y
	z[2] = x[0]*y[1] - x[1]*y[0] + x[3]*y[2] + x[2]*y[3];	//z
	z[3] = x[3]*y[3] - x[0]*y[0] - x[1]*y[1] - x[2]*y[2];	//w
}

inline void sbmQuatConj(TVect4 &src, TVect4 &tgt)
{
	tgt[0] = -src[0];	tgt[1] = -src[1];
	tgt[2] = -src[2];	tgt[3] = src[3];
}

inline void sbmQuatConj(TVect4 &src)
{
	src[0] = -src[0];
	src[1] = -src[1];
	src[2] = -src[2];
}

inline void sbmQuatInverse(TVect4 &src, TVect4 &tgt)
{
	float norm = 1.0f / sbmQuatNorm(src);
	tgt[0] = -src[0] * norm;
	tgt[1] = -src[1] * norm;
	tgt[2] = -src[2] * norm;
	tgt[3] = src[3] * norm;
}

inline float sbmQuatNorm(TVect4 &x)
{
	return (float)sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]+x[3]*x[3]);
}

inline float sbmQuatDot(TVect4 &x, TVect4 &y)
{
	return x[0]*y[0] + x[1]*y[1] + x[2]*y[2] + x[3]*y[3];
}

inline void sbmQuatSLERP(TVect4 &z, TVect4 &x, TVect4 &y, float t)
{
	// slerp z = (1.0f-t)*x + t*y
	float theta, s, sinvt, st, dot;
	TVect4 tempy;

	dot = sbmQuatDot(x, y);
	if(dot > 0.0)
	{
		tempy[0] = y[0];		tempy[1] = y[1];
		tempy[2] = y[2];		tempy[3] = y[3];
	}
	else
	{
		tempy[0] = -y[0];		tempy[1] = -y[1];
		tempy[2] = -y[2];		tempy[3] = -y[3];
		dot = -dot;
	}
	if(dot >= 1.0f)
		dot = 1.0f;

	theta = (float)acos(dot);
	s = (float)sin(theta);
	if(fabs(s) < 0.00001)
	{
		z[0] = x[0];
		z[1] = x[1];
		z[2] = x[2];
		z[3] = x[3];
	}
	else
	{
		sinvt = (float)sin(theta*(1.0f-t)) / s;
		st = (float)sin(theta*t) / s;

		z[0] = sinvt*x[0] + st*tempy[0];
		z[1] = sinvt*x[1] + st*tempy[1];
		z[2] = sinvt*x[2] + st*tempy[2];
		z[3] = sinvt*x[3] + st*tempy[3];
	}
}


inline float sbmReciprocalSqrt(float x)
{
	long i;
	float y, r;
	y = x*0.5f;
	i = *(long*)(&x);
	i = 0x5f3759df - (i >> 1);
	r = *(float*)(&i);
	r = r*(1.5f - r*r*y);
	return r;
}

inline void sbmMat4ToQuat(TMat4 &mat, TVect4 &q)
{
	float s0, s1, s2;
	int k0, k1, k2, k3;
	if(mat[0] + mat[5] + mat[10] > 0.0f)
	{
		k0 = 3;		k1 = 2;		k2 = 1;		k3 = 0;
		s0 = 1.0f;	s1 = 1.0f;	s2 = 1.0f;
	}
	else if(mat[0] > mat[5] && mat[0] > mat[10])
	{
		k0 = 0;		k1 = 1;		k2 = 2;		k3 = 3;
		s0 = 1.0f;	s1 = -1.0f;	s2 = -1.0f;
	}
	else if(mat[5] > mat[10])
	{
		k0 = 1;		k1 = 0;		k2 = 3;		k3 = 2;
		s0 = -1.0f;	s1 = 1.0f;	s2 = -1.0f;
	}
	else
	{
		k0 = 2;		k1 = 3;		k2 = 0;		k3 = 1;
		s0 = -1.0f;	s1 = -1.0f;	s2 = 1.0f;
	}

	float t = s0*mat[0] + s1*mat[5] + s2*mat[10] + 1.0f;
	float s = sbmReciprocalSqrt(t) * 0.5f;

	q[k0] = s*t;
	q[k1] = (mat[1] - s2*mat[4]) * s;
	q[k2] = (mat[8] - s1*mat[2]) * s;
	q[k3] = (mat[6] - s0*mat[9]) * s;

	q[0] = -q[0];
	q[1] = -q[1];
	q[2] = -q[2];
}

inline void sbmMat4ToQuatForDQ(TMat4 &mat, TVect4 &q)
{
	sbmMat4ToQuat(mat, q);
}

inline void sbmMat3ToQuat(TMat3 &mat, TVect4 &q)
{
	float s0, s1, s2;
	int k0, k1, k2, k3;
	if(mat[0] + mat[4] + mat[8] > 0.0f)
	{
		k0 = 3;		k1 = 2;		k2 = 1;		k3 = 0;
		s0 = 1.0f;	s1 = 1.0f;	s2 = 1.0f;
	}
	else if(mat[0] > mat[4] && mat[0] > mat[8])
	{
		k0 = 0;		k1 = 1;		k2 = 2;		k3 = 3;
		s0 = 1.0f;	s1 = -1.0f;	s2 = -1.0f;
	}
	else if(mat[4] > mat[8])
	{
		k0 = 1;		k1 = 0;		k2 = 3;		k3 = 2;
		s0 = -1.0f;	s1 = 1.0f;	s2 = -1.0f;
	}
	else
	{
		k0 = 2;		k1 = 3;		k2 = 0;		k3 = 1;
		s0 = -1.0f;	s1 = -1.0f;	s2 = 1.0f;
	}

	float t = s0*mat[0] + s1*mat[4] + s2*mat[8] + 1.0f;
	float s = sbmReciprocalSqrt(t) * 0.5f;

	q[k0] = s*t;
	q[k1] = (mat[1] - s2*mat[3]) * s;
	q[k2] = (mat[6] - s1*mat[2]) * s;
	q[k3] = (mat[5] - s0*mat[7]) * s;

	q[0] = -q[0];
	q[1] = -q[1];
	q[2] = -q[2];
}

inline void sbmDualQuatToMat4(TMat4 &mat, TVect4 &qn, TVect4 &qd)
{
	float sqrLen;
	float w, x, y, z;
	float t0, t1, t2, t3;
	sqrLen = qn[0]*qn[0] + qn[1]*qn[1] + qn[2]*qn[2] + qn[3]*qn[3];
	w = qn[0];		x = qn[1];		y = qn[2];		z = qn[3];
	t0 = qd[0];		t1 = qd[1];		t2 = qd[2];		t3 = qd[3];

	mat[ 0] = w*w + x*x - y*y - z*z;
	mat[ 1] = 2*x*y - 2*w*z;
	mat[ 2] = 2*x*z + 2*w*y;
	mat[ 4] = 2*x*y + 2*w*z;
	mat[ 5] = w*w + y*y - x*x - z*z;
	mat[ 6] = 2*y*z - 2*w*x; 
	mat[ 8] = 2*x*z - 2*w*y;
	mat[ 9] = 2*y*z + 2*w*x;
	mat[10] = w*w + z*z - x*x - y*y;

	mat[ 3] = -2*t0*x + 2*w*t1 - 2*t2*z + 2*y*t3;
	mat[ 7] = -2*t0*y + 2*t1*z - 2*x*t3 + 2*w*t2;
	mat[11] = -2*t0*z + 2*x*t2 + 2*w*t3 - 2*t1*y;

	mat[12] = 0.0f;		mat[13] = 0.0f;		mat[14] = 0.0f;		mat[15] = 1.0f;

	sqrLen = 1.0f / sqrLen;
	int i;
	for(i = 0; i < 12; i++)
		mat[i] *= sqrLen;	
}

inline void sbmVect3Zero(float *vect)
{
	vect[0] = 0.0f;
	vect[1] = 0.0f;
	vect[2] = 0.0f;
}

inline void sbmVect3Set(float *vect, float x, float y, float z)
{
	vect[0] = x;
	vect[1] = y;
	vect[2] = z;
}

inline void sbmVect3Add(float *z, float *x, float *y)
{
	z[0] = x[0] + y[0];
	z[1] = x[1] + y[1];
	z[2] = x[2] + y[2];
}

inline void sbmVect3Sub(float *z, float *x, float *y)
{
	z[0] = x[0] - y[0];
	z[1] = x[1] - y[1];
	z[2] = x[2] - y[2];
}

inline float sbmVect3Dot(float *x, float *y)
{
	return x[0]*y[0] + x[1]*y[1] + x[2]*y[2];
}

inline void sbmVect3Cross(float *z, float *x, float *y)
{
	z[0] = x[1]*y[2] - x[2]*y[1];
	z[1] = x[2]*y[0] - x[0]*y[2];
	z[2] = x[0]*y[1] - x[1]*y[0];
}

inline void sbmVect3Outer(TMat3 &z, float *x, float *y)
{
	z[0] = x[0]*y[0];
	z[1] = x[0]*y[1];
	z[2] = x[0]*y[2];

	z[3] = x[1]*y[0];
	z[4] = x[1]*y[1];
	z[5] = x[1]*y[2];

	z[6] = x[2]*y[0];
	z[7] = x[2]*y[1];
	z[8] = x[2]*y[2];
}

inline float sbmVect3Norm(float *x)
{
	return (float)sqrt(x[0]*x[0] + x[1]*x[1] + x[2]*x[2]);
}

inline float sbmVect3ApproxNorm(float *x)
{
	return (float)sbmReciprocalSqrt(x[0]*x[0] + x[1]*x[1] + x[2]*x[2]);
}


inline float sbmVect3SquareNorm(float *x)
{
	return x[0]*x[0] + x[1]*x[1] + x[2]*x[2];
}

inline void sbmVect3Scalar(float *x, float s)
{
	x[0] *= s;
	x[1] *= s;
	x[2] *= s;
}

inline void sbmVect3Scalar(float *z, float *x, float s)
{
	z[0] = x[0]*s;
	z[1] = x[1]*s;
	z[2] = x[2]*s;
}

inline float sbmVect3Distance(float *x, float *y)
{
	TVect3 diff;
	sbmVect3Sub(diff, x, y);
	return sbmVect3Norm(diff);
}

inline void sbmVect4Zero(float *vect)
{
	vect[0] = 0.0f;
	vect[1] = 0.0f;
	vect[2] = 0.0f;
	vect[3] = 0.0f;
}

inline void sbmVect4ZeroPoint(float *vect)
{
	vect[0] = 0.0f;
	vect[1] = 0.0f;
	vect[2] = 0.0f;
	vect[3] = 1.0f;
}

inline void sbmVect4Set(float *vect, float x, float y, float z, float w)
{
	vect[0] = x;
	vect[1] = y;
	vect[2] = z;
	vect[3] = w;
}

inline void sbmVect3ToVect4(float *source, float *target)
{
	target[0] = source[0];
	target[1] = source[1];
	target[2] = source[2];
	target[3] = 1.0f;
}

inline void sbmVect3Copy(float *source, float *target)
{
	target[0] = source[0];
	target[1] = source[1];
	target[2] = source[2];
}

inline void sbmVect3LinearInterp(float *z, float *x, float *y, float t)
{
	float invt = 1.0f - t;
	z[0] = invt*x[0] + t*y[0];
	z[1] = invt*x[1] + t*y[1];
	z[2] = invt*x[2] + t*y[2];
}

inline void sbmVect4LinearInterp(float *z, float *x, float *y, float t)
{
	float invt = 1.0f - t;
	z[0] = invt*x[0] + t*y[0];
	z[1] = invt*x[1] + t*y[1];
	z[2] = invt*x[2] + t*y[2];
	z[3] = invt*x[3] + t*y[3];
}

inline void sbmVect3FromMat4Trans(float *vect, float *mat)
{
	vect[0] = mat[3];
	vect[1] = mat[7];
	vect[2] = mat[11];
}


inline void sbmVect4Copy(float *source, float *target)
{
	target[0] = source[0];
	target[1] = source[1];
	target[2] = source[2];
	target[3] = source[3];
}

inline float sbmVect4Dot(TVect4 &x, TVect4 &y)
{
	return x[0]*y[0] + x[1]*y[1] + x[2]*y[2] + x[3]*y[3];
}

inline void sbmVect4Zero(TVect4 &z)
{
	z[0] = 0.0f;
	z[1] = 0.0f;
	z[2] = 0.0f;
	z[3] = 0.0f;
}

inline void sbmVect4Add(TVect4 &z, TVect4 &x, TVect4 &y)
{
	z[0] = x[0] + y[0];
	z[1] = x[1] + y[1];
	z[2] = x[2] + y[2];
	z[3] = x[3] + y[3];
}

inline void sbmVect4ScalarMultAdd(TVect4 &z, TVect4 &x, TVect4 &y, float s)
{
	z[0] = x[0] + s*y[0];
	z[1] = x[1] + s*y[1];
	z[2] = x[2] + s*y[2];
	z[3] = x[3] + s*y[3];
}

inline float sbmVect4Norm(TVect4 &x)
{
	return (float)sqrt(x[0]*x[0] + x[1]*x[1] + x[2]*x[2] + x[3]*x[3]);
}

inline void sbmVect4Scalar(TVect4 &x, float s)
{
	x[0] *= s;
	x[1] *= s;
	x[2] *= s;
	x[3] *= s;
}

inline void sbmVect4Scalar(TVect4 &z, TVect4 &x, float s)
{
	z[0] = x[0] * s;
	z[1] = x[1] * s;
	z[2] = x[2] * s;
	z[3] = x[3] * s;
}

inline void sbmVect2Set(TVect2 &vect, float x, float y)
{
	vect[0] = x;
	vect[1] = y;
}

inline void sbmVect2Add(float *z, float *x, float *y)
{
	z[0] = x[0] + y[0];
	z[1] = x[1] + y[1];
}
inline void sbmVect2Sub(float *z, float *x, float *y)
{
	z[0] = x[0] - y[0];
	z[1] = x[1] - y[1];
}
inline float sbmVect2Inner(float *x, float *y)
{
	return x[0]*y[0] + x[1]*y[1];
}
inline float sbmVect2Cross(float *x, float *y)
{
	return x[0]*y[1] - x[1]*y[0];
}
inline float sbmVect2Norm(float *x)
{
	return (float)sqrt(x[0]*x[0] + x[1]*x[1]);
}
inline void sbmVect2Scalar(float *x, float s)
{
	x[0] *= s;
	x[1] *= s;
}
inline void sbmVect2Scalar(float *z, float *x, float s)
{
	z[0] = x[0]*s;
	z[1] = x[1]*s;
}


inline void sbmMat2Add(float *z, float *x, float *y)
{
	z[0] = x[0] + y[0];		z[1] = x[1] + y[1];
	z[2] = x[2] + y[2];		z[3] = x[3] + y[3];
}
inline void sbmMat2Sub(float *z, float *x, float *y)
{
	z[0] = x[0] - y[0];		z[1] = x[1] - y[1];
	z[2] = x[2] - y[2];		z[3] = x[3] - y[3];
}
inline void sbmMat2Vect2Product(float *z, float *x, float *y)
{
	z[0] = x[0]*y[0] + x[1]*y[1];
	z[1] = x[2]*y[0] + x[3]*y[1];
}
inline void sbmMat2Inverse(float *mat)
{
	float det = 1.0f/(mat[0]*mat[3] - mat[1]*mat[2]);
	float temp;
	temp = mat[0];
	mat[0] = mat[3];
	mat[3] = temp;

	mat[0] *= det;	mat[1] *= -det;
	mat[2] *= -det;	mat[3] *= det;
}
inline void sbmMat2Inverse(float *source, float *target)
{
	float det = 1.0f/(source[0]*source[3] - source[1]*source[2]);
	target[0] = det*source[3];
	target[1] = -det*source[1];
	target[2] = -det*source[2];
	target[3] = det*source[0];
}

// end of the file



