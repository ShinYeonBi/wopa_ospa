#include "meshView.h"
/*

meshView::meshView(void)
{
	// Program variables
	*image_name = NULL;
	initial_camera_towards.Reset(-0.57735, -0.57735, -0.57735);
	initial_camera_up.Reset(-0.57735, 0.57735, 0.5773);
	initial_camera_origin.Reset(0,0,0);
	initial_camera = FALSE;

	// GLUT variables 
	GLUTwindow = 0;
	GLUTwindow_height = 480;
	GLUTwindow_width = 640;
	//GLUTmouse[2] = { 0, 0 };
	//GLUTbutton[3] = { 0, 0, 0 };
	GLUTmodifiers = 0;

	// Application variables
	viewer = NULL;

	// Display variables
	show_faces = 1;
	show_edges = 0;
	show_vertices = 0;
	show_materials = 0;
	show_segments = 0;
	show_boundaries = 0;
	show_axes = 0;
	show_face_names = 0;
	show_edge_names = 0;
	show_vertex_names = 0;
	show_material_names = 0;
	show_segment_names = 0;
	show_backfacing = 0;
	current_mesh = -1;
}


meshView::~meshView(void)
{
}
/*
void meshView::GLUTDrawText(const R3Point& p, const char *s)
{
  // Draw text string s and position p
  glRasterPos3d(p[0], p[1], p[2]);
  while (*s) glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *(s++));
}
  


void meshView::GLUTStop(void)
{
  // Destroy window 
  glutDestroyWindow(GLUTwindow);
  
  // Release all meshes
  int n = meshes.NEntries();
  for (int i = 0; i < n; i ++) {
    if(meshes[i]) delete meshes[i];
  } 
  // Exit
  exit(0);
}



void meshView::GLUTRedraw(void)
{
  // Set viewing transformation
  viewer->Camera().Load();

  // Clear window 
  glClearColor(1.0, 1.0, 1.0, 1.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Set backface culling
  if (show_backfacing) glDisable(GL_CULL_FACE);
  else glEnable(GL_CULL_FACE);

  // Set lights
  static GLfloat light0_position[] = { 3.0, 4.0, 5.0, 0.0 };
  glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
  static GLfloat light1_position[] = { -3.0, -2.0, -3.0, 0.0 };
  glLightfv(GL_LIGHT1, GL_POSITION, light1_position);

  // Draw every mesh
  for (int i = 0; i < meshes.NEntries(); i++) {
    R3Mesh *mesh = meshes[i];
    if ((current_mesh != -1) && (i != current_mesh)) continue;

    // Draw faces
    if (show_faces) {
      glEnable(GL_LIGHTING);
      static GLfloat default_material[] = { 0.8, 0.8, 0.8, 1.0 };
      glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, default_material); 
      if (show_materials || show_segments) {
        for (int i = 0; i < mesh->NFaces(); i++) {
          R3MeshFace *face = mesh->Face(i);
          int color_index = 0;
          if (show_materials) color_index = mesh->FaceMaterial(face);
          else if (show_segments) color_index = mesh->FaceSegment(face);
          static GLfloat colors[24][4] = {
            {1,0,0,1}, {0,1,0,1}, {0,0,1,1}, {1,0,1,1}, {0,1,1,1}, {1,1,0,1}, 
            {1,.3,.7,1}, {1,.7,.3,1}, {.7,1,.3}, {.3,1,.7,1}, {.7,.3,1,1}, {.3,.7,1,1}, 
            {1,.5,.5,1}, {.5,1,.5,1}, {.5,.5,1,1}, {1,.5,1,1}, {.5,1,1,1}, {1,1,.5,1}, 
            {.5,0,0,1}, {0,.5,0,1}, {0,0,.5,1}, {.5,0,.5,1}, {0,.5,.5,1}, {.5,.5,0,1} 
          };
          glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, colors[color_index%24]); 
          mesh->DrawFace(face);
        }
      }
      else {
        mesh->DrawFaces();
      }
    }

    // Draw edges
    if (show_edges) {
      glDisable(GL_LIGHTING);
      glColor3f(1.0, 0.0, 0.0);
      mesh->DrawEdges();
    }

    // Draw boundary edges
    if (show_boundaries) {
      glLineWidth(3);
      glDisable(GL_LIGHTING);
      glColor3f(0.0, 0.0, 0.0);
      for (int i = 0; i < mesh->NEdges(); i++) {
        R3MeshEdge *edge = mesh->Edge(i);
        if (mesh->FaceOnEdge(edge, 0) && mesh->FaceOnEdge(edge, 1)) continue;
        mesh->DrawEdge(edge);
      }
      glLineWidth(1);
    }

    // Draw vertices
    if (show_vertices) {
      glEnable(GL_LIGHTING);
      static GLfloat material[] = { 0.8, 0.4, 0.2, 1.0 };
      glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, material); 
      // mesh->DrawVertices();
      RNScalar radius = 0.005 * mesh->BBox().DiagonalLength();
      for (int i = 0; i < mesh->NVertices(); i++) {
        R3MeshVertex *vertex = mesh->Vertex(i);
        R3Point position = mesh->VertexPosition(vertex);
        position += 0.5 * radius * R3Vector(0.01, 0.01, 0.01);
        material[0] = (31 + 48 * ((3*i) % 5)) / 256.0;
        material[1] = (31 + 32 * ((5*i) % 7)) / 256.0;
        material[2] = (31 + 19 * ((7*i) % 11)) / 256.0;
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, material); 
        R3Sphere(position, radius).Draw();
      }
    }

    // Draw face names
    if (show_face_names) {
      glDisable(GL_LIGHTING);
      glColor3f(0, 0, 0);
      for (int i = 0; i < mesh->NFaces(); i++) {
        R3MeshFace *face = mesh->Face(i);
        char buffer[256];
        sprintf(buffer, "%d", mesh->FaceID(face));
        GLUTDrawText(mesh->FaceCentroid(face), buffer);
      }
    }

    // Draw edge names   
    if (show_edge_names) {
      glDisable(GL_LIGHTING);
      glColor3f(0.8, 0.0, 0.0);
      for (int i = 0; i < mesh->NEdges(); i++) {
        R3MeshEdge *edge = mesh->Edge(i);
        char buffer[256];
        sprintf(buffer, "%d", mesh->EdgeID(edge));
        GLUTDrawText(mesh->EdgeMidpoint(edge), buffer);
      }
    }

    // Draw vertex names
    if (show_vertex_names) {
      glDisable(GL_LIGHTING);
      glColor3f(0.5, 0.3, 0.1);
      for (int i = 0; i < mesh->NVertices(); i++) {
        R3MeshVertex *vertex = mesh->Vertex(i);
        char buffer[256];
        sprintf(buffer, "%d", mesh->VertexID(vertex));
        GLUTDrawText(mesh->VertexPosition(vertex), buffer);
      }
    }

    // Draw material names
    if (show_material_names) {
      glDisable(GL_LIGHTING);
      glColor3f(0, 0, 1);
      for (int i = 0; i < mesh->NFaces(); i++) {
        R3MeshFace *face = mesh->Face(i);
        char buffer[256];
        sprintf(buffer, "%d", mesh->FaceMaterial(face));
        GLUTDrawText(mesh->FaceCentroid(face), buffer);
      }
    }

    // Draw segment names
    if (show_segment_names) {
      glDisable(GL_LIGHTING);
      glColor3f(0, 0, 1);
      for (int i = 0; i < mesh->NFaces(); i++) {
        R3MeshFace *face = mesh->Face(i);
        char buffer[256];
        sprintf(buffer, "%d", mesh->FaceSegment(face));
        GLUTDrawText(mesh->FaceCentroid(face), buffer);
      }
    }
  }

  // Draw axes
  if (show_axes) {
    RNScalar d = meshes[0]->BBox().DiagonalRadius();
    glDisable(GL_LIGHTING);
    glLineWidth(3);
    R3BeginLine();
    glColor3f(1, 0, 0);
    R3LoadPoint(R3zero_point + d * R3negx_vector);
    R3LoadPoint(R3zero_point + d * R3posx_vector);
    R3EndLine();
    R3BeginLine();
    glColor3f(0, 1, 0);
    R3LoadPoint(R3zero_point + d * R3negy_vector);
    R3LoadPoint(R3zero_point + d * R3posy_vector);
    R3EndLine();
    R3BeginLine();
    glColor3f(0, 0, 1);
    R3LoadPoint(R3zero_point + d * R3negz_vector);
    R3LoadPoint(R3zero_point + d * R3posz_vector);
    R3EndLine();
    glLineWidth(1);
  }

  // Capture image and exit
  if (image_name) {
    R2Image image(GLUTwindow_width, GLUTwindow_height, 3);
    image.Capture();
    image.Write(image_name);
    GLUTStop();
  }

  // Swap buffers 
  glutSwapBuffers();
}    



void meshView::GLUTResize(int w, int h)
{
  // Resize window
  glViewport(0, 0, w, h);

  // Resize viewer viewport
  viewer->ResizeViewport(0, 0, w, h);

  // Remember window size 
  GLUTwindow_width = w;
  GLUTwindow_height = h;

  // Redraw
  glutPostRedisplay();
}



void meshView::GLUTMotion(int x, int y)
{
  // Invert y coordinate
  y = GLUTwindow_height - y;

  // Compute mouse movement
  int dx = x - GLUTmouse[0];
  int dy = y - GLUTmouse[1];
  
  // World in hand navigation 
  R3Point origin = meshes[0]->BBox().Centroid();
  if (GLUTbutton[0]) viewer->RotateWorld(1.0, origin, x, y, dx, dy);
  else if (GLUTbutton[1]) viewer->ScaleWorld(1.0, origin, x, y, dx, dy);
  else if (GLUTbutton[2]) viewer->TranslateWorld(1.0, origin, x, y, dx, dy);
  if (GLUTbutton[0] || GLUTbutton[1] || GLUTbutton[2]) glutPostRedisplay();

  // Remember mouse position 
  GLUTmouse[0] = x;
  GLUTmouse[1] = y;
}



void meshView::GLUTMouse(int button, int state, int x, int y)
{
  // Invert y coordinate
  y = GLUTwindow_height - y;
  
  // Process mouse button event

  // Remember button state 
  int b = (button == GLUT_LEFT_BUTTON) ? 0 : ((button == GLUT_MIDDLE_BUTTON) ? 1 : 2);
  GLUTbutton[b] = (state == GLUT_DOWN) ? 1 : 0;

  // Remember modifiers 
  GLUTmodifiers = glutGetModifiers();

   // Remember mouse position 
  GLUTmouse[0] = x;
  GLUTmouse[1] = y;

  // Redraw
  glutPostRedisplay();
}



void meshView::GLUTSpecial(int key, int x, int y)
{
  // Invert y coordinate
  y = GLUTwindow_height - y;

  // Process keyboard button event 

  // Remember mouse position 
  GLUTmouse[0] = x;
  GLUTmouse[1] = y;

  // Remember modifiers 
  GLUTmodifiers = glutGetModifiers();

  // Redraw
  glutPostRedisplay();
}



void meshView::GLUTKeyboard(unsigned char key, int x, int y)
{
  // Process keyboard button event 
  switch (key) {
  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
    if (key == '0') current_mesh = -1;
    else if (key - '1' < meshes.NEntries()) current_mesh = key - '1';
    else printf("Unable to select mesh %d\n", key - '1');
    break;

  case 'A':
  case 'a':
    show_axes = !show_axes;
    break;

  case 'B':
  case 'b':
    show_backfacing = !show_backfacing;
    break;

  case 'C': 
  case 'c': {
    // Print camera
    const R3Camera& camera = viewer->Camera();
    printf("#camera  %g %g %g  %g %g %g  %g %g %g  %g \n",
           camera.Origin().X(), camera.Origin().Y(), camera.Origin().Z(),
           camera.Towards().X(), camera.Towards().Y(), camera.Towards().Z(),
           camera.Up().X(), camera.Up().Y(), camera.Up().Z(),
           camera.YFOV());
    }
    break;
      
  case 'E':
    show_edge_names = !show_edge_names;
    break;

  case 'e':
    show_edges = !show_edges;
    break;

  case 'F':
    show_face_names = !show_face_names;
    break;

  case 'f':
    show_faces = !show_faces;
    break;

  case 'V':
    show_vertex_names = !show_vertex_names;
    break;

  case 'v':
    show_vertices = !show_vertices;
    break;

  case 'M':
    show_material_names = !show_material_names;
    break;

  case 'm':
    show_materials = !show_materials;
    break;

  case 'S':
    show_segment_names = !show_segment_names;
    break;

  case 's':
    show_segments = !show_segments;
    break;

  case 'T':
  case 't':
    show_boundaries = !show_boundaries;
    break;

  case 27: // ESCAPE
    GLUTStop();
    break;
  }

  // Remember mouse position 
  GLUTmouse[0] = x;
  GLUTmouse[1] = GLUTwindow_height - y;

  // Remember modifiers 
  GLUTmodifiers = glutGetModifiers();

  // Redraw
  glutPostRedisplay();  
}


void meshView::GLUTInit(int *argc, char **argv)
{
  // Open window 
  glutInit(argc, argv);
  glutInitWindowPosition(100, 100);
  glutInitWindowSize(GLUTwindow_width, GLUTwindow_height);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH); // | GLUT_STENCIL
  GLUTwindow = glutCreateWindow("OpenGL Viewer");

  // Initialize background color 
  glClearColor(200.0/255.0, 200.0/255.0, 200.0/255.0, 1.0);

  // Initialize lights
  static GLfloat lmodel_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
  glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
  static GLfloat light0_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
  glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
  glEnable(GL_LIGHT0);
  static GLfloat light1_diffuse[] = { 0.5, 0.5, 0.5, 1.0 };
  glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_diffuse);
  glEnable(GL_LIGHT1);
  glEnable(GL_NORMALIZE);
  glEnable(GL_LIGHTING);

  // Initialize graphics modes 
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);

  // Initialize GLUT callback functions 
  glutDisplayFunc(GLUTRedraw);
  glutReshapeFunc(GLUTResize);
  glutKeyboardFunc(GLUTKeyboard);
  glutSpecialFunc(GLUTSpecial);
  glutMouseFunc(GLUTMouse);
  glutMotionFunc(GLUTMotion);
}



void meshView::GLUTMainLoop(void)
{
  // Run main loop -- never returns 
  glutMainLoop();
}



int meshView::ReadMeshes(const RNArray<char *>& mesh_names, const RNArray<char*>& seg_names)
{
  // Read each mesh
  int nSegFiles = seg_names.NEntries();
  for (int i = 0; i < mesh_names.NEntries(); i++) {
    char *mesh_name = mesh_names[i];
    char *seg_name = NULL;
    R3Mesh* mesh;
    if (nSegFiles>0 && i < nSegFiles) {
      seg_name = seg_names[i];
      mesh = ReadMesh(mesh_name, seg_name);
    }	
    else
      mesh = ReadMesh(mesh_name); 
    assert(mesh);

    // Add mesh to list
    meshes.Insert(mesh);
  }

  // Return number of meshes
  return meshes.NEntries();
}



R3Viewer * meshView::CreateBirdsEyeViewer(R3Mesh *mesh)
{
    // Setup camera view looking down the Z axis
    R3Box bbox = mesh->BBox();
    assert(!bbox.IsEmpty());
    RNLength r = bbox.DiagonalRadius();
    assert((r > 0.0) && RNIsFinite(r));
    if (!initial_camera) initial_camera_origin = bbox.Centroid() - initial_camera_towards * (2 * r);
    R3Camera camera(initial_camera_origin, initial_camera_towards, initial_camera_up, 0.4, 0.4, 0.01 * r, 100.0 * r);
    R2Viewport viewport(0, 0, GLUTwindow_width, GLUTwindow_height);
    return new R3Viewer(camera, viewport);
}



int meshView::ParseArgs(int argc, char **argv)
{
  // Check number of arguments
  if ((argc == 2) && (*argv[1] == '-')) {
    printf("Usage: mshview mshInput [-seg segInput] [options]\n");
    exit(0);
  }

  // Parse arguments
  argc--; argv++;
  while (argc > 0) {
    if ((*argv)[0] == '-') {
      if (!strcmp(*argv, "-v")) { print_verbose = 1; }
      else if (!strcmp(*argv, "-image")) { argc--; argv++; image_name = *argv; }
      else if (!strcmp(*argv, "-vertices")) { show_vertices = TRUE; }
      else if (!strcmp(*argv, "-edges")) { show_edges = TRUE; }
      else if (!strcmp(*argv, "-back")) { show_backfacing = TRUE; }
      else if (!strcmp(*argv, "-axes")) { show_axes = TRUE; }
      else if (!strcmp(*argv, "-segments")) { show_segments = TRUE; }
      else if (!strcmp(*argv, "-camera")) { 
        RNCoord x, y, z, tx, ty, tz, ux, uy, uz;
        argv++; argc--; x = atof(*argv); 
        argv++; argc--; y = atof(*argv); 
        argv++; argc--; z = atof(*argv); 
        argv++; argc--; tx = atof(*argv); 
        argv++; argc--; ty = atof(*argv); 
        argv++; argc--; tz = atof(*argv); 
        argv++; argc--; ux = atof(*argv); 
        argv++; argc--; uy = atof(*argv); 
        argv++; argc--; uz = atof(*argv); 
        initial_camera_origin = R3Point(x, y, z);
        initial_camera_towards.Reset(tx, ty, tz);
        initial_camera_up.Reset(ux, uy, uz);
        initial_camera = TRUE;
      }
      else if (!strcmp(*argv, "-seg")) {
	// this can only appear after all mesh filenames are given
        // if used, should take the same number of .seg files as that number of meshes
        int nFiles = mesh_names.NEntries();
        for(int i = 0; i < nFiles; i ++) {
	  argv++; argc--;
          if (argc < 0) break;
          seg_names.Insert(*argv);
        }
      }	
      else { fprintf(stderr, "Invalid program argument: %s", *argv); exit(1); }
      argv++; argc--;
    }
    else {
      mesh_names.Insert(*argv);
      argv++; argc--;
    }
  }

  // Check mesh filename
  if (mesh_names.IsEmpty()) {
    fprintf(stderr, "You did not specify a mesh file name.\n");
    return 0;
  }

  // Return OK status 
  return 1;
}*/