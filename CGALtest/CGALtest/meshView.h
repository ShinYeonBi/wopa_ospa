/*
#pragma once

#include "R3Graphics/R3Graphics.h"
#include "Segmentations.h"
#include <freeglut.h>
#include <stdio.h>

class meshView
{
	// Program variables
	RNArray<char *> mesh_names;
	RNArray<char *> seg_names;
	char *image_name;
	R3Vector initial_camera_towards;
	R3Vector initial_camera_up;
	R3Point initial_camera_origin;
	RNBoolean initial_camera;

	// GLUT variables 
	int GLUTwindow;
	int GLUTwindow_height;
	int GLUTwindow_width;
	int GLUTmouse[2];
	int GLUTbutton[3];
	int GLUTmodifiers;

	// Application variables
	R3Viewer *viewer;
	RNArray<R3Mesh *> meshes;

	// Display variables
	int show_faces;
	int show_edges;
	int show_vertices;
	int show_materials;
	int show_segments;
	int show_boundaries;
	int show_axes;
	int show_face_names;
	int show_edge_names;
	int show_vertex_names;
	int show_material_names;
	int show_segment_names;
	int show_backfacing;
	int current_mesh;

public:
	meshView(void);
	~meshView(void);

	void GLUTDrawText(const R3Point& p, const char *s);
	void GLUTStop();
	void GLUTRedraw();
	void GLUTResize(int w, int h);
	void GLUTMotion(int x, int y);
	void GLUTMouse(int button, int state, int x, int y);
	void GLUTSpecial(int key, int x, int y);
	void GLUTKeyboard(unsigned char key, int x, int y);
	void GLUTInit(int *argc, char **argv);
	void GLUTMainLoop(void);

	int ReadMeshes(const RNArray<char *>& mesh_names, const RNArray<char*>& seg_names);
	R3Viewer *CreateBirdsEyeViewer(R3Mesh *mesh);
	int ParseArgs(int argc, char **argv);
};
*/
