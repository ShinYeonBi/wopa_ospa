#include "ms2ms.h"


ms2ms::ms2ms(void)
{
	input_name = NULL;
	output_name = NULL;
	flip_faces = 0;
	scale_factor = 1.0;
	max_edge_length = 0.0;
	xform_name = NULL;
	print_verbose = 0;
}


ms2ms::~ms2ms(void)
{
}

int ms2ms::ReadMatrix(R4Matrix& m, const char *filename)
{
  // Open file
  FILE *fp = fopen(filename, "r");
  if (!fp) {
    fprintf(stderr, "Unable to open matrix file: %s\n", filename);
    return 0;
  }

  // Read matrix from file
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      double value;
      fscanf(fp, "%lf", &value);
      m[i][j] = value;
    }
  }

  // Close file
  fclose(fp);

  // Return success
  return 1;
}

int ms2ms::ParseArgs(/*int argc, char **argv*/char *p_input, char *p_output)
{

  // Check number of arguments
	/*
  if (argc == 1) {
    printf("Usage: msh2msh fnMeshIn fnMeshOut [options]\n");
    exit(0);
  }
	argc = 3;

  // Parse arguments
  argc--; argv++;
  while (argc > 0) 
  {
      if (!input_name) input_name = *argv;
      else if (!output_name) output_name = *argv;
      else { fprintf(stderr, "Invalid program argument: %s", *argv); exit(1); }
      argv++; argc--;
   
  }*/
  if (!input_name) input_name = p_input;
	if (!output_name) output_name = p_output;
	else { fprintf(stderr, "Invalid program argument: %s", input_name); exit(1); }

  // Check input filename
  if (!input_name) {
    fprintf(stderr, "You did not specify an input file name.\n");
    return 0;
  }

  // Check output filename
  if (!output_name) {
    fprintf(stderr, "You did not specify an output file name.\n");
    return 0;
  }

  // Return OK status 
  return 1;
}

void ms2ms::changeformat(/*int argc, char **argv*/char *p_input, char *p_output)
{
  // Check number of arguments
  if (!ParseArgs(p_input, p_output)) exit(1);
  // Create mesh
  R3Mesh *mesh = new R3Mesh();
  if (!mesh) {
    fprintf(stderr, "Unable to allocate mesh data structure.\n");
    exit(1);
  }

  // Read mesh
  if (!mesh->ReadFile(input_name)) {
    fprintf(stderr, "Unable to read file: %s\n", input_name);
    exit(1);
  }

  // Transform every vertex
  if (xform_name) {
    // Read xform
    R4Matrix m;
    if (ReadMatrix(m, xform_name)) {
      for (int i = 0; i < mesh->NVertices(); i++) {
        R3MeshVertex *v = mesh->Vertex(i);
        R3Point p = mesh->VertexPosition(v);
        mesh->SetVertexPosition(v, m * p);
      }
    }
  }

  // Scale every vertex
  if (scale_factor != 1.0) {
    for (int i = 0; i < mesh->NVertices(); i++) {
      R3MeshVertex *v = mesh->Vertex(i);
      R3Point p = mesh->VertexPosition(v);
      mesh->SetVertexPosition(v, p * scale_factor);
    }
  }

  // Write mesh
  if (!mesh->WriteFile(output_name)) {
    fprintf(stderr, "Unable to write file: %s\n", output_name);
    exit(1);
  }
  
  // Clean up
  delete mesh;

}