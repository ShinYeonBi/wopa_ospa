#pragma once
#include <stdio.h>
#include "R3Shapes/R3Shapes.h"
class ms2ms
{
public:
	ms2ms(void);
	~ms2ms(void);

	char *input_name;
	char *output_name;
	int flip_faces;
	RNScalar scale_factor;
	RNLength max_edge_length;
	char *xform_name;
	int print_verbose;

	int ReadMatrix(R4Matrix& m, const char *filename);
	int ParseArgs(/*int argc, char **argv*/char *p_input, char *p_output);
	void changeformat(/*int argc, char **argv*/char *p_input, char *p_output);
};

