#include "mshIO.h"


mshIO::mshIO(void)
{
}


mshIO::~mshIO(void)
{
}


void mshIO::savePBDDataFile(string p_filename,  R3Mesh * p_refMesh, OPObject* p_object)
{
	int m_iter_i=0;
	int m_iter_j=0;

	float ParticlePosX, ParticlePosY, ParticlePosZ;
	float ParticleSizeX, ParticleSizeY, ParticleSizeZ;
	float ParticleOriX, ParticleOriY, ParticleOriZ, ParticleOriW;
	TVect4 ori;
	TMat3 rotmat3;
	TMat4 rotmat4;
	ofstream outFile((p_filename + ".dat").c_str(), ios::out | ios::binary);
	cout<<"save dat file"<<endl;
	if(!outFile)
	{
		cerr<< "can't open output file" << endl;
		exit(1);
	}
	int t_numOfParticle = p_object->segmentList.size();
	cout<<"# Of Particle : "<< t_numOfParticle <<endl;
	endswap(&t_numOfParticle);
	outFile.write(reinterpret_cast<const char *>(&t_numOfParticle), sizeof(t_numOfParticle));
	endswap(&t_numOfParticle);
	// write particleInfo
	for(m_iter_i=0; m_iter_i<t_numOfParticle; m_iter_i++)
	{
		ParticlePosX = p_object->segmentList[m_iter_i]->m_com.X(); 
		ParticlePosY = p_object->segmentList[m_iter_i]->m_com.Y(); 
		ParticlePosZ = p_object->segmentList[m_iter_i]->m_com.Z(); 

		ParticleSizeX = p_object->segmentList[m_iter_i]->m_radius.X(); 
		ParticleSizeY = p_object->segmentList[m_iter_i]->m_radius.Y(); 
		ParticleSizeZ = p_object->segmentList[m_iter_i]->m_radius.Z(); 


		rotmat3[0] = p_object->segmentList[m_iter_i]->m_eigen_vector1.X();
		rotmat3[3] = p_object->segmentList[m_iter_i]->m_eigen_vector1.Y();
		rotmat3[6] = p_object->segmentList[m_iter_i]->m_eigen_vector1.Z();
	  
		rotmat3[1] = p_object->segmentList[m_iter_i]->m_eigen_vector2.X();
		rotmat3[4] = p_object->segmentList[m_iter_i]->m_eigen_vector2.Y();
		rotmat3[7] = p_object->segmentList[m_iter_i]->m_eigen_vector2.Z();
	  
		rotmat3[2] = p_object->segmentList[m_iter_i]->m_eigen_vector3.X();
		rotmat3[5] = p_object->segmentList[m_iter_i]->m_eigen_vector3.Y();
		rotmat3[8] = p_object->segmentList[m_iter_i]->m_eigen_vector3.Z();
		sbmMat3ToQuat(rotmat3, ori);
		ParticleOriX = ori[0]; 
		ParticleOriY = ori[1]; 
		ParticleOriZ = ori[2];
		ParticleOriW = ori[3];
		

		    endswap(&ParticlePosX);		endswap(&ParticlePosY);	endswap(&ParticlePosZ);
			outFile.write(reinterpret_cast<const char *>(&ParticlePosX), sizeof(ParticlePosX));
			outFile.write(reinterpret_cast<const char *>(&ParticlePosY), sizeof(ParticlePosY));
			outFile.write(reinterpret_cast<const char *>(&ParticlePosZ), sizeof(ParticlePosZ));
			endswap(&ParticlePosX);		endswap(&ParticlePosY);	endswap(&ParticlePosZ);

			endswap(&ParticleOriX);		endswap(&ParticleOriY);		endswap(&ParticleOriZ);		endswap(&ParticleOriW);
			outFile.write(reinterpret_cast<const char *>(&ParticleOriX), sizeof(ParticleOriX));
			outFile.write(reinterpret_cast<const char *>(&ParticleOriY), sizeof(ParticleOriY));
			outFile.write(reinterpret_cast<const char *>(&ParticleOriZ), sizeof(ParticleOriZ));
			outFile.write(reinterpret_cast<const char *>(&ParticleOriW), sizeof(ParticleOriW));
			endswap(&ParticleOriX);		endswap(&ParticleOriY);		endswap(&ParticleOriZ);		endswap(&ParticleOriW);

				

			endswap(&ParticleSizeX);		endswap(&ParticleSizeY);	endswap(&ParticleSizeZ);
			outFile.write(reinterpret_cast<const char *>(&ParticleSizeX), sizeof(ParticleSizeX));
			outFile.write(reinterpret_cast<const char *>(&ParticleSizeY), sizeof(ParticleSizeY));
			outFile.write(reinterpret_cast<const char *>(&ParticleSizeZ), sizeof(ParticleSizeZ));
			endswap(&ParticleSizeX);		endswap(&ParticleSizeY);	endswap(&ParticleSizeZ);

				

			float tmp = 0;
			endswap(&tmp);
			outFile.write(reinterpret_cast<const char *>(&tmp), sizeof(tmp));
			outFile.write(reinterpret_cast<const char *>(&tmp), sizeof(tmp));
			outFile.write(reinterpret_cast<const char *>(&tmp), sizeof(tmp));
			endswap(&tmp);

		
		
		float tmp2 = 1;
		endswap(&tmp2);
		outFile.write(reinterpret_cast<const char *>(&tmp2), sizeof(tmp2));
		endswap(&tmp2);
		bool isFixPos = false;
		endswap(&isFixPos);
		outFile.write(reinterpret_cast<const char *>(&isFixPos), sizeof(isFixPos));		

		outFile.write(reinterpret_cast<const char *>(&isFixPos), sizeof(isFixPos));
		endswap(&isFixPos);

		float sfiff = 0.5f;
		endswap(&sfiff);
		outFile.write(reinterpret_cast<const char *>(&sfiff), sizeof(sfiff));		
		endswap(&sfiff);

		float groupcount = 0;
		endswap(&groupcount);
		outFile.write(reinterpret_cast<const char *>(&groupcount), sizeof(groupcount));	
		endswap(&groupcount);

		int gnum = 0;
		endswap(&gnum);
		outFile.write(reinterpret_cast<const char *>(&gnum), sizeof(gnum));	
		endswap(&gnum);

		endswap(&isFixPos);
		outFile.write(reinterpret_cast<const char *>(&isFixPos), sizeof(isFixPos));
		endswap(&isFixPos);

	}

	int zero = 0;
	// write connectionTable
	//endswap(&zero);
	for(m_iter_i=0; m_iter_i<t_numOfParticle*t_numOfParticle; m_iter_i++)
	{
		zero = p_object->m_connectionTable[m_iter_i];
		endswap(&zero);
		outFile.write(reinterpret_cast<const char *>(&zero), sizeof(zero));
	}   
	endswap(&zero);	

	// write skinningWeightTable
	//Skinning skinningInfo = new Skinning(p_refMesh, p_object);
	float tmp3 = 0;
	endswap(&tmp3);
	for(m_iter_i=0; m_iter_i<p_refMesh->NVertices()*4; m_iter_i++)
	{
		outFile.write(reinterpret_cast<const char *>(&tmp3), sizeof(tmp3));
	}
	endswap(&tmp3);

	//  skinningIndexTable
	short idx = 0;
	endswap(&idx);
	for(m_iter_i=0; m_iter_i<p_refMesh->NVertices()*4; m_iter_i++)
	{
		outFile.write(reinterpret_cast<const char *>(&idx), sizeof(idx));
	}
	endswap(&idx);

	float ela = 0.5f;	
	endswap(&ela);
	outFile.write(reinterpret_cast<const char *>(&ela), sizeof(ela));
	outFile.write(reinterpret_cast<const char *>(&ela), sizeof(ela));
	outFile.write(reinterpret_cast<const char *>(&ela), sizeof(ela));
	endswap(&ela);

	outFile.close();
}
