#pragma once
#include <iostream>
#include <fstream>
#include <algorithm>
#include "OPObject.h"
#include "segment.h"
#include "R3Graphics/R3Graphics.h"
#include "commonType.h"

using namespace std;

class mshIO
{
public:
	mshIO(void);
	~mshIO(void);

	filebuf buffer;
    //ostream output(&buffer);
    //istream input(&buffer);

	void savePBDDataFile(string p_filename, R3Mesh * p_refMesh, OPObject* p_object);

	template <class T>
	void endswap(T *objp)
	{
	  unsigned char *memp = reinterpret_cast<unsigned char*>(objp);
	  reverse(memp, memp + sizeof(T));
	};
};

