// Source file for the mesh viewer program
// Include files 
#include "adolcOPT.h"
#include "R3Graphics/R3Graphics.h"
#include "Segmentations.h"
#include "segment.h"
#include "OPObject.h"
#include "mshIO.h"
#include "ms2ms.h"
#include "Opt.h"
#include "twbar.h"
#include "WOPA.h"
#include <freeglut.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polyhedron_items_with_id_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/mesh_segmentation.h>
#include <CGAL/property_map.h>
#include <Windows.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>

//segmentation variables
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Polyhedron_3<K, CGAL::Polyhedron_items_with_id_3>  Polyhedron;
Polyhedron mesh;
std::size_t number_of_clusters = 10;       // use 4 clusters in soft clustering
double smoothing_lambda = 0.15;  // importance of surface features, suggested to be in-between [0,1]

vector <OPObject*> OPObjectList;
static int g_idx = 0;

// Program variables
static RNArray<char *> mesh_names;
static RNArray<char *> seg_names;
static char *image_name = NULL;
static R3Vector initial_camera_towards(-0.57735, -0.57735, -0.57735);
static R3Vector initial_camera_up(-0.57735, 0.57735, 0.5773);
static R3Point initial_camera_origin(0, 0, 0);
static RNBoolean initial_camera = FALSE;

// GLUT variables 
static int GLUTwindow = 0;
static int GLUTwindow_height = 480;
static int GLUTwindow_width = 640;
static int GLUTmouse[2] = { 0, 0 };
static int GLUTbutton[3] = { 0, 0, 0 };
static int GLUTmodifiers = 0;

// Application variables
static R3Viewer *viewer = NULL;
static RNArray<R3Mesh *> meshes;

// Display variables
static int show_faces = 1;
static int show_edges = 0;
static int show_vertices = 0;
static int show_materials = 0;
static int show_segments = 1;
static int show_boundaries = 0;
static int show_axes = 0;
static int show_face_names = 0;
static int show_edge_names = 0;
static int show_vertex_names = 0;
static int show_material_names = 0;
static int show_segment_names = 0;
static int show_backfacing = 0;
static int show_error = 0;
static int current_mesh = -1;
static int show_particles = 1;
static int show_dist = 0;
static int show_sosq = 0;
static int show_reconstructed_vertex = 0;
static int show_1ring_connection = 0;

//I/O
mshIO g_IO;

//bar
//SubWindowData g_SubWindowData;
typedef enum { MODE_WOPA = 1, MODE_OSPA } Mode;

bool g_isarr = false;
int g_mode = 0;
bool g_arrMode = true;
Mode g_currentMode = MODE_WOPA;
char **g_argv;
WOPA g_wopa;
bool b_segment = false;

template<class ValueType>
struct Facet_with_id_pmap : public boost::put_get_helper<ValueType&, Facet_with_id_pmap<ValueType> >
{
	typedef Polyhedron::Facet_const_handle key_type;
	typedef ValueType value_type;
	typedef value_type& reference;
	typedef boost::lvalue_property_map_tag category;
	Facet_with_id_pmap(std::vector<ValueType>& internal_vector) : internal_vector(internal_vector) { }
	reference operator[](key_type key) const
	{
		return internal_vector[key->id()];
	}
private:
	std::vector<ValueType>& internal_vector;
};

int load_mesh(string fileName)
{
	std::ifstream input(fileName.c_str());
	if (!input || !(input >> mesh) || mesh.empty())
	{
		std::cerr << "Not a valid off file." << std::endl;
		return EXIT_FAILURE;
	}
}

void objInit()
{
	OPObjectList.clear();

	if (g_currentMode == MODE_OSPA)
	{
		OPObject * obresult = new OPObject(mesh.size_of_facets(), meshes[0]);
		OPObjectList.push_back(obresult);

		OPObject * ob1 = new OPObject(mesh.size_of_facets(), meshes[0]);
		OPObjectList.push_back(ob1);
	}

	else if (g_currentMode == MODE_WOPA)
	{
		OPObject * obresult = new OPObject(mesh.size_of_facets(), meshes[0]);
		OPObjectList.push_back(obresult);

		OPObject * ob1 = new OPObject(mesh.size_of_facets(), meshes[0]);
		OPObjectList.push_back(ob1);

		OPObject * ob2 = new OPObject(mesh.size_of_facets(), meshes[0]);
		OPObjectList.push_back(ob2);

		OPObject * ob3 = new OPObject(mesh.size_of_facets(), meshes[0]);
		OPObjectList.push_back(ob3);

		OPObject * ob4 = new OPObject(mesh.size_of_facets(), meshes[0]);
		OPObjectList.push_back(ob4);

		OPObject * ob5 = new OPObject(mesh.size_of_facets(), meshes[0]);
		OPObjectList.push_back(ob5);
	}
}

void Segmentation(char **argv)
{
	float Time;
	BOOL err;

	if (g_currentMode == MODE_OSPA)
	{

	}

	else if (g_currentMode == MODE_WOPA)
	{
		// assign id field for each facet
		std::size_t facet_id = 0;
		for (Polyhedron::Facet_iterator facet_it = mesh.facets_begin();
			facet_it != mesh.facets_end(); ++facet_it, ++facet_id) {
			facet_it->id() = facet_id;
		}

		// create a property-map for SDF values
		std::vector<double> sdf_values(mesh.size_of_facets());
		Facet_with_id_pmap<double> sdf_property_map(sdf_values);

		printf("START ..... sdf_property_map\n");
		CGAL::sdf_values(mesh, sdf_property_map);


		std::cout << std::endl;
		int numf = 0;
		// create a property-map for segment-ids
		std::vector<std::size_t> segment_ids(mesh.size_of_facets());
		Facet_with_id_pmap<std::size_t> segment_property_map(segment_ids);
		Facet_with_id_pmap<std::size_t> segment_property_map2(segment_ids);
		Facet_with_id_pmap<std::size_t> segment_property_map3(segment_ids);

		Facet_with_id_pmap<std::size_t> segment_property_map4(segment_ids);
		Facet_with_id_pmap<std::size_t> segment_property_map5(segment_ids);

		printf("START ..... segmentation1");
		std::size_t number_of_segments = CGAL::segmentation_from_sdf_values(mesh, sdf_property_map, segment_property_map, number_of_clusters, smoothing_lambda);
		std::cout << "Number of segments: " << number_of_segments << std::endl;

		for (int i = 0; i< number_of_segments; i++)
		{
			segment * tmp = new segment(mesh.size_of_vertices(), meshes[0]);
			OPObjectList[1]->segmentList.push_back(tmp);
		}

		for (Polyhedron::Facet_const_iterator facet_it = mesh.facets_begin(); facet_it != mesh.facets_end(); ++facet_it)
		{
			OPObjectList[1]->seg_arr[numf] = segment_property_map[facet_it];
			OPObjectList[0]->rawsdf[numf] = sdf_property_map[facet_it];
			OPObjectList[1]->rawsdf[numf] = sdf_property_map[facet_it];
			numf++;
		}

		printf("START segmentation2");
		int numf2 = 0;
		std::size_t number_of_segments2 = CGAL::segmentation_from_sdf_values(mesh, sdf_property_map, segment_property_map2, 30, 0.4);
		printf("END segmentation2");

		for (int i = 0; i< number_of_segments2; i++)
		{
			segment * tmp = new segment(mesh.size_of_vertices(), meshes[0]);
			OPObjectList[2]->segmentList.push_back(tmp);
		}

		for (Polyhedron::Facet_const_iterator facet_it = mesh.facets_begin(); facet_it != mesh.facets_end(); ++facet_it)
		{
			OPObjectList[2]->seg_arr[numf2] = segment_property_map2[facet_it];
			OPObjectList[2]->rawsdf[numf2] = sdf_property_map[facet_it];
			numf2++;
		}


		printf("START segmentation3");
		numf2 = 0;
		std::size_t number_of_segments3 = CGAL::segmentation_from_sdf_values(mesh, sdf_property_map, segment_property_map3, 30, 0.3);
		printf("END segmentation3");

		for (int i = 0; i< number_of_segments3; i++)
		{
			segment * tmp = new segment(mesh.size_of_vertices(), meshes[0]);
			OPObjectList[3]->segmentList.push_back(tmp);
		}

		for (Polyhedron::Facet_const_iterator facet_it = mesh.facets_begin(); facet_it != mesh.facets_end(); ++facet_it)
		{
			OPObjectList[3]->seg_arr[numf2] = segment_property_map3[facet_it];
			OPObjectList[3]->rawsdf[numf2] = sdf_property_map[facet_it];
			numf2++;
		}

		printf("START segmentation4");
		numf2 = 0;
		std::size_t number_of_segments4 = CGAL::segmentation_from_sdf_values(mesh, sdf_property_map, segment_property_map4, 30, 0.2);
		printf("END segmentation4");

		for (int i = 0; i< number_of_segments4; i++)
		{
			segment * tmp = new segment(mesh.size_of_vertices(), meshes[0]);
			OPObjectList[4]->segmentList.push_back(tmp);
		}

		for (Polyhedron::Facet_const_iterator facet_it = mesh.facets_begin(); facet_it != mesh.facets_end(); ++facet_it)
		{
			OPObjectList[4]->seg_arr[numf2] = segment_property_map4[facet_it];
			OPObjectList[4]->rawsdf[numf2] = sdf_property_map[facet_it];
			numf2++;
		}

		printf("START segmentation5");
		numf2 = 0;
		std::size_t number_of_segments5 = CGAL::segmentation_from_sdf_values(mesh, sdf_property_map, segment_property_map5, 30, 0.12);
		printf("END segmentation5");

		for (int i = 0; i< number_of_segments5; i++)
		{
			segment * tmp = new segment(mesh.size_of_vertices(), meshes[0]);
			OPObjectList[5]->segmentList.push_back(tmp);
		}

		for (Polyhedron::Facet_const_iterator facet_it = mesh.facets_begin(); facet_it != mesh.facets_end(); ++facet_it)
		{
			OPObjectList[5]->seg_arr[numf2] = segment_property_map5[facet_it];
			OPObjectList[5]->rawsdf[numf2] = sdf_property_map[facet_it];
			numf2++;
		}

		std::cout << "numf: " << numf << " ";
		std::cout << std::endl;
	}
}

void GLUTDrawText(const R3Point& p, const char *s)
{
	// Draw text string s and position p
	glRasterPos3d(p[0], p[1], p[2]);
	while (*s) glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *(s++));
}

void drawSphere()
{
	GLfloat color[4] = { 1, 0.5, 0, 1.0 };
	glEnable(GL_LIGHTING);

	for (int i = 0; i<OPObjectList[g_idx]->segmentList.size(); i++)
	{
		glPushMatrix();
		glTranslated(OPObjectList[g_idx]->segmentList[i]->m_com.X(), OPObjectList[g_idx]->segmentList[i]->m_com.Y(), OPObjectList[g_idx]->segmentList[i]->m_com.Z());
		glMultMatrixd(OPObjectList[g_idx]->segmentList[i]->a);
		glScaled(OPObjectList[g_idx]->segmentList[i]->m_radius.X(), OPObjectList[g_idx]->segmentList[i]->m_radius.Y(), OPObjectList[g_idx]->segmentList[i]->m_radius.Z());

		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color);
		glutSolidSphere(1, 20, 20);
		glPopMatrix();
	}
}

void GLUTStop(void)
{
	// Destroy window 
	glutDestroyWindow(GLUTwindow);

	// Release all meshes
	int n = meshes.NEntries();
	for (int i = 0; i < n; i++) {
		if (meshes[i]) delete meshes[i];
	}
	//glDeleteLists(SHAPE_WOPA);
	TwTerminate();
	// Exit
	exit(0);
}

void GLUTRedraw(void)
{

	// Set viewing transformation
	viewer->Camera().Load();

	// Clear window 
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Set backface culling
	if (show_backfacing) glDisable(GL_CULL_FACE);
	else glEnable(GL_CULL_FACE);

	// Set lights
	static GLfloat light0_position[] = { 3.0, 4.0, 5.0, 0.0 };
	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	static GLfloat light1_position[] = { -3.0, -2.0, -3.0, 0.0 };
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);

	if (show_dist)
	{
		// OPObjectList[0]->drawFinalError();
		OPObjectList[0]->drawFinalError2();
	}

	if (show_1ring_connection)
	{
		OPObjectList[0]->draw1ring();
	}

	if (show_particles)
	{
		drawSphere();
	}

	// Draw every mesh
	for (int i = 0; i < meshes.NEntries(); i++)
	{
		R3Mesh *mesh = meshes[i];
		if ((current_mesh != -1) && (i != current_mesh)) continue;

		// Draw faces
		if (show_faces) {
			glEnable(GL_LIGHTING);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			if (show_materials || show_segments)
			{
				for (int i = 0; i < mesh->NFaces(); i++)
				{
					R3MeshFace *face = mesh->Face(i);
					int color_index = 0;
					if (show_materials) color_index = mesh->FaceMaterial(face);
					else if (show_segments) color_index = OPObjectList[g_idx]->seg_arr[i];
					//fffff
					static GLfloat colors[30][4] =
					{
						{ 1, 0, 0, 1 }, { 0, 1, 0, 1 }, { 0, 0, 1, 1 }, { 1, 0, 1, 1 }, { 0, 1, 1, 1 }, { 1, 1, 0, 1 },
						{ 1, .3, .7, 1 }, { 1, .7, .3, 1 }, { .7, 1, .3, 1 }, { .3, 1, .7, 1 }, { .7, .3, 1, 1 }, { .3, .7, 1, 1 },
						{ 1, .5, .5, 1 }, { .5, 1, .5, 1 }, { .5, .5, 1, 1 }, { 1, .5, 1, 1 }, { .5, 1, 1, 1 }, { 1, 1, .5, 1 },
						{ .5, 0, 0, 1 }, { 0, .5, 0, 1 }, { 0, 0, .5, 1 }, { .5, 0, .5, 1 }, { 0, .5, .5, 1 }, { .5, .5, 0, 1 },
						{ .6, .4, 0, 1 }, { .6, .4, 0, 1 }, { .8, .2, 0, 1 }, { .8, .5, 0, 1 }, { .2, .8, 0, 1 }, { .6, .5, 0, 1 }
					};
					// glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, colors[color_index%24]); 
					if (!b_segment)
					{
						colors[16][3] = 0.5;
						glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, colors[16]); //메쉬 색 하나로 통일용
					}

					else if (b_segment)
						glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, colors[color_index % 30]);

					mesh->DrawFace(face);
				}
			}
			else {
				mesh->DrawFaces();
			}
		}

		// Draw edges
		if (show_edges) {
			glDisable(GL_LIGHTING);
			glColor3f(1.0, 0.0, 0.0);
			mesh->DrawEdges();
		}

		// Draw boundary edges
		if (show_boundaries) {
			glLineWidth(3);
			glDisable(GL_LIGHTING);
			glColor3f(0.0, 0.0, 0.0);
			for (int i = 0; i < mesh->NEdges(); i++) {
				R3MeshEdge *edge = mesh->Edge(i);
				if (mesh->FaceOnEdge(edge, 0) && mesh->FaceOnEdge(edge, 1)) continue;
				mesh->DrawEdge(edge);
			}
			glLineWidth(1);
		}

		// Draw vertices
		if (show_vertices) {
			glEnable(GL_LIGHTING);
			static GLfloat material[] = { 0.8, 0.4, 0.2, 1.0 };
			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, material);

			RNScalar radius = 0.005 * mesh->BBox().DiagonalLength();
			for (int i = 0; i < mesh->NVertices(); i++) {
				R3MeshVertex *vertex = mesh->Vertex(i);
				R3Point position = mesh->VertexPosition(vertex);
				position += 0.5 * radius * R3Vector(0.01, 0.01, 0.01);
				material[0] = (31 + 48 * ((3 * i) % 5)) / 256.0;
				material[1] = (31 + 32 * ((5 * i) % 7)) / 256.0;
				material[2] = (31 + 19 * ((7 * i) % 11)) / 256.0;
				glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, material);
				R3Sphere(position, radius).Draw();
			}
		}

		// Draw reconstructed vertices  
		if (show_reconstructed_vertex) {
			R3Point position;
			glEnable(GL_LIGHTING);
			static GLfloat material[] = { 0.8, 0.4, 0.2, 1.0 };
			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, material);

			RNScalar radius = 0.005 * mesh->BBox().DiagonalLength();
			for (int i = 0; i < OPObjectList[0]->p_Points.size(); i++)
			{
				position = OPObjectList[0]->mesh->VertexPosition(OPObjectList[0]->p_Points[i].v);
				position += 0.5 * radius * R3Vector(0.01, 0.01, 0.01);
				material[0] = (31 + 48 * ((3 * i) % 5)) / 256.0;
				material[1] = (31 + 32 * ((5 * i) % 7)) / 256.0;
				material[2] = (31 + 19 * ((7 * i) % 11)) / 256.0;
				glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, material);
				R3Sphere(position, radius).Draw();
			}
		}


		// Draw face names
		if (show_face_names) {
			glDisable(GL_LIGHTING);
			glColor3f(0, 0, 0);
			for (int i = 0; i < mesh->NFaces(); i++) {
				R3MeshFace *face = mesh->Face(i);
				char buffer[256];
				sprintf(buffer, "%d", mesh->FaceID(face));
				GLUTDrawText(mesh->FaceCentroid(face), buffer);
			}
		}

		// Draw edge names   
		if (show_edge_names) {
			glDisable(GL_LIGHTING);
			glColor3f(0.8, 0.0, 0.0);
			for (int i = 0; i < mesh->NEdges(); i++) {
				R3MeshEdge *edge = mesh->Edge(i);
				char buffer[256];
				sprintf(buffer, "%d", mesh->EdgeID(edge));
				GLUTDrawText(mesh->EdgeMidpoint(edge), buffer);
			}
		}

		// Draw vertex names
		if (show_vertex_names) {
			glDisable(GL_LIGHTING);
			glColor3f(0.5, 0.3, 0.1);
			for (int i = 0; i < mesh->NVertices(); i++) {
				R3MeshVertex *vertex = mesh->Vertex(i);
				char buffer[256];
				sprintf(buffer, "%d", mesh->VertexID(vertex));
				GLUTDrawText(mesh->VertexPosition(vertex), buffer);
			}
		}

		// Draw material names
		if (show_material_names) {
			glDisable(GL_LIGHTING);
			glColor3f(0, 0, 1);
			for (int i = 0; i < mesh->NFaces(); i++) {
				R3MeshFace *face = mesh->Face(i);
				char buffer[256];
				sprintf(buffer, "%d", mesh->FaceMaterial(face));
				GLUTDrawText(mesh->FaceCentroid(face), buffer);
			}
		}

		// Draw segment names
		if (show_segment_names) {
			glDisable(GL_LIGHTING);
			glColor3f(0, 0, 1);
			for (int i = 0; i < mesh->NFaces(); i++) {
				R3MeshFace *face = mesh->Face(i);
				char buffer[256];
				sprintf(buffer, "%d", mesh->FaceSegment(face));
				GLUTDrawText(mesh->FaceCentroid(face), buffer);
			}
		}
	}

	// Draw axes
	if (show_axes) {
		RNScalar d = meshes[0]->BBox().DiagonalRadius();
		glDisable(GL_LIGHTING);
		glLineWidth(3);
		R3BeginLine();
		glColor3f(1, 0, 0);
		R3LoadPoint(R3zero_point + d * R3negx_vector);
		R3LoadPoint(R3zero_point + d * R3posx_vector);
		R3EndLine();
		R3BeginLine();
		glColor3f(0, 1, 0);
		R3LoadPoint(R3zero_point + d * R3negy_vector);
		R3LoadPoint(R3zero_point + d * R3posy_vector);
		R3EndLine();
		R3BeginLine();
		glColor3f(0, 0, 1);
		R3LoadPoint(R3zero_point + d * R3negz_vector);
		R3LoadPoint(R3zero_point + d * R3posz_vector);
		R3EndLine();
		glLineWidth(1);
	}

	// Capture image and exit
	if (image_name) {
		R2Image image(GLUTwindow_width, GLUTwindow_height, 3);
		image.Capture();
		image.Write(image_name);
		GLUTStop();
	}

	if (show_error)
	{
		double tmp = 0;
		glDisable(GL_LIGHTING);
		glColor3f(0, 0, 0);
		////ffff
		for (int i = 0; i < OPObjectList[g_idx]->segmentList.size(); i++)
		{
			char buffer[256];
			tmp = OPObjectList[g_idx]->segmentList[i]->m_everageEllipsoidDist;
			sprintf(buffer, "[seg%d]%.3f", i, tmp);
			GLUTDrawText(OPObjectList[g_idx]->segmentList[i]->m_com, buffer);
		}
		glEnable(GL_LIGHTING);
	}

	if (show_sosq)
	{
		double tmp = 0;
		glDisable(GL_LIGHTING);
		glColor3f(0, 0, 0);
		for (int i = 0; i < OPObjectList[g_idx]->segmentList.size(); i++)
		{
			char buffer[256];
			tmp = OPObjectList[g_idx]->segmentList[i]->m_error_normalangle;
			sprintf(buffer, "[seg%d]%.3f", i, tmp);
			GLUTDrawText(OPObjectList[g_idx]->segmentList[i]->m_com, buffer);
		}
		glEnable(GL_LIGHTING);
	}
	if (show_particles)
	{
		drawSphere();
	}


	//draw 1ring connection
	if (show_1ring_connection)
	{
		OPObjectList[0]->draw1ring();
	}


	TwDraw();

	glutSwapBuffers();
	glutPostRedisplay();
}

void GLUTResize(int w, int h)
{
	// Resize window
	glViewport(0, 0, w, h);

	// Resize viewer viewport
	viewer->ResizeViewport(0, 0, w, h);

	// Remember window size 
	GLUTwindow_width = w;
	GLUTwindow_height = h;

	// Redraw
	glutPostRedisplay();

	// Send the new window size to AntTweakBar
	TwWindowSize(w, h);
}

int __cdecl GLUTMotionCB(int x, int y)
{
	// Invert y coordinate
	int m_y = GLUTwindow_height - y;

	// Compute mouse movement
	int dx = x - GLUTmouse[0];
	int dy = m_y - GLUTmouse[1];

	// World in hand navigation 
	R3Point origin = meshes[0]->BBox().Centroid();
	if (GLUTbutton[0]) viewer->RotateWorld(1.0, origin, x, m_y, dx, dy);
	else if (GLUTbutton[1]) viewer->ScaleWorld(1.0, origin, x, m_y, dx, dy);
	else if (GLUTbutton[2]) viewer->TranslateWorld(1.0, origin, x, m_y, dx, dy);
	if (GLUTbutton[0] || GLUTbutton[1] || GLUTbutton[2]) glutPostRedisplay();

	// Remember mouse position 
	GLUTmouse[0] = x;
	GLUTmouse[1] = m_y;

	return TwEventMouseMotionGLUT(x, y);
}

int __cdecl GLUTMouseCB(int button, int state, int x, int y)
{
	// Invert y coordinate
	int m_y = GLUTwindow_height - y;

	// Remember button state 
	int b = (button == GLUT_LEFT_BUTTON) ? 0 : ((button == GLUT_MIDDLE_BUTTON) ? 1 : 2);
	GLUTbutton[b] = (state == GLUT_DOWN) ? 1 : 0;

	// Remember modifiers 
	GLUTmodifiers = glutGetModifiers();

	// Remember mouse position 
	GLUTmouse[0] = x;
	GLUTmouse[1] = m_y;

	return TwEventMouseButtonGLUT(button, state, x, y);
}

int __cdecl GLUTSpecialCB(int key, int x, int y)
{
	// Invert y coordinate
	int m_y = GLUTwindow_height - y;

	// Process keyboard button event 

	// Remember mouse position 
	GLUTmouse[0] = x;
	GLUTmouse[1] = m_y;

	// Remember modifiers 
	GLUTmodifiers = glutGetModifiers();

	return TwEventSpecialGLUT(key, x, y);
}

int __cdecl GLUTKeyboardCB(unsigned char key, int x, int y)
{
	// Process keyboard button event 
	switch (key) {
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
		if (key == '0') current_mesh = -1;
		else if (key - '1' < meshes.NEntries()) current_mesh = key - '1';
		else printf("Unable to select mesh %d\n", key - '1');
		break;

	case 'B':
	case 'b':
		show_backfacing = !show_backfacing;
		break;

	case 'C':
	case 'c': {
				  // Print camera
				  const R3Camera& camera = viewer->Camera();
				  printf("#camera  %g %g %g  %g %g %g  %g %g %g  %g \n",
					  camera.Origin().X(), camera.Origin().Y(), camera.Origin().Z(),
					  camera.Towards().X(), camera.Towards().Y(), camera.Towards().Z(),
					  camera.Up().X(), camera.Up().Y(), camera.Up().Z(),
					  camera.YFOV());
	}
		break;

	case 'E':
		show_edge_names = !show_edge_names;
		break;

	case 'e':
		show_edges = !show_edges;
		break;

	case 'F':
		show_face_names = !show_face_names;
		break;

	case 'V':
		show_vertex_names = !show_vertex_names;
		break;

	case 'v':
		show_vertices = !show_vertices;
		break;

	case 'z':
		show_reconstructed_vertex = !show_reconstructed_vertex;
		break;

	case 'M':
		show_material_names = !show_material_names;
		break;

	case 'S':
		show_segment_names = !show_segment_names;
		break;


	case 't':
		show_boundaries = !show_boundaries;
		break;

	case 'q':
		show_sosq = !show_sosq;
		break;

	case 'd':
		show_dist = !show_dist;
		break;
		/*
		case '[' :
		if(g_idx > 0)
		g_idx--;
		break;

		case ']' :
		if(g_idx < OPObjectList.size()-1)
		g_idx++;
		break;*/

	case 27: // ESCAPE
		GLUTStop();
		break;
	}

	// Remember mouse position 
	GLUTmouse[0] = x;
	GLUTmouse[1] = GLUTwindow_height - y;

	// Remember modifiers 
	GLUTmodifiers = glutGetModifiers();

	return TwEventKeyboardGLUT(key, x, y);
}

/*
void GLUTInit(int *argc, char **argv)
{
//SubWindowData *win;
//win = &g_SubWindowData;
// Open window
glutInit(argc, argv);
glutInitWindowPosition(100, 100);
glutInitWindowSize(GLUTwindow_width, GLUTwindow_height);
glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH); // | GLUT_STENCIL
GLUTwindow = glutCreateWindow("WOPA&OSPA program");

// Initialize background color
glClearColor(200.0/255.0, 200.0/255.0, 200.0/255.0, 1.0);

// Initialize lights
static GLfloat lmodel_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
static GLfloat light0_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
glEnable(GL_LIGHT0);
static GLfloat light1_diffuse[] = { 0.5, 0.5, 0.5, 1.0 };
glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_diffuse);
glEnable(GL_LIGHT1);
glEnable(GL_NORMALIZE);
glEnable(GL_LIGHTING);

// Initialize graphics modes
glEnable(GL_DEPTH_TEST);
glEnable(GL_CULL_FACE);

// Initialize GLUT callback functions
glutDisplayFunc(GLUTRedraw);
glutReshapeFunc(GLUTResize);

//glutKeyboardFunc(GLUTKeyboard);
glutKeyboardFunc((GLUTkeyboardfun)KeyboardCB);
glutSpecialFunc(GLUTSpecial);
glutMouseFunc(GLUTMouse);
glutMotionFunc(GLUTMotion);
TwGLUTModifiersFunc(GetModifiers);

// Initialize AntTweakBar
TwInit(TW_OPENGL, NULL);
}
*/

int ReadMeshes(const RNArray<char *>& mesh_names, const RNArray<char*>& seg_names)
{
	meshes.Empty();

	// Read each mesh
	int nSegFiles = seg_names.NEntries();
	for (int i = 0; i < mesh_names.NEntries(); i++) {
		char *mesh_name = mesh_names[i];
		char *seg_name = NULL;
		R3Mesh* mesh;
		if (nSegFiles>0 && i < nSegFiles) {
			seg_name = seg_names[i];
			mesh = ReadMesh(mesh_name, seg_name);
		}
		else
			mesh = ReadMesh(mesh_name);
		assert(mesh);

		// Add mesh to list
		meshes.Insert(mesh);
	}

	// Return number of meshes
	return meshes.NEntries();
}

static R3Viewer * CreateBirdsEyeViewer(R3Mesh * mesh)
{

	// Setup camera view looking down the Z axis
	R3Box bbox = mesh->BBox();
	assert(!bbox.IsEmpty());
	RNLength r = bbox.DiagonalRadius();
	assert((r > 0.0) && RNIsFinite(r));
	if (!initial_camera) initial_camera_origin = bbox.Centroid() - initial_camera_towards * (2 * r);
	R3Camera camera(initial_camera_origin, initial_camera_towards, initial_camera_up, 0.4, 0.4, 0.01 * r, 100.0 * r);
	R2Viewport viewport(0, 0, GLUTwindow_width, GLUTwindow_height);
	return new R3Viewer(camera, viewport);
}

int ParseArgs(int argc, char **argv)
{
	// Check number of arguments
	if ((argc == 2) && (*argv[1] == '-')) {
		printf("Usage: meshview mshInput [-seg segInput] [options]\n");
		exit(0);
	}

	// Parse arguments
	argc--; argv++;
	while (argc > 0) {
		if ((*argv)[0] == '-') {
			if (!strcmp(*argv, "-v")) { print_verbose = 1; }
			else if (!strcmp(*argv, "-image")) { argc--; argv++; image_name = *argv; }
			else if (!strcmp(*argv, "-vertices")) { show_vertices = TRUE; }
			else if (!strcmp(*argv, "-edges")) { show_edges = TRUE; }
			else if (!strcmp(*argv, "-back")) { show_backfacing = TRUE; }
			else if (!strcmp(*argv, "-axes")) { show_axes = TRUE; }
			else if (!strcmp(*argv, "-segments")) { show_segments = TRUE; }
			else if (!strcmp(*argv, "-camera")) {
				RNCoord x, y, z, tx, ty, tz, ux, uy, uz;
				argv++; argc--; x = atof(*argv);
				argv++; argc--; y = atof(*argv);
				argv++; argc--; z = atof(*argv);
				argv++; argc--; tx = atof(*argv);
				argv++; argc--; ty = atof(*argv);
				argv++; argc--; tz = atof(*argv);
				argv++; argc--; ux = atof(*argv);
				argv++; argc--; uy = atof(*argv);
				argv++; argc--; uz = atof(*argv);
				initial_camera_origin = R3Point(x, y, z);
				initial_camera_towards.Reset(tx, ty, tz);
				initial_camera_up.Reset(ux, uy, uz);
				initial_camera = TRUE;
			}
			else if (!strcmp(*argv, "-seg")) {
				// this can only appear after all mesh filenames are given
				// if used, should take the same number of .seg files as that number of meshes
				int nFiles = mesh_names.NEntries();
				for (int i = 0; i < nFiles; i++) {
					argv++; argc--;
					if (argc < 0) break;
					seg_names.Insert(*argv);
				}
			}
			else { fprintf(stderr, "Invalid program argument: %s", *argv); exit(1); }
			argv++; argc--;
		}
		else {
			mesh_names.Insert(*argv);
			argv++; argc--;
		}
	}

	// Check mesh filename
	if (mesh_names.IsEmpty()) {
		fprintf(stderr, "You did not specify a mesh file name.\n");
		return 0;
	}

	// Return OK status 
	return 1;
}

// Returns an empty string if dialog is canceled
string openfilename(char *filter = "All Files (*.*)\0*.*\0", HWND owner = NULL) {
	OPENFILENAME ofn;
	char fileName[MAX_PATH] = "";
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = owner;
	ofn.lpstrFilter = filter;
	ofn.lpstrFile = fileName;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	ofn.lpstrDefExt = "";
	string fileNameStr;
	if (GetOpenFileName(&ofn))
		fileNameStr = fileName;
	return fileNameStr;
}

void resetparticle()
{
	if (g_isarr)
	{/*
	 printf("resetparticle!");
	 OPObjectList[0]->segmentList.clear();
	 printf("1!\n");
	 OPObjectList[0]->m_connectionTable = NULL;
	 printf("2!\n");

	 OPObjectList[0]->seg_arr = NULL;
	 printf("3!\n");
	 OPObjectList[0]->everageEdgeLenght = 0;
	 printf("4!\n");
	 OPObjectList[0]->m_final_error_sum = 0;
	 printf("5!\n");
	 OPObjectList[0]->m_final_error_evg = 0;
	 printf("6!\n");
	 OPObjectList[0]->m_pre_error_sum = 0;
	 printf("7!\n");
	 OPObjectList[0]->m_pre_error_evg = 0;
	 printf("8!\n");*/
		objInit();
		g_wopa.setWOPAdata(&OPObjectList, &mesh, &meshes);

		g_isarr = false;
	}

	if (g_currentMode == MODE_OSPA)
	{
		cout << "OSPA";
	}
	else if (g_currentMode == MODE_WOPA)
	{
		cout << "WOPA";
	}

	b_segment = false;
}

void TW_CALL SetShowParticleCB(const void *value, void *clientData)
{
	show_particles = *(const int *)value;
}

void TW_CALL GetShowParticleCB(void *value, void *clientData)
{
	(void)clientData; // unused
	*(int *)value = show_particles; // copy g_AutoRotate to value
}

void TW_CALL SetShowmeshCB(const void *value, void *clientData)
{
	show_faces = *(const int *)value;
}

void TW_CALL GetShowmeshCB(void *value, void *clientData)
{
	(void)clientData; // unused
	*(int *)value = show_faces; // copy g_AutoRotate to value
}

void TW_CALL SetShowconnectionCB(const void *value, void *clientData)
{
	show_1ring_connection = *(const int *)value;
}

void TW_CALL GetShowconnectionCB(void *value, void *clientData)
{
	(void)clientData; // unused
	*(int *)value = show_1ring_connection; // copy g_AutoRotate to value
}

void TW_CALL ArrCB(void *clientData)
{
	if (g_currentMode == MODE_OSPA)
	{
		resetparticle();
		Segmentation(g_argv);

		if (!g_isarr)
		{
			OPObjectList[0]->ParticleArrangement();
			OPObjectList[0]->Connect1ring();

			for (int j = 0; j<OPObjectList.size(); j++)
			{
				for (int i = 0; i<OPObjectList[j]->segmentList.size(); i++)
				{
					OPObjectList[j]->segmentList[i]->intersect_radialPrj();
				}
			}
			OPObjectList[0]->computefinalError2(1);

			g_isarr = true;
		}
	}
	else if (g_currentMode == MODE_WOPA)
	{
		resetparticle();
		Segmentation(g_argv);

		if (!g_isarr)
		{
			g_wopa.WOPAmergeobj();
			b_segment = true;
			g_isarr = true;
		}
	}

}

void TW_CALL OptCB(void *clientData)
{
	if (g_currentMode == MODE_OSPA)
	{
		if (g_isarr)
		{
			Opt opttest;
			double br_err = 0;
			double bn_err = 0;
			double bd_err = 0;
			double bv_err = 0;

			for (int i = 0; i<OPObjectList[0]->segmentList.size(); i++)
			{
				br_err += OPObjectList[0]->computeRadialError(OPObjectList[0]->segmentList[i]);
				bn_err += OPObjectList[0]->computeNormalError(OPObjectList[0]->segmentList[i]);
				bd_err += OPObjectList[0]->computeDistError(OPObjectList[0]->segmentList[i]);
				bv_err += OPObjectList[0]->computeVolumeError(OPObjectList[0]->segmentList[i]);
			}

			opttest.all_optimization(OPObjectList[0], 1e-3);

			for (int i = 0; i<OPObjectList[0]->segmentList.size(); i++)
			{
				OPObjectList[0]->segmentList[i]->m_isOptimized = true;
			}

			for (int j = 0; j<OPObjectList.size(); j++)
			{
				for (int i = 0; i<OPObjectList[j]->segmentList.size(); i++)
				{
					OPObjectList[j]->segmentList[i]->intersect_radialPrj();
				}
			}
			/*
			do{
			OPObjectList[0]->findCandidatePoints();
			OPObjectList[0]->Connect1ring();
			Opt opttest2;
			opttest.all_optimization(OPObjectList[0], 1e-3);
			OPObjectList[0]->searcherrorPoints();
			}
			while(OPObjectList[0]->p_Points.size() >3);*/

			for (int j = 0; j<OPObjectList.size(); j++)
			{
				for (int i = 0; i<OPObjectList[j]->segmentList.size(); i++)
				{
					OPObjectList[j]->segmentList[i]->intersect_radialPrj();
				}
			}

			OPObjectList[0]->computefinalError2(2);
			double ar_err = 0;
			double an_err = 0;
			double ad_err = 0;
			double av_err = 0;

			for (int i = 0; i<OPObjectList[0]->segmentList.size(); i++)
			{
				ar_err += OPObjectList[0]->computeRadialError(OPObjectList[0]->segmentList[i]);
				an_err += OPObjectList[0]->computeNormalError(OPObjectList[0]->segmentList[i]);
				ad_err += OPObjectList[0]->computeDistError(OPObjectList[0]->segmentList[i]);
				av_err += OPObjectList[0]->computeVolumeError(OPObjectList[0]->segmentList[i]);
			}

			printf("[B rad : %f, A rad : %f, 감소: %f\n B nor : %f, A nor : %f, 감소: %f\n B dist : %f, A dist : %f, 감소: %f\n B vol : %f, A vol : %f, 감소: %f\n B total: %f, A total: %f , 감소: %f]\n",
				br_err, ar_err, br_err - ar_err,
				bn_err, an_err, bn_err - an_err,
				bd_err, ad_err, bd_err - ad_err,
				bv_err, av_err, bv_err - av_err,
				br_err + bn_err + bd_err + bv_err, ar_err + an_err + ad_err + av_err, (br_err + bn_err + bd_err + bv_err) - (ar_err + an_err + ad_err + av_err));

		}

		else
			std::cout << "please arrange particle" << std::endl;
	}

	else if (g_currentMode == MODE_WOPA)
	{
		if (g_isarr)
		{
			//g_wopa.WOPAoptimization();
			//g_wopa.WOPASplit();
		}

	}
}

void TW_CALL ResetCB(void *clientData)
{
	resetparticle();
}

void TW_CALL LoadCB(void *clientData)
{
	string fileName = openfilename();

	mesh.clear();
	mesh_names.Empty();
	mesh_names.Insert((char*)fileName.c_str());

	// Read mesh
	if (!ReadMeshes(mesh_names, seg_names)) exit(-1);
	load_mesh(fileName);

	objInit();
	g_wopa.setWOPAdata(&OPObjectList, &mesh, &meshes);

	printf("num of face: %d", meshes[0]->NFaces());

	// Create viewer
	viewer = CreateBirdsEyeViewer(meshes[0]);
	if (!viewer) exit(-1);
}

void TW_CALL SaveCB(void *clientData)
{
	char fileName[256];
	if (g_currentMode == MODE_WOPA)
		sprintf(fileName, "%s_WOPA", mesh_names[0]);
	else
		sprintf(fileName, "%s_OSPA", mesh_names[0]);

	g_IO.savePBDDataFile(fileName, meshes[0], OPObjectList[0]);
}

int main(int argc, char **argv)
{
	TwBar *bar; // Pointer to the tweak bar

	number_of_clusters = 40;
	smoothing_lambda = 0.52f;

	// Initialize GLUT
	//GLUTInit(&argc, argv);

	// Open window 
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(GLUTwindow_width, GLUTwindow_height);
	GLUTwindow = glutCreateWindow("WOPA&OSPA program");
	glutCreateMenu(NULL);

	// Initialize background color 
	glClearColor(200.0 / 255.0, 200.0 / 255.0, 200.0 / 255.0, 1.0);

	// Initialize lights
	static GLfloat lmodel_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
	static GLfloat light0_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
	glEnable(GL_LIGHT0);
	static GLfloat light1_diffuse[] = { 0.5, 0.5, 0.5, 1.0 };
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_diffuse);
	glEnable(GL_LIGHT1);
	glEnable(GL_NORMALIZE);
	glEnable(GL_LIGHTING);

	// Initialize graphics modes 
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	// Initialize GLUT callback functions 
	glutDisplayFunc(GLUTRedraw);
	glutReshapeFunc(GLUTResize);

	// Initialize AntTweakBar
	TwInit(TW_OPENGL, NULL);

	glutMouseFunc((GLUTmousebuttonfun)GLUTMouseCB);
	glutMotionFunc((GLUTmousemotionfun)GLUTMotionCB);
	glutKeyboardFunc((GLUTkeyboardfun)GLUTKeyboardCB);
	glutSpecialFunc((GLUTspecialfun)GLUTSpecialCB);

	TwGLUTModifiersFunc(glutGetModifiers);

	// Create a tweak bar
	bar = TwNewBar("TweakBar");
	TwDefine(" GLOBAL help='This example shows how to integrate AntTweakBar with GLUT and OpenGL.' ");
	TwDefine(" TweakBar size='200 400' color='96 116 224' ");

	TwAddButton(bar, "Load off file", LoadCB, NULL, " label='Load off file' key=f help='Load off file' ");

	{
		TwEnumVal modeEV[2] = { { MODE_WOPA, "WOPA" }, { MODE_OSPA, "OSPA" } };
		TwType modeType = TwDefineEnum("modeType", modeEV, 2);
		TwAddVarRW(bar, "Mode", modeType, &g_currentMode, " keyIncr='<' keyDecr='>' help='Change arrange mode.' ");
	}

	TwAddVarCB(bar, "Particle", TW_TYPE_BOOL32, SetShowParticleCB, GetShowParticleCB, NULL,
		" label='Show particle [p]' key=p help='Toggle show particle mode.' ");

	TwAddVarCB(bar, "Mesh", TW_TYPE_BOOL32, SetShowmeshCB, GetShowmeshCB, NULL,
		" label='Show mesh [m]' key=m help='Toggle show mesh mode.' ");

	TwAddVarCB(bar, "Connect", TW_TYPE_BOOL32, SetShowconnectionCB, GetShowconnectionCB, NULL,
		" label='Show connection [c]' key=c help='Toggle show connection mode.' ");

	TwAddButton(bar, "arrange", ArrCB, NULL, " label='Arrngement [a]' key=a help='Arr' ");
	TwAddButton(bar, "Optimization", OptCB, NULL, " label='Optimization [o]' key=o help='Optimization' ");
	TwAddButton(bar, "Reset particle", ResetCB, NULL, " label='Reset particle [r]' key=r help='Reset particle' ");

	TwAddButton(bar, "Save", SaveCB, NULL, " label='Save dat [s]' key=s help='Save dat file' ");

	// Parse program arguments
	//if (!ParseArgs(argc, argv)) exit(-1);

	string fileName = openfilename();

	mesh_names.Empty();
	mesh_names.Insert((char*)fileName.c_str());

	// Read mesh
	if (!ReadMeshes(mesh_names, seg_names)) exit(-1);
	load_mesh(fileName);

	objInit();
	g_wopa.setWOPAdata(&OPObjectList, &mesh, &meshes);

	printf("num of face: %d", meshes[0]->NFaces());

	// Create viewer
	viewer = CreateBirdsEyeViewer(meshes[0]);
	if (!viewer) exit(-1);

	// Run GLUT interface
	glutMainLoop();

	return 0;
}