#include "segment.h"


segment::segment(void)
{
	m_numpoint = 0;
	m_mesh = NULL;
	m_numsegvertex = 0;
	m_sumEllipsoidDist = 0;
	m_everageEllipsoidDist = 0;
	m_sumSQDist = 0;
	m_maxdistance = 0;
	m_mindistance = 0;
	m_MaxDistVertexIdx = 0;
	m_MaxDistrotMeshpoint.Reset(0,0,0);
	m_MaxDistrotEllipsoidpoint.Reset(0,0,0);
	m_error_normalangle = 0;
	weight[0] = 0.90;//alpha
	weight[1] = 0.05;//beta
	weight[2] = 0.05;//gamma
}

segment::~segment(void)
{
}

segment::segment(int numpoint)
{
	m_segpointList = new int[numpoint];
	m_numpoint = numpoint;
	m_mesh = NULL;
	m_numsegvertex = 0;
	m_sumEllipsoidDist = 0;
	m_everageEllipsoidDist = 0;
	m_sumSQDist = 0;
	m_maxdistance = 0;
	m_mindistance = 0;
	m_MaxDistVertexIdx = 0;
	m_MaxDistrotMeshpoint.Reset(0,0,0);
	m_MaxDistrotEllipsoidpoint.Reset(0,0,0);
	m_error_normalangle = 0;
	m_numNeighbor = 4;
	m_NeiborIdx = new int[m_numNeighbor];
	weight[0] = 0.3;//alpha
	weight[1] = 0.1;//beta
	weight[2] = 0.3;//0.35;//gamma
	weight[3] = 0.3;//volume
	m_averageEdgeLenght = 0;
	m_isOptimized = false;
	m_volume = 0;

	for(int i = 0; i<m_numNeighbor; i++)
	{
		m_NeiborIdx[i] = -1;
	}

	for(int i = 0; i<numpoint; i++)
	{
		m_segpointList[i] = 0;
	}
	a[0] = 1;	a[1] = 0;	a[2] = 0;	a[3] = 0; 
	a[4] = 0;	a[5] = 1;	a[6] = 0;	a[7] = 0; 
	a[8] = 0;	a[9] = 0;	a[10] = 1;	a[11] = 0; 
	a[12] = 0;	a[13] = 0;	a[14] = 0;	a[15] = 1; 
}

segment::segment(int numpoint, R3Mesh *msh)
{
	//parents = par;
	m_segpointList = new int[numpoint];
	m_numpoint = numpoint;
	m_mesh = NULL;
	m_numsegvertex = 0;
	m_sumEllipsoidDist = 0;
	m_everageEllipsoidDist = 0;
	m_sumSQDist = 0;
	m_maxdistance = 0;
	m_mindistance = 0;
	m_MaxDistVertexIdx = 0;
	m_vertN = 0;
	del = false;

	weight[0] = 0.3;//radial p
	weight[1] = 0;//normal
	weight[2] = 0;//0.35;//gamma
	weight[3] = 0.7;10000;//volume
	m_everageEdgeLenght = 0;
	m_volume = 0;
	m_maxradialerr=0;

	for(int i = 0; i<numpoint; i++)
	{
		m_segpointList[i] = 0;
	}
	a[0] = 1;	a[1] = 0;	a[2] = 0;	a[3] = 0; 
	a[4] = 0;	a[5] = 1;	a[6] = 0;	a[7] = 0; 
	a[8] = 0;	a[9] = 0;	a[10] = 1;	a[11] = 0; 
	a[12] = 0;	a[13] = 0;	a[14] = 0;	a[15] = 1; 

	m_mesh = msh;
	for (int i = 0; i < m_mesh->NEdges(); i++) 
	{
        R3MeshEdge *edge = m_mesh->Edge(i);
		m_everageEdgeLenght += m_mesh->EdgeLength(edge);
    }
	m_everageEdgeLenght/= m_mesh->NEdges();
}

void segment::setmesh(R3Mesh *msh)
{
	m_mesh = msh;
	for (int i = 0; i < m_mesh->NEdges(); i++) 
	{
        R3MeshEdge *edge = m_mesh->Edge(i);
		m_averageEdgeLenght += m_mesh->EdgeLength(edge);
    }
	m_averageEdgeLenght/= m_mesh->NEdges();
	//std::cout<<"ever:"<<m_everageEdgeLenght<<std::endl;
}


double segment::SignedVolumeOfTriangle(R3Point p1, R3Point p2, R3Point p3)
{
	double v321 = p3.X()*p2.Y()*p1.Z();
    double v231 = p2.X()*p3.Y()*p1.Z();
    double v312 = p3.X()*p1.Y()*p2.Z();
    double v132 = p1.X()*p3.Y()*p2.Z();
    double v213 = p2.X()*p1.Y()*p3.Z();
    double v123 = p1.X()*p2.Y()*p3.Z();
	
	return (-v321 + v231 + v312 - v132 - v213 + v123)/(6.0f);
}

double segment::computeVolume()
{
	double vols = 0;
	

	for(int i = 0; i<m_mesh->NFaces(); i++)
	{
		R3MeshFace *face = m_mesh->Face(i);
		R3MeshVertex * v1 = m_mesh->VertexOnFace(face, 0);
		R3MeshVertex * v2 = m_mesh->VertexOnFace(face, 1);
		R3MeshVertex * v3 = m_mesh->VertexOnFace(face, 2);
		vols += SignedVolumeOfTriangle(m_mesh->VertexPosition(v1), m_mesh->VertexPosition(v2), m_mesh->VertexPosition(v3));
	}

	return abs(vols);
}

double segment::computeEllipsoidVolume()
{
	return m_radius.X()*m_radius.Y()*m_radius.Z()*PI*4.0f/3.0f;
}


void segment::drawLineTest()
{
	for(int i = 0; i<m_rotMeshpoints.size(); i++)
	{
		glDisable(GL_LIGHTING);
		glBegin(GL_LINES);
		glColor3f(0.0, 0.0, 0.0);
		if(m_MaxDistrotMeshpoint.Vector() == m_rotMeshpoints[i].Vector())
			glColor3f(1, 0, 1);
		glVertex3d(m_rotMeshpoints[i].X(),m_rotMeshpoints[i].Y(), m_rotMeshpoints[i].Z()); 
		glVertex3d(m_ellipsoidpoints[i].X(),m_ellipsoidpoints[i].Y(), m_ellipsoidpoints[i].Z()); 
		glEnd( );

		glEnable(GL_LIGHTING);
	}
	
	//draw maxdist
	glDisable(GL_LIGHTING);
	glBegin(GL_LINES);
	glColor3f(1, 0, 0);
	glVertex3d(m_MaxDistrotMeshpoint.X(),m_MaxDistrotMeshpoint.Y(), m_MaxDistrotMeshpoint.Z()); 
	glVertex3d(m_MaxDistrotEllipsoidpoint.X(),m_MaxDistrotEllipsoidpoint.Y(), m_MaxDistrotEllipsoidpoint.Z()); 
	glEnd( );
	glEnable(GL_LIGHTING);

}


void segment::normalDrawLine()
{
	for(int i = 0; i<m_rotMeshpoints.size(); i++)
	{
		glDisable(GL_LIGHTING);
		glBegin(GL_LINES);
		glColor3f(0.5, 0.5, 0.5);
		glVertex3d(m_rotMeshpoints[i].X(),m_rotMeshpoints[i].Y(), m_rotMeshpoints[i].Z()); 
		glColor3f(0.0, 0.0, 0.0);
		glVertex3d(m_ellipsoidpoints2[i].X(),m_ellipsoidpoints2[i].Y(), m_ellipsoidpoints2[i].Z()); 
		glEnd( );
		glEnable(GL_LIGHTING);
	}
}

double segment::intersect_radialPrj()
{
	R3Point ray_origin(0,0,0);
	R3Vector ray_normal1(0,0,0);
	R3Point rx;
	R3Point ry;
	double hit = 0;
	m_sumEllipsoidDist = 0;
	m_everageEllipsoidDist = 0;
	m_sumSQDist = 0;
	int numpoint = m_numpoint;
	m_rotMeshpoints.clear();
	m_ellipsoidpoints.clear();
	m_MeshPoints_origin.clear();

	double max = FLT_MIN;

		for(int i = 0; i<m_InnerMeshVertex.size(); i++)
		{
			R3Point p = m_OriMat*R3Point(m_mesh->VertexPosition(m_InnerMeshVertex[i]).X() - m_com.X(),
				m_mesh->VertexPosition(m_InnerMeshVertex[i]).Y() - m_com.Y(), 
				m_mesh->VertexPosition(m_InnerMeshVertex[i]).Z() - m_com.Z());
			
			m_MeshPoints_origin.push_back(p);//원점 중심에 오도록 회전, 이동한 segment의 점들 저장

			ray_normal1.Reset(p.X(), p.Y(), p.Z());
			ray_normal1.Normalize();

			double a = (ray_normal1.X()*ray_normal1.X())/(m_radius.X()*m_radius.X())
					+ (ray_normal1.Y()*ray_normal1.Y())/(m_radius.Y()*m_radius.Y())
					+ (ray_normal1.Z()*ray_normal1.Z())/(m_radius.Z()*m_radius.Z());

			double b = (2*ray_origin.X()*ray_normal1.X())/(m_radius.X()*m_radius.X())
					+ (2*ray_origin.Y()*ray_normal1.Y())/(m_radius.Y()*m_radius.Y())
					+ (2*ray_origin.Z()*ray_normal1.Z())/(m_radius.Z()*m_radius.Z());

			double c = (ray_origin.X()*ray_origin.X())/(m_radius.X()*m_radius.X())
					+ (ray_origin.Y()*ray_origin.Y())/(m_radius.Y()*m_radius.Y())
					+ (ray_origin.Z()*ray_origin.Z())/(m_radius.Z()*m_radius.Z())
					- 1;

			double d = ((b*b)-(4*a*c));			
			if ( d < 0 ) { continue; }

			hit = sqrt(a)/a;

			if(max < hit)
				m_maxradialerr = hit;

			rx = ray_origin + ray_normal1*hit;
			m_sumEllipsoidDist += (rx-p).Length();
			m_sumSQDist += (rx-p).Length()*(rx-p).Length();

			ry = inv*R3Point(p.X(), p.Y(), p.Z());
			rx = inv*R3Point(rx.X(), rx.Y(), rx.Z());

			ry+=m_com;
			rx+=m_com;
			//라인 그리기를 위한 위치
			m_rotMeshpoints.push_back(R3Point(ry.X(), ry.Y(), ry.Z()));//메쉬 위의 점
			m_ellipsoidpoints.push_back(R3Point(rx.X(), rx.Y(), rx.Z()));//타구체 위의 점
			//m_InnerMeshVertex.push_back(v);
		}

	m_everageEllipsoidDist = m_sumEllipsoidDist / m_numsegvertex;
	return m_sumEllipsoidDist;
}


void segment::rayEllipsoidIntersect()
{
	R3Point ray_origin(0,0,0);
	R3Vector ray_normal1(0,0,0);

	R3Point ry;
	R3Point rx;
	R3Point p;
	double hit = 0;
	double hitsecond = 0;
	double sum = 0;
	double tmp = 0;
	double px = 0;
	double py = 0;
	double pz = 0;
	double maxdistance = FLT_MIN;
	double tmpmax = 0;
	double tmpmin = 0;

	R3Vector cdv;//파티클 센터와 maxerror위치와의 거리
	double cdlen = 0;

	m_rotMeshpoints.clear();
	m_ellipsoidpoints.clear();
	m_ellipsoidpoints2.clear();

	for(int i = 0; i<m_numpoint; i++)
	{
		if(m_segpointList[i] == 1)
		{
			R3MeshVertex * v =m_mesh->Vertex(i);
			p = m_OriMat*R3Point(m_mesh->VertexPosition(v).X()-m_com.X(),m_mesh->VertexPosition(v).Y()-m_com.Y(),m_mesh->VertexPosition(v).Z()-m_com.Z());

			cdv = m_com - m_mesh->VertexPosition(v);


			ray_origin.Reset(p.X(),	p.Y(), p.Z());
			ray_normal1= m_OriMat*(m_mesh->VertexNormal(v));


			double a = ((ray_normal1.X()*ray_normal1.X())/(m_radius.X()*m_radius.X()))
					+ ((ray_normal1.Y()*ray_normal1.Y())/(m_radius.Y()*m_radius.Y()))
					+ ((ray_normal1.Z()*ray_normal1.Z())/(m_radius.Z()*m_radius.Z()));

			double b = ((2*ray_origin.X()*ray_normal1.X())/(m_radius.X()*m_radius.X()))
					+ ((2*ray_origin.Y()*ray_normal1.Y())/(m_radius.Y()*m_radius.Y()))
					+ ((2*ray_origin.Z()*ray_normal1.Z())/(m_radius.Z()*m_radius.Z()));

			double c = ((ray_origin.X()*ray_origin.X())/(m_radius.X()*m_radius.X()))
					+ ((ray_origin.Y()*ray_origin.Y())/(m_radius.Y()*m_radius.Y()))
					+ ((ray_origin.Z()*ray_origin.Z())/(m_radius.Z()*m_radius.Z()))
					- 1;

			double d = ((b*b)-(4*a*c));			
			if ( d < 0 ) { continue; }
			else { d = sqrt(d); }
			hit = (-b + d)/(2*a);
			hitsecond = (-b - d)/(2*a);
			
			if((hit<0 && hitsecond<0) || (hit>0 && hitsecond>0))//만약 hit, hitsecond의 부호가 같으면 절대값이 작은걸 고른다
			{
				if( fabs(hit) < fabs(hitsecond))
				{	
					rx = ray_origin + ray_normal1*hit;
					sum += fabs(hit);
					tmp = fabs(hit);
				}
				else
				{
					rx = ray_origin + ray_normal1*hitsecond;
					sum += fabs(hitsecond);
					tmp = fabs(hitsecond);
				}	
			}

			else//만약 hit, hitsecond중 하나는 양수, 하나는 음수이면 (노말 또는 노말 반대방향)과 같은방향이라는 것을 나타내는 해(양수인 해)를 고른다
			{
				if(hit>=0)
				{
					rx = ray_origin + ray_normal1*hit;
					sum += fabs(hit);
					tmp = fabs(hit);
				}

				else
				{
					rx = ray_origin + ray_normal1*hitsecond;
					sum += fabs(hitsecond);
					tmp = fabs(hitsecond);
				}
			
			}
			if(computeEllipsoidNormal(rx).IsZero())
				continue;

			R3Vector tmpv = inv*computeEllipsoidNormal(rx);
			//m_Rotellipsoidnormal.push_back(tmpv);

			ry = inv*R3Point(ray_origin.X(), ray_origin.Y(), ray_origin.Z());//메쉬 위의 점
			rx = inv*R3Point(rx.X(), rx.Y(), rx.Z());//타구체 위의 점

			ry+=m_com;
			rx+=m_com;

			if(80 < computeVectorAngle(tmpv, rx-ry))
			{
				continue;
			}

			m_rotMeshpoints.push_back(R3Point(ry.X(), ry.Y(), ry.Z()));//메쉬 위의 점
			m_ellipsoidpoints.push_back(R3Point(rx.X(), rx.Y(), rx.Z()));//타구체 위의 점

			//max dist 찾기
			if(maxdistance < tmp)
			{
				maxdistance = tmp;
				m_maxdistance = tmp;
				m_MaxDistrotMeshpoint.Reset(ry.X(), ry.Y(), ry.Z());
				m_MaxDistrotEllipsoidpoint.Reset(rx.X(), rx.Y(), rx.Z());
				m_MaxDistVertexIdx = i;
			}
			
		}
	}
}


R3Vector segment::computeEllipsoidNormal(R3Point point)
{
	int a = (point.X()*point.X())/(m_radius.X()*m_radius.X())
					+ (point.Y()*point.Y())/(m_radius.Y()*m_radius.Y())
					+ (point.Z()*point.Z())/(m_radius.Z()*m_radius.Z()) - 1;

	if(a != 0)//타구 위의 점인지 아닌지 검사
	{
		printf("no%d",a);
		return R3Vector(0,0,0);
	}

	R3Vector Ellipsoid_normal((2*point.X())/(m_radius.X()*m_radius.X()), (2*point.Y())/(m_radius.Y()*m_radius.Y()), (2*point.Z())/(m_radius.Z()*m_radius.Z()));
	Ellipsoid_normal.Normalize();
	return Ellipsoid_normal;
}

double segment::computeVectorAngle(R3Vector v1, R3Vector v2)
{
	double angle = 0;
	angle = acos(v1.Dot(v2)/(v1.Length()*v2.Length()));

	double sel_angle = std::min(angle, 2*PI-angle)*180/PI;

	return sel_angle;
}

double segment::computeVectorAngle2(R3Vector v1, R3Vector v2)
{
	v1.Normalize();
	v2.Normalize();
	double angle = v1.Dot(v2);

	return -(angle +1);
}

int segment::intersectLineAndTriangle(R3Vector p_lineStart, R3Vector p_lineEnd, R3Vector p_t0, R3Vector p_t1, R3Vector p_t2)
{
	R3Vector    u, v, n;             // triangle vectors
	R3Vector    dir, w0, w;          // ray vectors
	double     r, a, b;             // params to calc ray-plane intersect

	// get triangle edge vectors and plane normal
	u = p_t1 - p_t0;
	v = p_t2 - p_t0;
	n = u%v;             // cross product
	if (n.X() == 0 && n.Y() == 0 && n.Z() == 0)            // triangle is degenerate
		return -1;                 // do not deal with this case

	dir = p_lineEnd - p_lineStart;             // ray direction vector
	w0 = p_lineStart - p_t0;
	a = -(n.Dot(w0));
	b = n.Dot(dir);

	if (fabsf(b) < 0.00000001) {     // ray is parallel to triangle plane
		if (a == 0)                // ray lies in triangle plane
			return 2;
		else return 0;             // ray disjoint from plane
	}

	// get intersect point of ray with triangle plane
	r = a / b;
	if (r < 0.0)                   // ray goes away from triangle
		return 0;                  // => no intersect
	// for a segment, also test if (r > 1.0) => no intersect
	R3Vector intersectPoint = p_lineStart + r * dir;           // intersect point of ray and plane
	

	// is I inside T?
	double    uu, uv, vv, wu, wv, D;
	uu = u.Dot(u);
	uv = u.Dot(v);
	vv = v.Dot(v);
	w =  intersectPoint - p_t0;
	wu = w.Dot(u);
	wv = w.Dot(v);
	D = uv * uv - uu * vv;

	// get and test parametric coords
	float s, t;
	s = (uv * wv - vv * wu) / D;
	if (s < 0.0 || s > 1.0)        // I is outside T
		return 0;
	t = (uv * wu - uu * wv) / D;
	if (t < 0.0 || (s + t) > 1.0)  // I is outside T
		return 0;

	if (r< 0.0f || r > 1.0f)
		return 0;	
	
	m_Est.push_back(inv*p_lineStart.Point()+ m_com);
	m_Eed.push_back(inv*intersectPoint.Point()+ m_com);
	return 1;                      // I is in T
}

void segment::searchEllipsoidfaceIntersect()
{
	R3Vector p1, p2, p3;
	R3Vector pt1, pt2, pt3;
	R3Vector st;
	R3Vector ed;
	for(int j = 0; j<m_PointsOnEllipsoid.size(); j++)
	{
		for(int i = 0; i<m_segFaceList.size(); i++)
		{
			R3MeshVertex * v1 = m_mesh->VertexOnFace(m_segFaceList[i], 0);
			R3MeshVertex * v2 = m_mesh->VertexOnFace(m_segFaceList[i], 1);
			R3MeshVertex * v3 = m_mesh->VertexOnFace(m_segFaceList[i], 2);

			pt1 = m_mesh->VertexPosition(v1).Vector();
			pt2 = m_mesh->VertexPosition(v2).Vector();
			pt3 = m_mesh->VertexPosition(v3).Vector();

			p1 = m_OriMat*R3Vector((pt1-m_com.Vector()));
			p2 = m_OriMat*R3Vector((pt2-m_com.Vector()));
			p3 = m_OriMat*R3Vector((pt3-m_com.Vector()));

			st.Reset(m_PointsOnEllipsoid[j].X(), m_PointsOnEllipsoid[j].Y(), m_PointsOnEllipsoid[j].Z());
			ed = (m_PointsOnEllipsoid[j] + m_Rotellipsoidnormal[j]).Vector();

			intersectLineAndTriangle(st, ed, p1, p2, p3);

		}
	}
}

double segment::gaussianCurvature(R3Point point)
{
	double u = pow(m_radius.X(), 6) * pow(m_radius.Y(), 6) * pow(m_radius.Z(), 2);
	double d = pow(m_radius.X(), 4) * pow(m_radius.Y(), 4) + 
				pow(m_radius.Y(), 4)*(pow(m_radius.Z(), 2) - pow(m_radius.X(), 2))*point.X()*point.X() + 
				pow(m_radius.X(), 4)*(pow(m_radius.Z(), 2) - pow(m_radius.Y(), 2))*point.Y()*point.Y();

	return u/(d*d);
}

void segment::computeNormalError()
{
	double error = 0;
	R3Vector Enormal;
	R3Vector Mnormal;

	for(int i = 0; i<m_ellipsoidpoints.size(); i++)
	{

		Enormal = computeEllipsoidNormal((m_OriMat*(m_ellipsoidpoints[i]-m_com)).Point());

		Enormal = inv*Enormal;

		Mnormal = m_mesh->VertexNormal(m_InnerMeshVertex[i]);

		error += computeVectorAngle2(Enormal, Mnormal);

	}
	if(m_numsegvertex == 0 || error == 0)
		m_error_normalangle = -2.0f;

	else
		m_error_normalangle = error/m_numsegvertex;
}

void segment::searchInnerVertex()
{
	double a = 0;
	R3Point point;
	int cnt = 0;
	m_InnerMeshVertex.clear();
	for(int i = 0; i<m_mesh->NVertices(); i++)
	{
		//m_segpointList[i] = 0;
		R3MeshVertex * v =m_mesh->Vertex(i);
		point = (m_OriMat*(m_mesh->VertexPosition(v)-m_com)).Point();
		double a = (point.X()*point.X())/(m_radius.X()*m_radius.X())
					+ (point.Y()*point.Y())/(m_radius.Y()*m_radius.Y())
					+ (point.Z()*point.Z())/(m_radius.Z()*m_radius.Z()) - 1;

		if(a < 0)//파티클 내부에 있는 점임
		{
			//m_segpointList[i] = 1;
			m_InnerMeshVertex.push_back(v);
			cnt++;
		}
	}

	m_numsegvertex = cnt;
}

void segment::compute1ringDist()
{
	double length = 0;
	double real_length = 0;

	for(int i = 0; i<m_numNeighbor; i++)
	{
		length += (m_NeiborsegPointer[i]->m_com - m_com).Length();
		real_length += compute1ringrayDist(m_NeiborsegPointer[i]->m_com);
		real_length += m_NeiborsegPointer[i]->compute1ringrayDist(m_com);
	}

	std::cout<<"dist::::"<<abs(length - real_length)<<std::endl;
}

double segment::compute1ringrayDist(R3Point ray)
{
	R3Point ray_origin(0,0,0);
	R3Vector ray_normal1(0,0,0);
	R3Point rx;
	R3Point ry;
	double hit = 0;
	double Dist = 0;
	
			//ray = global에서 1링 이웃 파티클의 중심 위치
			R3Point p = m_OriMat*R3Point(ray.X() - m_com.X(), ray.Y() - m_com.Y(), ray.Z() - m_com.Z());//원점 중심에 오도록 회전
			
			//m_MeshPoints_origin.push_back(p);//원점 중심에 오도록 회전, 이동한 segment의 점들 저장

			ray_normal1.Reset(p.X(), p.Y(), p.Z());
			ray_normal1.Normalize();//ray

			double a = (ray_normal1.X()*ray_normal1.X())/(m_radius.X()*m_radius.X())
					+ (ray_normal1.Y()*ray_normal1.Y())/(m_radius.Y()*m_radius.Y())
					+ (ray_normal1.Z()*ray_normal1.Z())/(m_radius.Z()*m_radius.Z());

			double b = (2*ray_origin.X()*ray_normal1.X())/(m_radius.X()*m_radius.X())
					+ (2*ray_origin.Y()*ray_normal1.Y())/(m_radius.Y()*m_radius.Y())
					+ (2*ray_origin.Z()*ray_normal1.Z())/(m_radius.Z()*m_radius.Z());

			double c = (ray_origin.X()*ray_origin.X())/(m_radius.X()*m_radius.X())
					+ (ray_origin.Y()*ray_origin.Y())/(m_radius.Y()*m_radius.Y())
					+ (ray_origin.Z()*ray_origin.Z())/(m_radius.Z()*m_radius.Z())
					- 1;

			double d = ((b*b)-(4*a*c));			


			hit = sqrt(a)/a;

			rx = ray_origin + ray_normal1*hit;//거리....
			Dist += rx.Vector().Length();

	return Dist;
}