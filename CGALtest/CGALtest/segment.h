#pragma once
#include "R3Graphics/R3Graphics.h"
#include <CGAL/basic.h>
#include <CGAL/Object.h>
#include <CGAL/centroid.h>
#include <CGAL/eigen.h>
#include <CGAL/PCA_util.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/bounding_box.h>
#include "DistancePointEllipseEllipsoid.h"
#include <freeglut.h>
#include <cmath>
#include <iterator>
#include <vector>
const double PI = 3.141592;

class segment
{

public:
	segment(void);
	~segment(void);
	segment(int numpoint);
	segment(int numpoint, R3Mesh *msh);

	int m_numpoint;		//메쉬 버텍스 개수
	int m_numsegvertex;	//세그먼트의 버텍스 개수
	R3Point m_com;
	int *m_segpointList;//메쉬 버텍스 개수 크기의 배열. 이 세그먼트가 메쉬의 어떤 버텍스를 포함하고있는지 저장(포함=1, 안포함=0)
	R3Mesh *m_mesh;
	double m_eigen_values[3];
	R3Vector m_eigen_vector1;
	R3Vector m_eigen_vector2;
	R3Vector m_eigen_vector3;
	R4Matrix m_OriMat;
	R4Matrix inv;
	GLdouble a[16];
	R3Vector m_radius;
	double m_volume_seg;
	double m_volume_diff;
	double m_sumEllipsoidDist;
	double m_everageEllipsoidDist;
	double m_sumSQDist;
	double m_maxdistance;
	double m_mindistance;
	double m_error_normalangle;// 일립소이드와 메쉬 표면 벡터의 차이
	double weight[4];
	double m_averageEdgeLenght;
	double m_maxradialerr;
	bool m_isOptimized;
	double m_volume;

	//WOPA용
	double m_vertN;//실제로 계산되는 버텍스 개수
	bool del; //split 후에 지워져야하는 파티클인가
	double m_everageEdgeLenght;


	//1ring
	int m_numNeighbor;
	int *m_NeiborIdx;
	std::vector <segment*> m_NeiborsegPointer;

	R3Point m_MaxDistrotMeshpoint;
	R3Point m_MaxDistrotEllipsoidpoint;
	int m_MaxDistVertexIdx;

	DistancePointEllipseEllipsoid dist;
	std::vector <R3Point> m_rotMeshpoints;
	std::vector <R3Point> m_ellipsoidpoints;
	std::vector <R3Point> m_MeshPoints_origin;

	//normal vector를 이용한 error저장용
	std::vector <R3Point> m_ellipsoidpoints2;
	//타구체 노말벡터계산 테스트용
	std::vector <R3Vector> m_Rotellipsoidnormal;
	//타구체 위의 점 샘플링 테스트용
	std::vector <R3Point> m_PointsOnEllipsoid;
	//세그먼트에 속한 face list
	std::vector <R3MeshFace*> m_segFaceList;
	//일립소이드와 메쉬 표면 벡터 차이 계산용. 초기 배치 뒤 파티클 내부에 있는 점들의 포인터 저장
	std::vector <R3MeshVertex*> m_InnerMeshVertex;
	

	//그리기용
	std::vector <R3Point> m_Est;
	std::vector <R3Point> m_Eed;

	double e[3];
	double y[3];
	double x[3];

	void setmesh(R3Mesh *msh);
	double computeVolume();
	double SignedVolumeOfTriangle(R3Point p1, R3Point p2, R3Point p3);
	double computeEllipsoidVolume();
	void drawLineTest();
	void normalDrawLine();
	void computeNormalError();

	double intersect_radialPrj();
	void rayEllipsoidIntersect();
	double computeVectorAngle(R3Vector v1, R3Vector v2);
	double computeVectorAngle2(R3Vector v1, R3Vector v2);
	R3Vector computeEllipsoidNormal(R3Point point);
	int intersectLineAndTriangle(R3Vector p_lineStart, R3Vector p_lineEnd, R3Vector p_t0, R3Vector p_t1, R3Vector p_t2);
	void searchEllipsoidfaceIntersect();
	void searchInnerVertex();
	void compute1ringDist();
	double compute1ringrayDist(R3Point ray);//1ring과 딱 붙었을때의 거리를 구함

	//curvature
	double gaussianCurvature(R3Point point);

	void computeEigenv()
	{
		CGAL::Simple_cartesian<double> asd;
		std::vector<CGAL::Simple_cartesian<double>::Point_3> points;
		std::vector<CGAL::Simple_cartesian<double>::Point_3> rotpoints;

		for(int i = 0; i<m_InnerMeshVertex.size(); i++)
		{

			points.push_back(CGAL::Simple_cartesian<double>::Point_3(m_mesh->VertexPosition(m_InnerMeshVertex[i]).X(),
																	m_mesh->VertexPosition(m_InnerMeshVertex[i]).Y(),
																	m_mesh->VertexPosition(m_InnerMeshVertex[i]).Z()));
		}

		CGAL::Simple_cartesian<double>::FT eigen_values[3];
		CGAL::Simple_cartesian<double>::FT eigen_vectors[9];
		CGAL::Simple_cartesian<double>::FT covariance[6];
		// precondition: at least one element in the container.
		CGAL_precondition(points.begin() != points.end());
		// compute centroid
		CGAL::Simple_cartesian<double>::Point_3 c = centroid(points.begin(), points.end(), CGAL::Dimension_tag<0>());
		// assemble covariance matrix
		CGAL::internal::assemble_covariance_matrix_3(points.begin(), points.end(), covariance, c, asd, (CGAL::Simple_cartesian<double>::Point_3*)NULL, CGAL::Dimension_tag<0>());
		
		CGAL::internal::eigen_symmetric<CGAL::Simple_cartesian<double>::FT>(covariance,3,eigen_vectors,eigen_values);

		m_eigen_values[0] = eigen_values[0];	m_eigen_values[1] = eigen_values[1];	m_eigen_values[2] = eigen_values[2];

		m_eigen_vector1.Reset(eigen_vectors[0], eigen_vectors[1], eigen_vectors[2]);
		m_eigen_vector2.Reset(eigen_vectors[3], eigen_vectors[4], eigen_vectors[5]);
		m_eigen_vector3.Reset(eigen_vectors[6], eigen_vectors[7], eigen_vectors[8]);

		R3Vector crs(eigen_vectors[3], eigen_vectors[4], eigen_vectors[5]);
		crs.Cross(m_eigen_vector3);
		m_eigen_vector1.Reset(crs.X(), crs.Y(), crs.Z());

		//set OriMatrix
		a[0] = m_eigen_vector1.X();
		a[1] = m_eigen_vector1.Y();
		a[2] = m_eigen_vector1.Z();
		a[3] = 0;

		a[4] = m_eigen_vector2.X();
		a[5] = m_eigen_vector2.Y();
		a[6] = m_eigen_vector2.Z();
		a[7] = 0;

		a[8] = m_eigen_vector3.X();
		a[9] = m_eigen_vector3.Y();
		a[10] = m_eigen_vector3.Z();
		a[11] = 0;

		a[12] = 0;
		a[13] = 0;
		a[14] = 0;
		a[15] = 1;
		m_OriMat = R4Matrix(a);

		
		for(int i = 0; i<m_InnerMeshVertex.size(); i++)
		{

			R3Point rotvec = m_OriMat*R3Point(m_mesh->VertexPosition(m_InnerMeshVertex[i]).X(),
												m_mesh->VertexPosition(m_InnerMeshVertex[i]).Y(),
												m_mesh->VertexPosition(m_InnerMeshVertex[i]).Z());
			rotpoints.push_back(CGAL::Simple_cartesian<double>::Point_3(rotvec.X(),rotvec.Y(),rotvec.Z()));
		}

		
		CGAL::Simple_cartesian<double>::Iso_cuboid_3 c3 = CGAL::bounding_box(rotpoints.begin(), rotpoints.end());
		
		m_radius.Reset((c3.xmax()-c3.xmin())/2.0f, (c3.ymax()-c3.ymin())/2.0f, (c3.zmax()-c3.zmin())/2.0f);

		m_volume = 4*PI*m_radius.X()*m_radius.Y()*m_radius.Z()/3.0f;

		R3Point tmp = R3Point(c3.xmin(), c3.ymin(), c3.zmin())+m_radius;
		inv = m_OriMat.Inverse();
		R3Point com = inv*tmp;
		m_com.Reset(com.X(), com.Y(),com.Z());
	}
};
