#include "twbar.h"


twbar::twbar(void)
{
	bar = TwNewBar("TweakBar");
	TwDefine(" GLOBAL help='This example shows how to integrate AntTweakBar with GLUT and OpenGL.' "); // Message added to the help bar.
    TwDefine(" TweakBar size='300 400' color='96 216 224' "); // change default tweak bar size and color
}


twbar::~twbar(void)
{
}

void twbar::createbar()
{
	bar = TwNewBar("TweakBar");
	TwDefine(" GLOBAL help='This example shows how to integrate AntTweakBar with GLUT and OpenGL.' "); // Message added to the help bar.
    TwDefine(" TweakBar size='300 400' color='96 216 224' "); // change default tweak bar size and color
}
/*
void twbar::SetShowParticleCB(const void *value, void *clientData)
{
	show_particles = *(const int *)value;
}

void twbar::GetShowParticleCB(void *value, void *clientData)
{
    (void)clientData; // unused
    *(int *)value = show_particles; // copy g_AutoRotate to value
}

void TW_CALL twbar::SetShowmeshCB(const void *value, void *clientData)
{
	show_faces = *(const int *)value;
}

void TW_CALL twbar::GetShowmeshCB(void *value, void *clientData)
{
    (void)clientData; // unused
    *(int *)value = show_faces; // copy g_AutoRotate to value
}

void TW_CALL twbar::SetShowconnectionCB(const void *value, void *clientData)
{
	show_1ring_connection = *(const int *)value;
}

void TW_CALL twbar::GetShowconnectionCB(void *value, void *clientData)
{
    (void)clientData; // unused
    *(int *)value = show_1ring_connection; // copy g_AutoRotate to value
}

void TW_CALL twbar::ArrCB(void *clientData)
{
	if(!g_isarr)
	{
	   OPObjectList[0]->ParticleArrangement();
		OPObjectList[0]->Connect1ring();
	
		for(int j = 0;j<OPObjectList.size(); j++)
		{
			for(int i = 0; i<OPObjectList[j]->segmentList.size(); i++)
			{
				OPObjectList[j]->segmentList[i]->intersect_radialPrj();
			}
		}
		OPObjectList[0]->computefinalError2(1);

		g_isarr = true;
	}
}

void TW_CALL twbar::OptCB(void *clientData)
{
	if(g_isarr)
	{
	   Opt opttest;
		double br_err = 0;
		double bn_err = 0;
		double bd_err = 0;
		double bv_err = 0;

		for(int i = 0; i<OPObjectList[0]->segmentList.size(); i++)
		{
			br_err += OPObjectList[0]->computeRadialError(OPObjectList[0]->segmentList[i]);
			bn_err+= OPObjectList[0]->computeNormalError(OPObjectList[0]->segmentList[i]);
			bd_err+= OPObjectList[0]->computeDistError(OPObjectList[0]->segmentList[i]);
			bv_err+= OPObjectList[0]->computeVolumeError(OPObjectList[0]->segmentList[i]);
		}
	
		opttest.all_optimization(OPObjectList[0], 1e-3);

		for(int i = 0; i<OPObjectList[0]->segmentList.size(); i++)
		{
			OPObjectList[0]->segmentList[i]->m_isOptimized = true;
		}

		for(int j = 0;j<OPObjectList.size(); j++)
		{
			for(int i = 0; i<OPObjectList[j]->segmentList.size(); i++)
			{
				OPObjectList[j]->segmentList[i]->intersect_radialPrj();
			}
		}
		/*
		do{
			OPObjectList[0]->findCandidatePoints();
			OPObjectList[0]->Connect1ring();
			Opt opttest2;
			opttest.all_optimization(OPObjectList[0], 1e-3);
			OPObjectList[0]->searcherrorPoints();
		}
		while(OPObjectList[0]->p_Points.size() >3);*/
	/*
		for(int j = 0;j<OPObjectList.size(); j++)
		{
			for(int i = 0; i<OPObjectList[j]->segmentList.size(); i++)
			{
				OPObjectList[j]->segmentList[i]->intersect_radialPrj();
			}
		}
	
		OPObjectList[0]->computefinalError2(2);
		double ar_err = 0;
		double an_err = 0;
		double ad_err = 0;
		double av_err = 0;

		for(int i = 0; i<OPObjectList[0]->segmentList.size(); i++)
		{
			ar_err += OPObjectList[0]->computeRadialError(OPObjectList[0]->segmentList[i]);
			an_err+= OPObjectList[0]->computeNormalError(OPObjectList[0]->segmentList[i]);
			ad_err+= OPObjectList[0]->computeDistError(OPObjectList[0]->segmentList[i]);
			av_err+= OPObjectList[0]->computeVolumeError(OPObjectList[0]->segmentList[i]);
		}

		printf("[B rad : %f, A rad : %f, 皑家: %f\n B nor : %f, A nor : %f, 皑家: %f\n B dist : %f, A dist : %f, 皑家: %f\n B vol : %f, A vol : %f, 皑家: %f\n B total: %f, A total: %f , 皑家: %f]\n",
			br_err, ar_err, br_err- ar_err, 
			bn_err, an_err, bn_err- an_err, 
			bd_err, ad_err, bd_err- ad_err, 
			bv_err, av_err, bv_err- av_err, 
			br_err+ bn_err+ bd_err+ bv_err, ar_err+an_err+ad_err+av_err,  (br_err+ bn_err+ bd_err+ bv_err) - (ar_err+an_err+ad_err+av_err));

	}

	else
		std::cout<<"please arrange particle"<<std::endl;
}

void TW_CALL twbar::ResetCB(void *clientData)
{
	if(g_isarr)
	{
		OPObjectList[0]->segmentList.clear();
		OPObjectList[0]->m_connectionTable = NULL;
		OPObjectList[0]->seg_arr = NULL;
		OPObjectList[0]->everageEdgeLenght = 0;
		OPObjectList[0]->m_final_error_sum = 0;
		OPObjectList[0]->m_final_error_evg = 0;
		OPObjectList[0]->m_pre_error_sum = 0;
		OPObjectList[0]->m_pre_error_evg = 0;	

		g_isarr = false;
	}
}

void TW_CALL twbar::LoadCB(void *clientData)
{
	std::cout << openfilename().c_str();	
}

void twbar::twkbarsetting(TwBar *bar)
{
	TwAddButton(bar, "Load off file", LoadCB, NULL, " label='Load off file' key=f help='Load off file' ");
	
    TwAddVarCB(bar, "Particle", TW_TYPE_BOOL32, SetShowParticleCB, GetShowParticleCB, NULL, 
               " label='Show particle [p]' key=p help='Toggle show particle mode.' ");
	
    TwAddVarCB(bar, "Mesh", TW_TYPE_BOOL32, SetShowmeshCB, GetShowmeshCB, NULL, 
               " label='Show mesh [m]' key=m help='Toggle show mesh mode.' ");

	TwAddVarCB(bar, "Connect", TW_TYPE_BOOL32, SetShowconnectionCB, GetShowconnectionCB, NULL, 
               " label='Show connection [c]' key=c help='Toggle show connection mode.' ");

	TwAddButton(bar, "arrange", ArrCB, NULL, " label='Arr' key=A help='Arr' ");
	TwAddButton(bar, "Optimization", OptCB, NULL, " label='Optimization' key=o help='Optimization' ");
	TwAddButton(bar, "Reset particle", ResetCB, NULL, " label='Reset particle' key=r help='Reset particle' ");
}

*/