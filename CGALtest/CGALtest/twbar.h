#pragma once
#include <AntTweakBar.h>
#include <iostream>
class twbar
{
public:
	twbar(void);
	~twbar(void);

	TwBar *bar; // Pointer to the tweak bar

	void createbar();

	void twkbarsetting(TwBar *bar);

	void SetShowParticleCB(const void *value, void *clientData);
	void GetShowParticleCB(void *value, void *clientData);
	void SetShowmeshCB(const void *value, void *clientData);
	void GetShowmeshCB(void *value, void *clientData);
	void SetShowconnectionCB(const void *value, void *clientData);
	void GetShowconnectionCB(void *value, void *clientData);
	void ArrCB(void *clientData);
	void OptCB(void *clientData);
	void ResetCB(void *clientData);
	void LoadCB(void *clientData);
};

